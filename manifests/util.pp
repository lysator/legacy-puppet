# Comment out lines in the file $file matching the regexp $pattern.
# The regexp is implicitly anchored at both ends of the line.
# This is only done on matching lines within sections within groups;
# see the regexp_replace_lines type for how sections and groups work.
#
# Commenting out is done by adding $comment (default "#") to the start
# of the lines, and $comment_end to the end of the lines.

define comment_lines($file, $pattern='.*', $comment='#', $comment_end='',
$group_start='', $group_end='', $start='', $end='')
{
  $quoted_comment = regexp_quote($comment)
  $quoted_comment_end = regexp_quote($comment_end)

  regexp_replace_lines {
    "comment--${title}--${file}--${start}--${end}":
      file        => $file,
      group_start => $group_start,
      group_end   => $group_end,
      start       => $start,
      end         => $end,
      pattern     => $pattern,
      skip        => "${quoted_comment}${pattern}(${quoted_comment_end})?",
      replacement => "${comment}\\&${comment_end}";
  }
}



define config_option($file, $option, $value, $ensure='present')
{
  case $ensure {
    'present': {
      ensure_line {
        "config_option--${file}--${option}":
          file    => $file,
          line    => "${option}=${value}",
          pattern => "^(#)?${option}[ \t]*=.*$";
      }
    }
    'absent': {
      # FIXME: This will create an out-commented line if none exists.
      ensure_line {
        "config_option--${file}--${option}":
          file    => $file,
          line    => "#${option}=${value}",
          pattern => "^(#)?${option}[ \t]*=.*$";
      }
    }
    'purged': {
      delete_lines {
        "config_option--${file}--${option}":
          file    => $file,
          pattern => "^(#)?${option}[ \t]*=.*$";
      }
    }
    default: {
      fail("Bad config_option parameter ensure: ${ensure}")
    }
  }
}


# Ensure that the variable $setting is set to $value for subsystem $subsystem.
# If $service is set, that service is notified when the file is changed.
#
# Note that if you need spaces or other shell metacharacters in $value,
# you need to quote it yourself.
#
# This definition is RedHat-specific, as it does its duty by editing
# the /etc/sysconfig/$subsystem file.

define rh_sysconfig($subsystem, $setting, $value, $ensure='present',
$service='')
{
  config_option {
    "rh_sysconfig--${subsystem}--${setting}":
      ensure => $ensure,
      file   => "/etc/sysconfig/${subsystem}",
      option => $setting,
      value  => $value,
      notify => $service ? { '' => [], default => Service[$service] };
  }
}



# Configure settings in /etc/make.conf in Gentoo/Portage.
# Parameters:
# - $name        The name of the setting, e.g. CFLAGS, FEATURES or VIDEO_CARDS
# - $value        The value of the setting.  The value will be surrounded
#                by double quotes, but no other quoting of the value will
#                be done.  Be careful to not have newlines in the value,
#                since portage_makeconf won't be able to handle what it
#                will write in the file.
# - $ensure        'present' (default), 'absent' or 'purged'

define portage_makeconf($value='', $ensure='present')
{
  config_option {
    "portage_makeconf--${name}":
      ensure => $ensure,
      file   => '/etc/make.conf',
      option => $name,
      value  => "\"${value}\"",
      tag    => 'pkgrepo';
  }
}



# Internal helper class for Gentoo/Portage.
class portage_files__
{
  file {
    '/etc/portage':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755';
    [ '/etc/portage/package.use',
      '/etc/portage/package.keywords',
      '/etc/portage/package.mask',
      '/etc/portage/package.unmask',
    ]:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444';
  }
}

# Declare Portage USE flags for packages.
# Parameters:
# - $name        The package for which USE flags are set.
#                The special name "GLOBAL" means set global use flags,
#                i.e. set the USE variable in /etc/make.conf.
# - $use        The USE flags to set.  When set to the empty string,
#                the entire line for the package will be removed from
#                /etc/portage/package.use.

# Note: It might be tempting to add an automatic before => Package[$name],
# but that would be a mistake.  Sometimes you want to set use flags for
# packages that are only installed as dependencies for other packages,
# not because you want that package for itself.  Having to add that
# package to the Puppet manifests would be a nuisance, and wrong (it
# would then appear in the Portage world file).
#    Instead we tag the changes with 'pkgrepo', so it will be done in
# the first run of Puppet from Make.

define portage_useflags($use)
{
  case $name {
    'GLOBAL': {
      portage_makeconf {
        'USE': value => $use;
      }
    }
    default: {
      include ::portage_files__
      $quoted_package = regexp_quote($name)
      $quoted_use = regexp_quote($use)
      if $use {
        ensure_line {
          "portage-useflags--${name}":
            file       => '/etc/portage/package.use',
            line       => sprintf("%-24s\t%s", $name, $use),
            pattern    => "${quoted_package}[ \t]+.*",
            sufficient => "${quoted_package}[ \t]+${quoted_use}",
            require    => File['/etc/portage/package.use'],
            tag        => 'pkgrepo';
        }
      } else {
        delete_lines {
          "portage-useflags--${name}":
            file    => '/etc/portage/package.use',
            pattern => "${quoted_package}[ \t]+.*",
            require => File['/etc/portage/package.use'],
            tag     => 'pkgrepo';
        }
      }
    }
  }
}



# Declare Portage keyword for packages.
# Parameters:
# - $name        The package for which keyword is set.
#                The special name "GLOBAL" means set global keyword,
#                i.e. set the ACCEPT_KEYWORDS variable in /etc/make.conf.
# - $keyword        The keyword to set.  When set to the empty string,
#                the entire line for the package will be removed from
#                /etc/portage/package.keywords.

# Note: Don't add a before => Package[$name]; see the note for
# portage_useflags for further explanation.

define portage_keyword($keyword)
{
  case $name {
    'GLOBAL': {
      portage_makeconf {
        'ACCEPT_KEYWORDS': value => $keyword;
      }
    }
    default: {
      include ::portage_files__
      $quoted_package = regexp_quote($name)
      $quoted_keyword = regexp_quote($keyword)
      if $keyword {
        ensure_line {
          "portage-keyword--${name}":
            file       => '/etc/portage/package.keywords',
            line       => sprintf("%-24s\t%s", $name, $keyword),
            pattern    => "${quoted_package}[ \t]+.*",
            sufficient => "${quoted_package}[ \t]+${quoted_keyword}",
            require    => File['/etc/portage/package.keywords'],
            tag        => 'pkgrepo';
        }
      } else {
        delete_lines {
          "portage-keyword--${name}":
            file    => '/etc/portage/package.keywords',
            pattern => "${quoted_package}[ \t]+.*",
            require => File['/etc/portage/package.keywords'],
            tag     => 'pkgrepo';
        }
      }
    }
  }
}



# Set Gentoo config options, in /etc/conf.d.
# Parameters:
# - $subsystem        The subsystem for which an option is defined.  The option
#                will be written to /etc/conf.d/$subsystem.
# - $option        The name of the option to set.
# - $value        The value of the option.  The value will be surrounded
#                by double quotes, but no other quoting will be performed.
# - $ensure        'present' (default), 'absent' or 'purged'.

define gentoo_conf($subsystem, $option, $value, $ensure='present')
{
  config_option {
    "gentoo--${subsystem}--${option}":
      ensure => $ensure,
      file   => "/etc/conf.d/${subsystem}",
      option => $option,
      value  => "\"${value}\"",
  }
}



# Set options for a Xinetd service.
# Assumes that the service definition is in a file in /etc/xinetd.d
# with the same name as the service.

define xinetd_srv_option($service, $option, $value)
{
  $qopt = regexp_quote($option)
  $qval = regexp_quote($value)

  ensure_line {
    "xinetd--${service}--${option}":
      file       => "/etc/xinetd.d/${service}",
      line       => sprintf("\t%-23s = %s", $option, $value),
      pattern    => sprintf("[ \t]*%s[ \t]*=", $qopt),
      sufficient => sprintf("[ \t]*%s[ \t]*=[ \t]*%s[ \t]*$",
      $qopt, $qval),
      start      => ".*\\{",
      end        => "[ \t]*\\}",
  }
}



# Create (or remove) a system accont, i.e one with a "low" uid.
# The normal user type can't be instructed to create a system
# account without hardcoding a uid, which we don't want to do.
#
# A group with the same name as the user will be created at the
# same time.
#
# This implementation is RedHat specific.

define rh_sysuser($comment='', $home='/', $shell='/sbin/nologin',
$ensure='present')
{
  rh_sysgroup {
    $name:
      ensure => $ensure;
  }
  case $ensure {
    'present': {
      exec {
        "sysuser--${name}":
          command => "useradd -r -c '${comment}' -M -d '${home}' -s '${shell}' -g '${name}'  '${name}'",
          unless  => "getent passwd '${name}'",
          path    => '/sbin:/usr/sbin:/bin:/usr/bin',
          require => Rh_sysgroup[$name];
      }
    }
    'absent': {
      exec {
        "sysuser--${name}":
          command => "userdel -f '${name}'",
          onlyif  => "getent passwd '${name}'",
          path    => '/sbin:/usr/sbin:/bin:/usr/bin',
          before  => Rh_sysgroup[$name];
      }
    }
    default: {
      fail("Bad rh_sysuser parameter ensure: ${ensure}")
    }
  }
  # These are here so we get auto-require from things that want
  # user and/or group names, like the file type.
  user {
    $name:
      ensure  => $ensure,
      gid     => $name,
      comment => $comment,
      home    => $home,
      shell   => $shell,
      require => Exec["sysuser--${name}"],
  }
}


define rh_sysgroup($ensure='present')
{
  case $ensure {
    'present': {
      exec {
        "sysgroup--${name}":
          command => "groupadd -r '${name}'",
          unless  => "getent group '${name}'",
          path    => '/sbin:/usr/sbin:/bin:/usr/bin';
      }
    }
    'absent': {
      exec {
        "sysgroup--${name}":
          command => "groupdel '${name}'",
          onlyif  => "getent group '${name}'",
          path    => '/sbin:/usr/sbin:/bin:/usr/bin';
      }
    }
    default: {
      fail("Bad rh_sysgroup parameter ensure: ${ensure}")
    }
  }
  # This is here so we get auto-require from things that want
  # group names, like the file type.
  group {
    $name:
      ensure => $ensure, require => Exec["sysgroup--${name}"],
  }
}



# Mirror a directory tree using rsync.
#
# The key in the file $sshkey must not be encrypted.
#
# For security, the remote host should have something like this as flags
# in its .ssh/authorized_keys file for the key:
#
# command="/usr/bin/rsync --server -vlogDtprR --delete --delete-excluded . $dir"
# no-pty
# no-port-forwarding
# no-agent-forwarding
# no-X11-forwarding

define rsync_mirror($source, $target,
  $sshkey='',
  $unless='', $onlyif='', $creates='',
  $month='1-12', $monthday='1-31', $weekday='0-7',
  $hour='', $minute='',
  $ensure='present'
)
{
  if $sshkey {
    $sshcmd = "ssh -i${sshkey}"
  } else {
    $sshcmd = 'ssh'
  }
  $rsynccmd = 'rsync -aRqv --no-implied-dirs --delete --delete-excluded'
  $command = "RSYNC_RSH=\"${sshcmd}\"  ${rsynccmd}  '${source}'  '${target}'"

  case $ensure {
    'present': {
      exec {
        "${rsynccmd} '${source}' '${target}'":
          path    => '/bin:/usr/bin',
          unless  => $unless ? { '' => undef, default => $unless },
          onlyif  => $onlyif ? { '' => undef, default => $onlyif },
          creates => $creates ? { '' => undef, default => $creates };
      }

      if $hour {
        cron {
          "mirror--${title}":
            command  => "PATH=/bin:/usr/bin;  ${command}",
            user     => 'root',
            month    => $month,
            monthday => $monthday,
            weekday  => $weekday,
            hour     => $hour,
            minute   => $minute;
        }
      }
    }
    'absent': {
      tidy {
        $target:
          recurse => true,
          rmdirs  => true,
          size    => 0,
          backup  => false
          ;
      }
      cron {
        "mirror--${title}":
          ensure  => absent,
          command => "PATH=/bin:/usr/bin;  ${command}",
      }
    }
    default: {
      fail("Bad rsync_mirror parameter ensure: ${ensure}")
    }
  }
}




define rh_interface($bootproto='static',# "static", "dhcp" or "unconfigured"
  $ipaddress='',      # <IP>, <IP>/<MASK> or <IP>/<BITS>
  $netmask='',        # Netmask or bits
  $gateway='',
  $ipaddress6='',
  $netmask6='',
  $gateway6='',
  $onboot='yes',
  $ensure='up',
$persistent_dhcp='yes')
{
  file {
    "/etc/sysconfig/network-scripts/ifcfg-${name}":
      content => template("${::pupfiles}/rh-ifcfg.erb"),
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      notify  => Exec["rh_interface--ifconfig--${name}/${ensure}"];
  }
  case $ensure {
    'up': {
      exec {
        "rh_interface--ifconfig--${name}/up":
          command     => "/sbin/ifdown '${name}' && /sbin/ifup '${name}'",
          refreshonly => true,
          path        => '/bin:/usr/bin:/sbin:/usr/sbin';
      }
    }
    'down': {
      exec {
        "rh_interface--ifconfig--${name}/down":
          command     => "/sbin/ifdown '${name}'",
          refreshonly => true,
          path        => '/bin:/usr/bin:/sbin:/usr/sbin';
      }
    }
    default: {
      fail("Bad ensure parameter to rh_interface ${title}: ${ensure}")
    }
  }
}


# Define a sysctl setting.
#
# This sets the kernel's current value, as well as adding it to
# /etc/sysctl.conf so it gets loaded immediately when booting.
#
# Setting $value to the empty string ("") will remove the setting
# from /etc/sysctl.conf.  However, since there is no way to make
# the running kernel revert to its default value, it will not take
# effect until next boot.
#
# Note also that some sysctl parameters may require a restart of
# the kernel subsystem to take effect (e.g, the NFS lock daemon).

define sysctl($value)
{
  $qname = regexp_quote($name)

  if $value {
    ensure_line {
      "sysctl--${name}":
        file    => '/etc/sysctl.conf',
        line    => "${name} = ${value}",
        pattern => "${qname}[ \t]*=.*";
    }
    exec {
      "sysctl-w-${name}":
        command => "sysctl -w ${name}=${value}",
        unless  => "[ \"`sysctl -n ${name}`\" = '${value}' ]",
        path    => '/sbin:/bin:/usr/bin';
    }
  } else {
    delete_lines {
      "sysctl--${name}":
        file    => '/etc/sysctl.conf',
        pattern => "${qname}[ \t]*=.*";
    }
  }
}


define aptrepo($type, $uri, $dist, $components, $filename, $gpguri = false)
{
  $command = ''

  case $type {
    'deb': {
      ensure_line {
        "aptrepo--deb--${uri}--${dist}--${components}":
          file => "/etc/apt/sources.list.d/${filename}",
          line => "deb ${uri} ${dist} ${components}",
          tag  => pkgrepo;
      }
    }
    'deb-src': {
      ensure_line {
        "aptrepo--deb--src--${uri}--${dist}--${components}":
          file => "/etc/apt/sources.list.d/${filename}",
          line => "deb-src ${uri} ${dist} ${components}",
          tag  => pkgrepo;
      }
    }
    'other': {
      ensure_line {
        "aptrepo--deb--${uri}--${dist}--${components}":
          file => "/etc/apt/sources.list.d/${filename}",
          line => "deb ${uri} ${dist} ${components}",
          tag  => pkgrepo;
      }
      ensure_line {
        "aptrepo--deb--src--${uri}--${dist}--${components}":
          file => "/etc/apt/sources.list.d/${filename}",
          line => "deb-src ${uri} ${dist} ${components}",
          tag  => pkgrepo;
      }
    }
  }

  if $gpguri {
    exec {
      "aptrepo--gpg--${gpguri}":
        command => "/usr/bin/wget -T 3 -t 2 -O - ${gpguri} | /usr/bin/apt-key add -",
        unless  => "/usr/bin/apt-key list | /bin/grep `/usr/bin/wget -T 3 -t 2 -q -O - ${gpguri} |/usr/bin/gpg |/usr/bin/head -n1|/usr/bin/cut -d '/' -f 2|/usr/bin/cut -d ' ' -f 1`",
        tag     => pkgrepo;
    }
  }
}

define aptrepo_main($type, $uri, $dist, $components, $gpguri = false, $delete = false)
{
  $command = ''
  if $delete {
    case $type {
      'deb': {
        delete_lines {
          "aptrepo--deb--${uri}--${dist}--${components}":
            file    => '/etc/apt/sources.list',
            pattern => "deb ${uri} ${d}{ist} ${c}{omponents}",
            tag     => pkgrepo;
        }
      }
      'deb-src': {
        delete_lines {
          "aptrepo--deb--src--${uri}--${dist}--${components}":
            file    => '/etc/apt/sources.list',
            pattern => "deb-src ${uri} ${dist} ${components}",
            tag     => pkgrepo;
        }
      }
      'other': {
        delete_lines {
          "aptrepo--deb--${uri}--${dist}--${components}":
            file    => '/etc/apt/sources.list',
            pattern => "deb ${uri} ${dist} ${components}",
            tag     => pkgrepo;
        }
        delete_lines {
          "aptrepo--deb--src--${uri}--${dist}--${components}":
            file    => '/etc/apt/sources.list',
            pattern => "deb-src ${uri} ${dist} ${components}",
            tag     => pkgrepo;
        }
      }
    }
  }
  else {
    case $type {
      'deb': {
        ensure_line {
          "aptrepo--deb--${uri}--${dist}--${components}":
            file => '/etc/apt/sources.list',
            line => "deb ${uri} ${dist} ${components}",
            tag  => pkgrepo;
        }
      }
      'deb-src': {
        ensure_line {
          "aptrepo--deb--src--${uri}--${dist}--${components}":
            file => '/etc/apt/sources.list',
            line => "deb-src ${uri} ${dist} ${components}",
            tag  => pkgrepo;
        }
      }
      'other': {
        ensure_line {
          "aptrepo--deb--${uri}--${dist}--${components}":
            file => '/etc/apt/sources.list',
            line => "deb ${uri} ${dist} ${components}",
            tag  => pkgrepo;
        }
        ensure_line {
          "aptrepo--deb--src--${uri}--${dist}--${components}":
            file => '/etc/apt/sources.list',
            line => "deb-src ${uri} ${dist} ${components}",
            tag  => pkgrepo;
        }
      }
    }
  }

  if $gpguri {
    exec {
      "aptrepo--gpg--${gpguri}":
        command => "/usr/bin/wget -T 3 -t 2 -O - ${gpguri} | /usr/bin/apt-key add -",
        unless  => "/usr/bin/apt-key list | /bin/grep `/usr/bin/wget -T 3 -t 2 -q -O - ${gpguri} |/usr/bin/gpg |/usr/bin/head -n1|/usr/bin/cut -d '/' -f 2|/usr/bin/cut -d ' ' -f 1`",
        tag     => pkgrepo;
    }
  }
}

define seed_package($ensure = installed) {
  $seedpath = '/var/cache/local/preseeding'
  file {
    "${seedpath}/${name}.seed":
      path   => "${seedpath}/${name}.seed",
      source => [ "${::pupfiles}/seeds/${osdist}-${name}.seed",
        "${::pupfiles}/seeds/${::operatingsystem}-${name}.seed",
      "${::pupfiles}/seeds/${name}.seed" ],
      mode   => '0444',
      owner  => root,
      group  => root,
  }
  package {
    $name:
      ensure       => $ensure,
      responsefile => "${seedpath}/${name}.seed",
      require      => File["${seedpath}/${name}.seed"],
  }
}

define gitrepo($source, $destination)
{
  exec { "git-clone-${name}":
    command => "/usr/bin/git clone ${source} ${destination}",
    creates => "${destination}/.git";
  }

  $git_remote_onlyif_cmd = @("EOF"/$)
  /usr/bin/test "\$(/usr/bin/git -C ${destination} remote get-url origin)" != "${source}"
  | EOF
  exec { "/usr/bin/git -C ${destination} remote set-url origin ${source}":
    onlyif  => $git_remote_onlyif_cmd,
    require => Exec["git-clone-${name}"],
    before  => Exec["git-update-${name}"],
  }

  exec { "git-update-${name}":
    cwd         => $destination,
    environment => [
      "source=${source}",
      "destination=${destination}",
    ],
    command     => '/usr/bin/git pull',
    require     => Exec["git-clone-${name}"],
    onlyif      => '/usr/bin/test "`/usr/bin/git ls-remote ${source} | /usr/bin/head -n1`" != "`/usr/bin/git ls-remote ${destination} | /usr/bin/head -n1`"';
  }
}

define gconf($type, $value)
{
  exec { "gconf-${name}-${value}":
    command => "/usr/bin/gconftool-2 --type ${type} --set ${name} \"${value}\"",
    onlyif  => "/usr/bin/test `/usr/bin/gconftool-2 --get ${name}` != \"${value}\"",
    require => [Package['gconf2']],
  }
}

define gconf_direct($type, $value, $source)
{
  exec { "gconf-${name}-${value}":
    command => "/usr/bin/gconftool-2 --direct --config-source ${source} --type ${type} --set ${name} \"${value}\"",
    onlyif  => "/usr/bin/test -z \"`/usr/bin/gconftool-2 --direct --config-source ${source} --get ${name}`\" -o  \"`/usr/bin/gconftool-2 --direct --config-source ${source} --get ${name}`\" != \"${value}\"",
    require => [Package['gconf2']],
  }
}

define postmap_file($source, $service='')
{
  file {
    $name:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => $source,
      notify => Exec["postmap-${name}"];
  }

  exec {
    "postmap-${name}":
      command     => "/usr/sbin/postmap ${name}",
      refreshonly => true,
      notify      => $service ? { '' => 'Exec'[''], default => Service[$service] };
  }
}

define postalias_file($source, $service='')
{
  file {
    $name:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => $source,
      notify => Exec["postalias-${name}"];
  }

  exec {
    "postalias-${name}":
      command     => "/usr/sbin/postalias ${name}",
      refreshonly => true,
      notify      => $service ? { '' => [], default => Service[$service] };
  }
}


define vhost($source, $linkname = $name)
{
  file {
    "/etc/apache2/sites-available/${name}":
      owner   => root,
      group   => root,
      mode    => '0444',
      source  => $source,
      notify  => Service['apache2'],
      require => Package['apache2'],
      before  => File["/etc/apache2/sites-enabled/${linkname}"];
    "/etc/apache2/sites-enabled/${linkname}":
      ensure  => symlink,
      target  => "/etc/apache2/sites-available/${name}",
      owner   => root,
      group   => root,
      mode    => '0444',
      require => Package['apache2'],
      notify  => Service['apache2'];
  }
}


# http://forge.puppetlabs.com/mozrtucker/recursive_directory
# But with some local changes.
#
# == Class: recursive_directory
#                                                                                                                      
# Defined type to recursively create files from templates
#
# === Parameters
#
# Document parameters here.
#
# [*source_dir*]
#   The source directory containing templates
#
# [*dest_dir*]
#   The destination for the interpolated files
#
# [*file_mode*]
#   The file mode for the created file resources
#   Default: '0600'
#
# [*owner*]
#   The file owner for the created files
#   Default: 'nobody'
#
# [*group*]
#   The file group for the created files
#   Default: 'nobody'
#
# === Examples
# class recursive_directory::test {
#     recursive_directory{'recursive_something':
#         source_dir => 'recursive_directory',
#         dest_dir  => '/tmp',
#         owner      => 'root',
#         group      => 'root'
#     }
# }
#
# === Authors
#
# Rob Tucker <rtucker@mozilla.com>
#
# === Copyright
#
# Copyright 2013 Rob Tucker here, unless otherwise noted.
#
define recursive_directory (
  $source_dir = undef,
  $dest_dir = undef,
  $file_mode = undef,
  $owner = 'nobody',
  $group = 'nobody',
  $dir_mode = undef
){
  if $source_dir and $dest_dir {
    $resources_to_create = recurse_directory(
      $source_dir,
      $dest_dir,
      $file_mode,
      $owner,
      $group,
      $dir_mode
    )
    #notice($resources_to_create)
    create_resources('file', $resources_to_create)
  } else {
    fail('source_dir and dest_dir are required')
  }
}
