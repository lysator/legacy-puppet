class ubuntu_package_repositories
{
  exec {
    'apt-update':
      command => '/usr/bin/apt-get update',
  } -> Package <| |>

  file {
    'local-apt-preferences':
      path    => '/etc/apt/preferences.d/',
      source  => "${::pupfiles}/apt-prefs-ubuntu-${::lsbdistcodename}/",
      recurse => true,
      purge   => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  file {
    'local-apt-keys':
      path    => '/etc/apt/trusted.gpg.d/',
      source  => "${::pupfiles}/apt-keys-ubuntu/",
      recurse => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0644';
  }

  recursive_directory {
    'local-apt-sources':
      source_dir => "${::pupfiles}/apt-sources-ubuntu/",
      dest_dir   => '/etc/apt/sources.list.d/',
      owner      => 'root',
      group      => 'root',
      dir_mode   => '0755',
      file_mode  => '0644',
      notify     => Exec['apt-update'],
      require    => File['local-apt-keys'];
  }

  file {
    '/etc/apt/sources.list.d/':
      ensure  => 'directory',
      recurse => true,
      purge   => true,
  }
}

class ubuntu_install_cleanup {
  user {
    'install':
      ensure => absent,
  }
}

class ubuntu_locales {
  file {
    '/var/lib/locales/supported.d/local':
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template("${::pupfiles}/ubuntu-locales.erb"),
      notify  => Exec['ubuntu-generate-locales'];
  }

  exec {
    'ubuntu-generate-locales':
      command     => '/usr/sbin/dpkg-reconfigure locales',
      refreshonly => true;
  }
}

class ubuntu_usb_udev
{
  file {
    '/etc/udev/rules.d/99-lysator-usb-block-devices.rules':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/udev/udev-conf.d-usbsticks.rules",
      notify => Exec['update-udev'];
  }

  exec {
    'update-udev':
      command     => '/sbin/udevadm control --reload_rules',
      refreshonly => true;
  }
}

class ubuntu_gconfs
{
  gconf_direct {
    '/apps/gdm/simple-greeter/disable_user_list':
      type   => 'bool',
      value  => true,
      source => 'xml:readwrite:/etc/gconf/gconf.xml.defaults';
  }

  gconf {
    '/apps/metacity/general/button_layout':
      type  => 'string',
      value => ':minimize,maximize,close';
  }
}

class ubuntu_apt
{
  file {
    '/etc/apt/sources.list':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      content => template("${::pupfiles}/sources.list-Ubuntu.erb"),
      notify  => Exec['apt-update'],
      tag     => pkgrepo;
  }
}

class ubuntu_papersize
{
  file {
    '/etc/papersize':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      content => template("${::pupfiles}/papersize-Ubuntu.erb");
  }
}

class ubuntu_udev
{
  file {
    '/etc/udev/rules.d/99-ubuntu-serial.rules':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template("${::pupfiles}/udev/99-ubuntu-serial.rules.erb"),
      notify  => Service['udev'];
  }

  service {
    'udev':
      ensure  => $::running,
      enable  => true,
      pattern => 'udevd';
  }
}

class ubuntu_disable_suspend
{
  file {
    '/usr/share/polkit-1/actions/org.freedesktop.upower.policy':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template("${::pupfiles}/ubuntu-org.freedesktop.upower.policy"),
  }
}

class ubuntu_disable_fstrim
{
  file {
    '/etc/cron.weekly/fstrim':
      ensure => absent;
  }
}

# sudo cannot be uninstalled because gksu installs both gksu _and_
# gksudo, where the latter depends on sudo, and some other programs in
# turn depend on gksu. Let's just remove the binaries for sudo and
# gksudo to prevent users from trying to run them.
class ubuntu_disable_sudo
{
  file {
    '/usr/bin/sudo':
      ensure => absent;
  }
  file {
    '/usr/bin/sudoedit':
      ensure => absent;
  }
  file {
    '/usr/bin/gksudo':
      ensure => absent;
  }
}

class ubuntu_video_audio_device_permissions
{
  file {
    '/etc/udev/rules.d/50-udev-default.rules':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/udev/50-udev-default.rules",
  }
}

class ubuntu_jtag_devices_permissions
{
  file {
    '/etc/udev/rules.d/99-ubuntu-jtag-stellaris-launchpad.rules':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/udev/99-ubuntu-jtag-stellaris-launchpad.rules",
  }
}

class ubuntu_allow_sysrq_sak
{
  file {
    '/etc/sysctl.d/10-magic-sysrq.conf':
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/sysctl.d-ubuntu/10-magic-sysrq.conf",
  }
}

class ubuntu_set_ptrace_scope
{
  file {
    '/etc/sysctl.d/10-ptrace.conf':
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/sysctl.d-ubuntu/10-ptrace.conf",
  }
}

class ubuntu_enable_bitmap_fonts
{
  file {
    '/etc/fonts/conf.d/70-no-bitmaps.conf':
      ensure => absent,
  }
  file {
    '/etc/fonts/conf.d/70-yes-bitmaps.conf':
      ensure => link,
      target => '../conf.avail/70-yes-bitmaps.conf',
  }
}

class ubuntu_remove_ibus {
  file {
    '/usr/bin/ibus-daemon':
      ensure => absent,
  }
}

class ubuntu_allow_usb_disk_mount {
  file {
    '/etc/polkit-1/localauthority/30-site.d/org.freedesktop.udisks2.filesystem-mount.pkla':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/polkit/org.freedesktop.udisks2.filesystem-mount.pkla",
      require => Package['policykit-1'],
  }
  file {
    '/etc/polkit-1/localauthority/30-site.d/org.freedesktop.udisks2.eject-media.pkla':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/polkit/org.freedesktop.udisks2.eject-media.pkla",
      require => Package['policykit-1'],
  }
  package {
    'policykit-1':
      ensure => installed,
  }
}

class ubuntu_disable_whoopsie {
  service {
    'whoopsie':
      ensure => stopped,
      enable => false,
  }
}

class ubuntu_mate_default_background {
  file {
    '/usr/share/glib-2.0/schemas/zz-mate-desktop.gschema.override':
      content => "[org.mate.background]\npicture-filename='/usr/share/backgrounds/warty-final-ubuntu.png'\n",
  }
}

class ubuntu_disable_baloo {
  file {
    '/etc/xdg/autostart/baloo_file.desktop':
      ensure => absent,
  }
}

class ubuntu_custom_xorg_conf {
  $xorgconf = file("${::pupfiles}/xorg.conf-${::hostname}", '/dev/null')
  if $xorgconf != '' {
    file {
      '/etc/X11/xorg.conf':
        content => $xorgconf,
        owner   => 'root',
        group   => 'root',
        mode    => '0444',
        require => Class['network'],
    }
  }
}

class ubuntu_shutup_update_notifier {
  # This cron script prints unnecessary information on stdout. Silence
  # it to reduce spam emails.
  ensure_line {
    'update_notifier_common_shut_up':
      file  => '/etc/cron.daily/update-notifier-common',
      line  => 'exec > /dev/null  # Shut up -- Puppet',
      where => '#!/bin/.*sh',
  }
}

class ubuntu_remove_pppoeconf {
  # https://bugs.launchpad.net/ubuntu/+source/ubuntu-meta/+bug/1766659
  package {
    'pppoeconf':
      ensure => 'purged';
    # only pulled in by pppoeconf
    'ifupdown':
      ensure => 'purged';
  }
}

class ubuntu_nuke_systemd_resolved {
  service { 'systemd-resolved':
    ensure => stopped,
    enable => mask,
  }
}

class ubuntu_fix_logind_nis {
  # https://github.com/systemd/systemd/issues/7074
  package { 'nscd':
    ensure => installed,
  }
}

class ubuntu_environment {
  file { '/etc/profile.d/lysator-env-ubuntu.sh':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "${::pupfiles}/profile.d/lysator-env.sh-ubuntu",
  }

  # This really should be generalized to all systems.
  ensure_line { 'studieinfo_man':
    file => '/etc/manpath.config',
    line => 'MANDATORY_MANPATH /lysator/studieinfo/man',
  }
}

class ubuntu_nuke_motd {
  # ubuntu adds a lot of spam in a custom motd
  # one of the scripts (/etc/update-motd.d/50-motd-news) also calls home with
  # ubuntu release, os type, kernel version, arch, cpu model, uptime
  # => nuke all of it
  file { '/etc/update-motd.d/':
    ensure => absent,
    force  => true,
  }
  service { ['motd-news.service', 'motd-news.timer']:
    ensure => stopped,
    enable => mask,
  }
  file { '/run/motd.dynamic':
    ensure => absent,
  }
}

class ubuntu_apparmor_man_nfs {
  file { '/etc/apparmor.d/local/usr.bin.man':
    ensure => file,
    source => "${::pupfiles}/apparmor-usr.bin.man",
  } ~> exec { 'reload apparmor':
    command => 'systemctl reload apparmor',
    path    => '/bin',
  }
}

class ubuntu_fixes
{
  include ::ubuntu_install_cleanup
  include ::ubuntu_locales
  include ::ubuntu_gconfs
  include ::ubuntu_package_repositories
  include ::ubuntu_apt
  include ::ubuntu_papersize
  include ::ubuntu_udev
  include ::debian_preseed
  include ::debian_pathsh
  include ::ubuntu_disable_suspend
  include ::ubuntu_disable_fstrim
  include ::ubuntu_disable_sudo
  include ::ubuntu_video_audio_device_permissions
  include ::ubuntu_jtag_devices_permissions
  include ::ubuntu_allow_sysrq_sak
  include ::ubuntu_set_ptrace_scope
  include ::ubuntu_remove_ibus
  include ::ubuntu_allow_usb_disk_mount
  include ::ubuntu_disable_whoopsie
  include ::ubuntu_mate_default_background
  include ::ubuntu_disable_baloo
  include ::ubuntu_custom_xorg_conf
  include ::ubuntu_enable_bitmap_fonts
  include ::ubuntu_shutup_update_notifier
  include ::ubuntu_remove_pppoeconf
  include ::ubuntu_nuke_systemd_resolved
  include ::ubuntu_fix_logind_nis
  include ::ubuntu_environment
  include ::ubuntu_nuke_motd
  include ::ubuntu_apparmor_man_nfs
}
