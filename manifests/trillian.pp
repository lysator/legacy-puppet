class trillian  {

  package { ['sudo', 'firmware-linux-nonfree', 'firmware-bnx2', 'infiniband-diags', 'libmlx4-1', 'python', 'wget', 'fail2ban']:
    ensure  => installed,
    require => Exec['apt-update'],
  }

  ~> file { '/etc/sudoers.d/ceph-vogon':
    content => 'ceph-vogon ALL = (root) NOPASSWD:ALL',
    mode    => '0440',
    owner   => root,
    group   => root,
  }

  user { 'ceph-vogon':
    ensure => present,
    home   => '/ceph/ceph-vogon',
  }

  ~> file { '/ceph':
    ensure => directory,
    owner  => 'ceph-vogon',
  }

  ~> file { '/ceph/ceph-vogon':
    ensure => directory,
    owner  => 'ceph-vogon',
  }

  ~> ssh_authorized_key { 'ceph-vogon@marvin':
    ensure => present,
    user   => 'ceph-vogon',
    type   => 'ssh-rsa',
    key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDIvOt7p7PLOdslGmUYO+4qmPhjTEEiCuSfT2asPuRvmiB4rsVzcCmlItS2wvqEkcuO41YWbBcWGo48tuDm/UcXAFe2bR5O7hdPjc85xE4qJv2l9wF8jh1h5pqbkdncLmQ5wrkY1pSQZ5HybrKMEQ85NfFxUSIM1PwjLiET9+0c71d3dnYvMJMsknoeIrKpPCWWm/L52mrH9mWo87qavINYOtBF9KfTaM4r5XW6A4QQPMNMiqKxdqh6KNIsqWBGneSA0ZDa8w5q4WLY35gtwTgvEhikm/0k/Dz3bAYycqlcgTKGIKM12/cmAu0bA7PsuviKIHQN1TSQ690rsC6eL/yv',
  }

  file { '/etc/modules':
    source => "${::pupfiles}/trillian/etc/modules",
    owner  => root,
    group  => root,
    mode   => '0644',
  }

  file { '/etc/logrotate.d/ceph-common':
    source => "${::pupfiles}/trillian/etc/logrotate.d/ceph-common",
    owner  => root,
    group  => root,
    mode   => '0644',
  }
}
