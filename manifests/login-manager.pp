class login_manager
{
  include "login_manager::${operatingsystem}"
}

class login_manager::ubuntu
{
  $login_manager = 'wdm'

  package {
    'login-manager':
      ensure => installed,
      name   => $login_manager,
  }

  file {
    'default-display-manager':
      ensure  => file,
      name    => '/etc/X11/default-display-manager',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['login-manager'],
      content => "/usr/bin/${login_manager}",
  }

  file { '/etc/systemd/system/display-manager.service':
    ensure => link,
    target => '/lib/systemd/system/wdm.service',
    owner  => 'root',
    group  => 'root',
  }

  file {
    'xsession.desktop':
      ensure  => file,
      name    => '/usr/share/xsessions/xsession.desktop',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['login-manager'],
      source  => "${::pupfiles}/xsessions/xsession.desktop";
  }

  service {
    'login-manager':
      ensure    => $running,
      enable    => true,
      name      => $login_manager,
      require   => Package['login-manager'],
      subscribe => [ File['default-display-manager'],
      Service['nis'] ];
  }

  ensure_line {
    'wdm_config_background':
      file    => '/etc/X11/wdm/wdm-config',
      line    => 'DisplayManager*wdmBg: pixmap:/usr/share/backgrounds/warty-final-ubuntu.png',
      pattern => '^DisplayManager[*]wdmBg:.*',
      require => Package['login-manager'],
  }

  ensure_line {
    'wdm_config_logo':
      file    => '/etc/X11/wdm/wdm-config',
      line    => 'DisplayManager*wdmLogo: /usr/share/images/lysator-logo-500x500.png',
      pattern => '^DisplayManager[*]wdmLogo:.*',
      require => Package['login-manager'],
  }

  file { '/usr/share/images/':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file {
    '/usr/share/images/lysator-logo-500x500.png':
      ensure  => file,
      source  => "${::pupfiles}/lysator-logo-500x500.png",
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => File['/usr/share/images/'],
  }

  # Custom Xsession to make wdm source /etc/profile.
  file {
    '/etc/X11/wdm/Xsession':
      ensure  => file,
      require => Package['login-manager'],
      name    => '/etc/X11/wdm/Xsession',
      source  => "${::pupfiles}/Xsession-wdm-workstations",
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
  }
}
