class harbinger {

  ### packages
  package {
    ['boost-devel', 'gcc', 'gcc-c++', 'gcc7', 'gcc7-c++', 'irssi', 'irssi-extra',
      'php7','python3', 'python3-virtualenv', 'python3-pip', 'python3-numpy',
    'python', 'python-virtualenv', 'python-pip', 'python-numpy', 'snapd', 'weechat', 
    'python-devel', ]:
      ensure => installed,
  }

  cron {
    'update-pip':
      command  => '/opt/puppet/scripts/randomsleep; pip install --quiet --upgrade pip && pip3 install --quiet --upgrade pip',
      user     => 'root',
      month    => '1-12',
      monthday => '1-31',
      weekday  => '0-7',
      hour     => '3',
      minute   => '33';
  }


}
