class ldap_common
{
  package {
    [
      'ldap-utils',
    ]:
      ensure  => installed,
      require => Class['network'];
  }
}

class autofs_ldap
{
  package {
    [
      'autofs-ldap',
    ]:
      ensure  => installed,
      require => Class['network'];
  }
  file {
    '/etc/default/autofs':
      ensure => file,
      source => "${::pupfiles}/ldap/autofs",
      owner  => 'root',
      group  => 'root',
      mode   => '0644';

    '/etc/autofs_ldap_auth.conf':
      ensure => file,
      source => "${::pupfiles}/ldap/autofs_ldap_auth.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0600';
  }
}

class ldap_common_client
{
  include '::ldap_common'
  package {
    [
      'libnss-ldapd', 'nslcd', 'libsasl2-modules-gssapi-mit',
    ]:
      ensure  => installed,
      require => Class['network'];
  }

  file {
    '/etc/nslcd.conf':
      ensure => file,
      source => "${::pupfiles}/ldap/nslcd.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0640';

    '/etc/ldap/ldap.conf':
      ensure => file,
      source => "${::pupfiles}/ldap/ldap.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0644';

    '/etc/ldap/ipa_ca.crt':
      ensure => file,
      source => "${::pupfiles}/ldap/ipa_ca.crt",
      owner  => 'root',
      group  => 'root',
      mode   => '0644';
  }

  service {
    'nslcd':
      ensure => running,
      enable => true,
      name   => 'nslcd';
  }
}

class ldap_server
{
  include '::ldap_common'
  package {
    [
      'slapd', 'migrationtools', 'ldapvi', 'ldapscripts',
    ]:
      ensure  => installed,
      require => Class['network'];
  }

  file {
    '/etc/ldap/schema/autofs-ldap.ldif':
      ensure => file,
      source => "${::pupfiles}/ldap/autofs-ldap.ldif",
      owner  => 'root',
      group  => 'root',
      mode   => '0644';

    '/etc/default/slapd':
      ensure => file,
      source => "${::pupfiles}/ldap/slapd",
      owner  => 'root',
      group  => 'root',
      mode   => '0644';
  }

  service {
    'slapd':
      ensure => running,
      name   => 'slapd',
      enable => true;
  }
}

class ldap_client
{
  include '::ldap_common_client'
  file {
    '/etc/nsswitch.conf':
      ensure => file,
      source => "${::pupfiles}/ldap/nsswitch.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0644';
  }
}

class ldap_client_limited_base
{
  include '::ldap_common_client'
  file {
    '/roots':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0770';
  }

  ensure_line {
    'passwd_root':
      file    => '/etc/passwd',
      line    => $has_homedirs ? {
        true    => '+@root:x:::::',
        default => '+@root:x::::/roots:',
      },
      before  => Ensure_line['passwd_nouser'],
      require => File['/etc/nsswitch.conf'];

    'passwd_nouser':
      file    => '/etc/passwd',
      line    => '+:x:::::/bin/nologin',
      require => File['/etc/nsswitch.conf'];
  }
}

class ldap_client_limited
{
  include '::ldap_client_limited_base'
  file {
    '/etc/nsswitch.conf':
      ensure => file,
      source => "${::pupfiles}/ldap/nsswitch.conf-limited",
      owner  => 'root',
      group  => 'root',
      mode   => '0644';
  }
}

class ldap_client_limited_cached
{
  include '::ldap_client_limited_base'
  file {
    '/etc/nsswitch.conf':
      ensure => file,
      source => "${::pupfiles}/ldap/nsswitch.conf-limited_cached",
      owner  => 'root',
      group  => 'root',
      mode   => '0644';
  }
  package {
    [
      'nss-updatedb',
    ]:
      ensure  => installed,
      require => Class['network'];
  }
  package {'moreutils': }
  cron {
    'nss_updatedb':
      command => '/usr/bin/chronic -e /usr/sbin/nss_updatedb ldap',
      user    => 'root',
      hour    => 3,
      minute  => 0,
      require => Package['nss-updatedb'];
  }
}
