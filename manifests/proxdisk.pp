class proxdisk
{
  cron {
    'scrub-backup':
      command  => '/sbin/zpool scrub backup',
      user     => 'root',
      month    => '1-12',
      monthday => '7',
      weekday  => '*',
      hour     => '3',
      minute   => '33';
  }

  proxdisk::backup_user { 'holgerspexet':
    uid     => 995101,
    ssh_key => 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCrXm5IEeWuZxHVo4LxWtQF1nIFFEEzumWZLLZ8sazxI97pqLGYr8cShmo2kHQxtQqeUxdeazVMrPuc+9QF+9zcxKJwtnTLoYRnbF1b0+wfuOv62Svx5o8YkAKfj5Rh47zPix+3rCBxitfFkqQAHAgHSiNuc2Vcph0uR8QAxnBZBg6eY5X0n4VaAyzNE5F72vo6i13zZ+2BPB1WdNM6dTg0fSSzThGw3znRMxXta3LmdgpcBNhmJ6z1jW8hhgBLExYYvecRu3B2/o/3PxbJbcsI9TH2iPsqtWiBGiSD81JrrJYRW80cloxfcMXuxtYsNWSqUkNnP0W1HDQlmqrCr11z root@holgerspexet\nssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9f7alOqUs2bsY6fU8j3r57Ixm83hyr/ViQz+5SEhNxpk14Iq4MD1IJ3fk/goVPfANyruk8IYjeQulaOhdDKbI16KftfHHzSp8hFDBmg8QZ7KfzKqhQr88BV5R6qeWubTaBV+GaYQIC+D0lMoV4lFTf4rI+05Xdbqkena46IhoKc9Fl9rokV4REFm/jHpPt8mkHg2q9wNWfMDSO5xh0CcAeLF1lp4AJZ1AC61RGmH06G+owIXs+AASjUwH5OP5dq33hIizRNoncTSPWKtmpZWorOcbt8SdF8X0XJAmAQoWRkuloUpsklXDOGvC8pClcrWbv6FkzAm2p4JVJkxNnioT root@holgerspexet-public'
  }

  # WWW
  proxdisk::backup_user { 'knuth':
    ssh_key => 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDG9HcO0rv08ncRkpFlKCebe89/5Uo7CuVLvPCsXIzBRntdpFLBfFrA23vtpHL6iITBpEMRMLUfHTAXvglX4ll+BHm76bK1YTzB8HYmwe107LXoZ0wW+n+qsSkZWTEvgDwGmc76/caRrilLoPDeFw+C0ZVAPzPvNcaqIjOezjFas41CF/eBhk8St5emzsn/lxBgzl6tWE9Ig438pDTeJJJODKy8ZSNdkAf+45ekQ6qtFitn++itwgzX5aMToviPxyemf8CbEDu9GeK/tzj1lysQyafRe17bOYt97EPljJIjxv0lHEqJXEW3Po3musDaMgPP6NMUEeirElVYzBlDhZT9 root@knuth.lysator.liu.se'
  }

  proxdisk::backup_user { 'liufs-test':
    ssh_key => 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDj/y5lJEvdYvSbvXPUrnww2zJMfenchjCU90kf7JO7g/SqjiePbLLiwbmlCUoCQj/LWAJlOABoSqscAa60PO1sBx69RyacUlIHI1CcH1XvjffwWkCS9yZCUzPvJb2aie1EjBmg9IP3IF7gZxq6wPC0tbbOjklQXsNgC51KMGbTEX3l3zx5kRUq/MPXLIWtnIkCByOeVYkgPsGQd2+Icw8aT0mvcSEK2QOXAKUyEP+R4PGNi6h76OTOTUgHzCvvfIdzSMIIyKsgJRSvtK035ZB/Y52OTWL9uaCzqjZp6JUa3rPM8bMsqjmRts/rsk8XrQtB8oFumMaEyK5AkNtDz8dp root@liufs-test'
  }

  proxdisk::backup_user { 'violet':
    ssh_key => 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCqLKC2tld2mc9K0jAdKgzk1lxv0nep86Xo03/46zjf0x7ZnyLFmjVVdVFAmxUUD5rji+Tm/N70ThsGKBjBxpuOuHu437P8V5GYk3N3f+GYdQfXybW3BUpYP9MJal3FI3odtH9eq0E9n2biasJvt53spYSk4QjUUkZpv/VZLFT8F5kzGFcAnPSbEHeuheQLVJ5naj8uQeiB5oXU9GdCsGcaR6fRXAD9VK14z+XzHyAnWB4/abeAvY1M4p/qRjvkgX/rjZOpFVtRSQAbCDcLFLt5xXBPCKUMzd/OVgeAXd8jOXiKmcs+8LkTYjX3enGxq/tMtNpgR5DCcaduORiS1uc7 root@violet'
  }

  package {
    [
      'ifenslave', 'nfs-kernel-server',
    ]:
      ensure => installed,
  }

#  file {
#    "exports":
#        name => "/etc/exports",
#        ensure => file,
#        owner => "root", group => "root", mode => '0644',
#        content => "/proxdisk/iso-storage 10.44.1.0/255.255.255.0(rw,no_root_squash,subtree_check)",
#        notify => Exec["export-all-nfs"];
#    "iso-storage":
#        name => "/proxdisk/iso-storage",
#        ensure => directory,
#        owner => "root", group => "root", mode => '0755';
#  }

  file {
    'sysctl':
      ensure => file,
      name   => '/etc/sysctl.conf',
      source => "${::pupfiles}/sysctl.conf-proxdisk",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

#  file {
#    'iscsitarget':
#        name => "/etc/default/iscsitarget",
#        ensure => file,
#        source => "${::pupfiles}/default-iscsitarget-proxdisk",
#        owner => 'root',
#        group => 'root',
#        mode => '0644',
#  }

  file {
    'aliases-bond':
      ensure => file,
      name   => '/etc/modprobe.d/aliases-bond.conf',
      source => "${::pupfiles}/network/interfaces/aliases-bond.conf-ib_lag",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  exec {
    'export-all-nfs':
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => 'exportfs -a',
  }
#  exec {
#    'zpool-create-backup':
#      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
##        command => "zpool create proxdisk raidz2 sdc sdd sde sdf sdg sdh sdi sdj sdk sdl raidz2 sdm sdn sdo sdp sdq sdr sdt sdu sdv sdw raidz2 sdx sdy sdz sdaa sdab sdac sdad sdae sdaf sdag spare sdah sdai sdaj",
#      command => 'zpool create backup raidz3 sdc sdd  sde  sdf  sdg  sdh  sdi  sdj  sdk  sdl  sdm  raidz3 sdn  sdo  sdp  sdq  sdr  sds  sdt  sdu  sdv  sdw  sdx  raidz3 sdy  sdz  sdaa sdab sdac sdad sdae sdaf sdag sdah sdai raidz3 sdaj sdak sdal sdam sdan sdao sdap sdaq sdar sdas sdat spare sdau sdav',
#      unless  => 'zpool status backup',
#      notify  => Exec['zfs-create-backup/vm', 'zfs-create-backup/home'],
#  }
#  exec {
#    'zfs-create-proxdisk/vm':
#      path        => '/bin:/usr/bin:/sbin:/usr/sbin',
#      require     => Exec['zpool-create-backup'],
#      command     => 'zfs create -o compression=off proxdisk/vm',
#      unless      => 'zfs list proxdisk/vm',
#      refreshonly => true,
#  }
#  exec {
#    'zfs-create-proxdisk/home':
#      path        => '/bin:/usr/bin:/sbin:/usr/sbin',
#      require     => Exec['zpool-create-backup'],
#      command     => 'zfs create -o compression=off proxdisk/home',
#      unless      => 'zfs list proxdisk/home',
#      refreshonly => true,
#  }
}
