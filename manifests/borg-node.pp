class borg_node {
  ### packages
  package {

    [ 'slurm-llnl', 'slurmdbd', 'mpi-default-dev', ]:
      ensure => installed,
  }

  ## slurm
  file {
    '/etc/slurm-llnl/slurm.conf':
      ensure => file,
      source => "${::pupfiles}/borg/slurm.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  service {
    'slurmctld':
      ensure => running,
      enable => true,
  }

  ## Load infiniband kernel modules
  file {
    '/etc/modules':
      ensure => file,
      source => "${::pupfiles}/borg/infiniband_modules",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  ## munge key
  file {
    '/srv/nfsroot/etc/munge/munge.key':
      ensure => file,
      source => '/etc/munge/munge.key',
      owner  => 'munge',
      group  => 'munge',
      mode   => '0400',
  }
}
