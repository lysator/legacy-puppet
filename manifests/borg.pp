class borg {

  ### packages
  package {

    ['isc-dhcp-server', 'tftpd-hpa', 'slurm-llnl', 'iptables-persistent', 'fail2ban',
      'debootstrap', 'syslinux', 'nfs-kernel-server', 'pxelinux', 'dnsmasq', 'slurmdbd',
      'distcc', 'mpi-default-dev',
    'opensm',]:
      ensure => installed,
  }

  ### tftpd
  service {
    'tftpd-hpa':
      ensure    => running,
      require   => [Package['tftpd-hpa'],File['/srv/tftp/']],
      enable    => true,
      subscribe => [File['/etc/default/tftpd-hpa']],
  }

  service {
    'nfs-kernel-server':
      ensure  => running,
      require => [Package['nfs-kernel-server']],
      enable  => true,
  }

  file {
    '/etc/default/tftpd-hpa':
      ensure => file,
      source => "${::pupfiles}/borg/tftpd-hpa",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  ### pxeboot files.
  file { # pxelinux config
    '/srv/tftp/':
      ensure  => directory,
      source  => "${::pupfiles}/borg/tftp/",
      recurse => true,
      owner   => 'root',
      group   => 'nogroup',
      mode    => '0755',
  }

  file { # ldlinux.c32 from master
    '/srv/tftp/ldlinux.c32':
      ensure => file,
      source => '/usr/lib/syslinux/modules/bios/ldlinux.c32',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file { # pxelinux from master
    '/srv/tftp/pxelinux.0':
      ensure => file,
      source => '/usr/lib/PXELINUX/pxelinux.0',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file { # initramfs from chroot
    '/srv/tftp/initrd.img':
      ensure => file,
      source => '/srv/nfsroot/initramfs',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }


  file { # vmlinuz from host
    '/srv/tftp/vmlinuz':
      ensure => file,
      source => '/boot/vmlinuz-3.16.0-4-amd64',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  ### dhcpd
  file {
    '/etc/dhcp/dhcpd.conf':
      ensure => file,
      source => "${::pupfiles}/borg/dhcpd.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file {
    '/etc/default/isc-dhcp-server':
      ensure => file,
      source => "${::pupfiles}/borg/isc-dhcp-server",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  service {
    'isc-dhcp-server':
      ensure    => running,
      subscribe => [File['/etc/dhcp/dhcpd.conf'], File['/etc/default/isc-dhcp-server']],
      enable    => true,
  }


  ### iptables
  file {
    '/etc/iptables/':
      ensure  => directory,
      source  => "${::pupfiles}/borg/iptables/",
      recurse => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
  }

  file { # enable forwarding 
    '/etc/sysctl.conf':
      ensure => file,
      source => "${::pupfiles}/borg/sysctl.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
  }

  ### dns
  file {
    '/etc/dnsmasq.conf':
      ensure => file,
      source => "${::pupfiles}/borg/dnsmasq.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file {
    '/etc/borg_resolv.conf':
      ensure => file,
      source => "${::pupfiles}/borg/borg-resolv.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
  }

  file {
    '/etc/borg_nodes':
      ensure => file,
      source => "${::pupfiles}/borg/borg_nodes",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  service {
    'dnsmasq':
      ensure    => running,
      subscribe => [File['/etc/dnsmasq.conf']],
      enable    => true,
  }

  ### nfs-root export
  file {
    '/etc/exports':
      ensure => file,
      source => "${::pupfiles}/borg/exports",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  ### slurm
  file {
    '/etc/slurm-llnl/slurm.conf':
      ensure => file,
      source => "${::pupfiles}/borg/slurm.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  service {
    'slurmctld':
      ensure => running,
      enable => true,
  }

  ###### Setup config files on the slaves nfsroot

  file { # Install lots of packages
    '/srv/nfsroot/root/install_packages.sh':
      ensure => file,
      source => "${::pupfiles}/borg/install_packages.sh",
      owner  => 'root',
      group  => 'root',
      mode   => '0744',
  }

  file { # enable aufs in modules
    '/srv/nfsroot/etc/initramfs-tools/modules':
      ensure => file,
      source => "${::pupfiles}/borg/modules",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file { # the actual aufs-script 
    '/srv/nfsroot/etc/initramfs-tools/scripts/aufs':
      ensure => file,
      source => "${::pupfiles}/borg/aufs",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file { # Load infiniband kernel modules
    '/etc/modules':
      ensure => file,
      source => "${::pupfiles}/borg/infiniband_modules",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file { # Load infiniband kernel modules
    '/srv/nfsroot/etc/modules':
      ensure => file,
      source => "${::pupfiles}/borg/infiniband_modules",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file { # fstab
    '/srv/nfsroot/etc/fstab':
      ensure => file,
      source => "${::pupfiles}/borg/fstab",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file { # ntp
    '/srv/nfsroot/etc/ntp.conf':
      ensure => file,
      source => '/etc/ntp.conf',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file { # slurm.conf
    '/srv/nfsroot/etc/slurm-llnl/slurm.conf':
      ensure => file,
      source => "${::pupfiles}/borg/slurm.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  # Enable nis
  file {
    '/srv/nfsroot/etc/yp.conf':
      ensure => file,
      source => "${::pupfiles}/borg/yp.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file {
    '/srv/nfsroot/etc/nsswitch.conf':
      ensure => file,
      source => "${::pupfiles}/borg/nsswitch.conf",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file {
    '/srv/nfsroot/etc/defaultdomain':
      ensure => file,
      source => '/etc/defaultdomain',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  # Hemdisk
  file {
    '/srv/nfsroot/etc/auto.master':
      ensure => file,
      source => "${::pupfiles}/borg/auto.master",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  # hosts
  file {
    '/srv/nfsroot/etc/hosts':
      ensure => file,
      source => '/etc/hosts',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  # munge key
  file {
    '/srv/nfsroot/etc/munge/munge.key':
      ensure => file,
      source => '/etc/munge/munge.key',
      owner  => 'munge',
      group  => 'munge',
      mode   => '0400',
  }
}
