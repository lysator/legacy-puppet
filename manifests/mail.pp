class mail (
  Boolean $server
) {
  if $server {
    include ::services::mailserver
  } else {
    include ::mailclient
  }
}

class mailclient {
  case $operatingsystem {
    'OpenSuSE': {
      include '::mail::opensuse'
      include '::mail::alpine'
    }
    default: {
      include '::mail::ssmtp'
      include '::mail::pine'
    }
  }

  file {
    '/var/mail':
      ensure  => symlink,
      target  => '/mp/mail',
      replace => true,
      force   => true;
  }
}

class mail::opensuse
{
  file {
    '/etc/sysconfig/mail':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/OpenSuSE.mail";
  }
}

class mail::alpine
{
  case $operatingsystem {
    'OpenSuSE': {
      package {
        ['alpine','alpine-branding-openSUSE',]:
          ensure => installed,
      }

      file {
        '/etc/pine.conf':
          ensure  => file,
          owner   => 'root',
          group   => 'root',
          mode    => '0644',
          content => template("${::pupfiles}/pine.conf");
      }
    }
  }
}

class mail::ssmtp
{
  package {
    'ssmtp':
      ensure => installed;
  }

  file {
    '/etc/ssmtp/ssmtp.conf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['ssmtp'],
      content => template("${::pupfiles}/ssmtp.conf.erb");
  }
}

class mail::pine
{
  file {
    '/etc/pine.conf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template("${::pupfiles}/pine.conf");
  }
}
