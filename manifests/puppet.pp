class puppetify
{
  $puppetpkg = $operatingsystem ? {
    'Debian' => ['puppet'],
    'Ubuntu' => ['puppet'],
    'Fedora' => 'puppet',
    'CentOS' => 'puppet',
    'Gentoo' => 'app-admin/puppet',
    'OpenSuSE' => 'rubygem-puppet',
  }

  package { $puppetpkg:
    ensure => installed,
  }

  file { 'opt':
    ensure => directory,
    name   => '/opt',
    mode   => '0644',
    tag    => puppet_config;
  }

  file { 'puppet':
    ensure  => directory,
    name    => '/opt/puppet',
    mode    => '0644',
    require => File['opt'],
    tag     => puppet_config;
  }

  file {
    'puppet-scripts':
      ensure => directory,
      name   => '/opt/puppet/scripts',
      mode   => '0644',
  }

  file {
    'randomsleep':
      ensure  => file,
      name    => '/opt/puppet/scripts/randomsleep',
      owner   => root,
      group   => root,
      mode    => '0755',
      source  => "${::pupfiles}/randomsleep",
      require => File['puppet-scripts'];
  }

  package { 'git':
    ensure => installed,
    tag    => puppet_config;
  }

  gitrepo { 'puppet-conf-git':
    source      => 'https://git.lysator.liu.se/lysator/legacy-puppet.git',
    destination => '/opt/puppet/conf.git',
    require     => [ File['puppet'],
    Package['git'] ],
    tag         => puppet_config;
  }

  if $pup_doinstall {
    cron {
      'run-puppet':
        ensure   => present,
        command  => "/opt/puppet/scripts/randomsleep; cd '${puptree}' && make -s --no-print-directory cron",
        month    => '1-12',
        monthday => '1-31',
        weekday  => '0-6',
        hour     => [0,4,8,12,16,20],
        minute   => '7',
        user     => 'root';
    }
    file {
      '/etc/init.d/puppetrun':
        ensure => absent,
    }

    service { 'puppet':
      ensure    => stopped,
      enable    => false,
      hasstatus => true,
      require   => Package[$puppetpkg];
    }
  }
}

class log_puppetrun
{
  $runlogdir  = "${pup_logdir}/${::hostname}"
  $runlogfile = "${runlogdir}/puppetrun"
  $timestamp  = lysstrftime('%Y-%m-%d  %H:%M:%S')

  file {
    $runlogdir:
      ensure => directory, owner => root, group => root, mode => '0755';
    $runlogfile:
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0644',
      require => File[$runlogdir];
  }
  ensure_line {
    'log_puppetrun':
      file     => $runlogfile,
      line     => sprintf("%s\t%s\t%s\t%s",
      $timestamp, $osdist, $puppetversion, $puphome),
      require  => File[$runlogfile],
      loglevel => info;
  }
}
