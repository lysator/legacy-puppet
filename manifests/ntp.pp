class ntp::chrony {
  package { 'ntp':
    ensure => absent,
  }

  $config_path = $facts['os']['name'] ? {
    'Ubuntu'   => '/etc/chrony/chrony.conf',
    'Debian'   => '/etc/chrony/chrony.conf',
    'OpenSuSE' => '/etc/chrony.conf',
    'CentOS'   => '/etc/chrony.conf',
    default    => '/etc/chrony.conf',
  }

  $service_name = $facts['os']['name'] ? {
    'Ubuntu'   => 'chrony',
    'Debian'   => 'chrony',
    'OpenSuSE' => 'chronyd',
    'CentOS'   => 'chronyd',
    default    => 'chronyd',
  }

  package { 'chrony':
    ensure => installed,
  }
  ~> file { $config_path:
    source => "${::pupfiles}/chrony.conf",
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
  }
  ~> service { $service_name:
    ensure => $::running,
    enable => true,
  }
}

class ntp::client {
  include ::ntp::chrony
}
