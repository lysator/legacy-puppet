class freebsd {
  # Packages
  package {
    [
      'ssmtp',
      'vim-tiny',
    ]:
      ensure => installed,
  }

  # Make periodic logs go to files instead of mail.
  file { '/etc/periodic.conf':
    ensure => file,
    source => "${::pupfiles}/freebsd/periodic.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # Redirect syslog to Lysator's server.
  file { '/etc/syslog.conf':
    ensure => file,
    source => "${::pupfiles}/freebsd/syslog.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # Configure ssmtp
  file { '/usr/local/etc/ssmtp/ssmtp.conf':
    ensure  => file,
    content => template("${::pupfiles}/freebsd/ssmtp.conf.erb"),
    owner   => 'root',
    group   => 'ssmtp',
    mode    => '0640',
  }

  file { '/etc/mail/mailer.conf':
    ensure => file,
    source => "${::pupfiles}/freebsd/mailer.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/auto_master':
    ensure => file,
    source => "${::pupfiles}/freebsd/auto_master",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # Disable sendmail.
  ensure_line { 'Disable sendmail':
    file => '/etc/rc.conf',
    line => 'sendmail_enable="NO"',
  }
  ensure_line { 'Disable sendmail submit':
    file => '/etc/rc.conf',
    line => 'sendmail_submit_enable="NO"',
  }
  ensure_line { 'Disable sendmail outbound':
    file => '/etc/rc.conf',
    line => 'sendmail_outbound_enable="NO"',
  }
  ensure_line { 'Disable sendmail msp':
    file => '/etc/rc.conf',
    line => 'sendmail_msp_queue_enable="NO"',
  }
}

class freebsd::workstation inherits freebsd {
  # Remove vim-tiny added by the base class since it conflicts with
  # the vim package.
  Package['vim-tiny'] {
    ensure => absent,
  }

  package {
    [
      'a2ps',
      'anki',
      'arandr',
      'aria2',
      'asciidoc',
      'audacity' ,
      'autoconf',
      'autocutsel',
      'automake',
      'awesome',
      'bash',
      'blender',
      'bspwm',
      'calibre',
      'cdecl',
      'cgoban',
      'chromium',
      'chrpath',
      'cinnamon',
      'cloc',
      'cmake',
      'codeblocks',
      'conky',
      'cowsay',
      'cscope',
      'curl',
      'darkice',
      'dmenu',
      'dosbox',
      'doxygen',
      'eclipse',
      'eclipse-cdt',
      'elinks',
      'enlightenment',
      'erlang',
      'fcitx-qt5',
      'feh',
      'figlet',
      'filezilla',
      'firefox',
      'fish',
      'fluxbox',
      'freecad',
      'fvwm',
      'gcc',
      'gconf2',
      'gdb',
      'geany',
      'global',
      'gnucash',
      'google-fonts',
      'gperf',
      'gtk-recordmydesktop',
      'guile2',
      'gwenview',
      'help2man',
      'hexchat',
      'hs-pandoc',
      'hs-xmonad',
      'htop',
      'i3',
      'i3status',
      'icecast',
      'inotify-tools',
      'iperf3',
      'josm',
      'jq',
      'k3b',
      'kdiff3',
      'kicad',
      'kicad-library-footprints',
      'kicad-library-symbols',
      'kicad-library-tmpl',
      'kmail',
      'ko-fcitx-hangul',
      'kontact',
      'konversation',
      'krita',
      'latex-biber',
      'libtool',
      'links',
      'love',
      'lua52',
      'luajit',
      'lynx',
      'lyx',
      'mate',
      'mate-desktop',
      'maven',
      'mc',
      'milkytracker',
      'minicom',
      'monodevelop',
      'moreutils',
      'mpc',
      'mpv',
      'mtr-nox11',
      'mumble',
      'mutt',
      'ncdu',
      'ncmpcpp',
      'netbeans',
      'newsboat',
      'nitrogen',
      'nmap',
      'nss',
      'numlockx',
      'octave',
      'okular',
      'openttd',
      'parallel',
      'password-store',
      'plasma5-plasma',
      'pms',
      'polybar',
      'protobuf',
      'pwgen',
      'py36-ipython',
      'py36-pip',
      'python3',
      'qemu',
      'qtcreator',
      'quassel',
      'racket',
      'rdesktop',
      'rebar3',
      'rubygem-puppet-lint',
      'redshift',
      'remmina',
      'rsync',
      'rxvt-unicode',
      'scons',
      'scrot',
      'source-highlight',
      'sqlite',
      'sqlitebrowser',
      'stellarium',
      'stow',
      'taskwarrior',
      'terminology',
      'terminus-font',
      'terminus-ttf',
      'texworks',
      'tf',
      'thunderbird',
      'tig',
      'tmux',
      'unrar',
      'uqm',
      'urlview',
      'vim',
      'vit',
      'vlock',
      'wine',
      'wireshark',
      'xbindkeys',
      'xdotool',
      'xfce4-terminal',
      'xmlto',
      'xorg',
      'xpenguins',
      'zathura',
      'zsh',
    ]:
      ensure => installed,
  }

  file { '/etc/nsswitch.conf':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/nsswitch.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/krb5.conf':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/krb5.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/sshd':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/pam.sshd",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/system':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/pam.system",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/su':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/pam.su",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/ssh/ssh_config':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/ssh_config",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/ssh/sshd_config':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/sshd_config",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/etc/slim.conf':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/slim.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/share/slim-lysator/themes/':
    ensure  => directory,
    recurse => true,
    source  => "${::pupfiles}/freebsd/workstation/slim.themes/",
    owner   => 'root',
    group   => 'wheel',
    mode    => '0644',
  }

  ensure_line { 'Enable D-Bus':
    file => '/etc/rc.conf',
    line => 'dbus_enable="YES"',
  }

  ensure_line { 'Set nisdomain':
    file => '/etc/rc.conf',
    line => 'nisdomainname="lysator"',
  }

  ensure_line { 'Enable nis client':
    file => '/etc/rc.conf',
    line => 'nis_client_enable="YES"',
  }

  ensure_line { 'Set nis flags':
    file => '/etc/rc.conf',
    line => 'nis_client_flags="-s -m -S lysator,nis.lysator.liu.se,nis-slave.lysator.liu.se"',
  }

  ensure_line { 'Enable autofs':
    file => '/etc/rc.conf',
    line => 'autofs_enable="YES"',
  }

  ensure_line { 'Enable slim login manager':
    file => '/etc/rc.conf',
    line => 'slim_enable="YES"',
  }

  file { '/etc/autofs/include_yp':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/include_yp",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0744',
  }

  file { '/etc/autofs/include':
    ensure => 'link',
    target => '/etc/autofs/include_yp',
  }

  # Set default Xorg keyboard layout to Swedish.
  file { '/usr/local/etc/X11/xorg.conf.d/keyboard.conf':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/keyboard.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # Create symlinks for shells to be compatible with the passwd file.
  file { '/bin/bash':
    ensure => 'link',
    target => '/usr/local/bin/bash',
  }

  file { '/bin/zsh':
    ensure => 'link',
    target => '/usr/local/bin/zsh',
  }

  file { '/bin/fish':
    ensure => 'link',
    target => '/usr/local/bin/fish',
  }

  file { '/lysator':
    ensure => 'link',
    target => '/mp/lysator',
  }

  file { '/var/mail':
    ensure => 'link',
    target => '/mp/mail',
    force => true,
  }
}

class freebsd::nvidia {
  ensure_line { 'Enable linux support':
    file => '/boot/loader.conf',
    line => 'linux_load="YES"',
  }
  ~> exec { '/sbin/kldload linux':
    refreshonly => true,
  }
  -> package {
    [
      'nvidia-driver',
    ]:
      ensure   => installed,
  }
  -> ensure_line { 'Enable nvidia driver':
    file => '/boot/loader.conf',
    line => 'nvidia-modeset_load="YES"',
  }
  ~> exec { '/sbin/kldload nvidia-modeset':
    refreshonly => true,
  }

  ensure_line { 'Fix broken vt-terminals':
    file => '/boot/loader.conf',
    line => 'hw.vga.textmode=1',
  }
}
