schedule {
  'updates':
    range => '3:00-5:00', period => daily, repeat => 1;
}


class keep_os_updated
{
  case $operatingsystem {
    'Debian': {
      include ::debian_keep_os_updated
    }
    'Ubuntu': {
      include ::debian_keep_os_updated
    }
    'OpenSuSE' : {
      include ::opensuse_keep_os_updated
    }
  }
}

class osfixes
{
  case $operatingsystem {
    'Debian': {
      include ::debian_fixes
    }
    'Ubuntu': {
      include ::ubuntu_fixes
    }
    'OpenSuSE': {
      include ::opensuse_fixes
    }
  }
  include ::keep_os_updated
  include ::general_environment
}

class osfixes_workstations
{
  case $operatingsystem {
    'Debian': {
      include ::ubuntu_usb_udev
    }
  }
}

class osfixes_arm
{
  case $operatingsystem {
    'Debian': {
      include ::raspbian_fixes
    }
  }

}

class general_environment {
  file {
    '/etc/profile.d/lysator-env.sh':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/profile.d/lysator-env.sh",
  }
}
