class caspian {
  $proxmox = true
  file {
    '/etc/sysctl.conf':
      ensure => file,
      source => "${::pupfiles}/sysctl.conf-caspian",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file {
    '/etc/modprobe.d/aliases-bond.conf':
      ensure => file,
      source => "${::pupfiles}/network/interfaces/aliases-bond.conf-caspian",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  package {
    [
      'fail2ban',
      'ifenslave',
      'infiniband-diags',
      'libmlx4-1',
    ]:
      ensure => 'installed',
  }

  file {
    '/etc/fail2ban/jail.local':
      ensure  => file,
      source  => "${::pupfiles}/fail2ban/caspian-jail.local",
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['fail2ban'],
  }

  file {
    '/etc/fail2ban/filter.d/proxmox.conf':
      ensure  => file,
      source  => "${::pupfiles}/fail2ban/filter.d/proxmox.conf",
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['fail2ban'],
  }

  service { 'fail2ban':
    ensure  => running,
    require => [
      Package['fail2ban'],
      File['/etc/fail2ban/filter.d/proxmox.conf'],
      File['/etc/fail2ban/jail.local'],
    ],
  }

#  file {
#    '/etc/iscsi/iscsid.conf':
#      ensure => file,
#      source => "${::pupfiles}/iscsi/caspian-iscsid.conf",
#      owner => 'root',
#      group => 'root',
#      mode => '0600',
#  }

}
