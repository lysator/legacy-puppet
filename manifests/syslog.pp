class syslog
{
  $syslog_conf = $operatingsystem ? {
    'Debian' => '/etc/rsyslog.conf',
    'Ubuntu' => '/etc/rsyslog.conf',
    'centos' => '/etc/rsyslog.conf',
    'OpenSuSE' => '/etc/rsyslog.conf',
    default => '/etc/syslog.conf'
  }

  $syslog_source = $operatingsystem ? {
    'Debian' => "${::pupfiles}/rsyslogd.conf",
    'Ubuntu' => "${::pupfiles}/rsyslogd.conf",
    'centos' => "${::pupfiles}/rsyslogd.conf",
    'OpenSuSE' => "${::pupfiles}/rsyslogd.conf",
  }

  $syslog = $operatingsystem ? {
    'Debian' => 'rsyslog',
    'Ubuntu' => 'rsyslog',
    'centos' => 'rsyslog',
    'OpenSuSE' => 'rsyslog',
    default => 'syslogd'
  }

  case $operatingsystem {
    'OpenSuSE' : {
      package {
        'rsyslog':
          ensure => installed,
      }
    }

  }

  file {
    'syslog.conf':
      ensure => file,
      name   => $syslog_conf,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => $syslog_source;
  }

  service {
    $syslog:
      ensure    => $running,
      enable    => true,
      pattern   => 'syslog',
      subscribe => File['syslog.conf'],
      require   => Class['network'];
  }

  cron {
    'syslog_mark':
      ensure  => present,
      command => '/usr/bin/logger mark',
      user    => root,
      minute  => 43,
  }
}
