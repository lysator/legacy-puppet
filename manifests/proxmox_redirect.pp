class proxmox_redirect {

  package {
    [
      'nginx',
    ]:
      ensure => 'installed',
  }

  -> file {
    '/etc/nginx/sites-available/proxmox-gui':
      ensure => file,
      source => "${::pupfiles}/nginx/sites-available/proxmox-gui",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }
  file {
    '/etc/nginx/sites-enabled/default':
      ensure => purged,
  }

  ~> exec {
    'enable-site':
      command => 'ln -sf /etc/nginx/sites-available/proxmox-gui /etc/nginx/sites-enabled/',
      path    => '/bin',
  }

  exec {
    'generate-dhparam-file':
      command => 'openssl dhparam -out /etc/nginx/ssl/dhparam.pem 4096',
      path    => '/usr/bin',
      unless  => 'test -f /etc/nginx/ssl/dhparam.pem',
  }

  service {
    'nginx':
      ensure  => running,
      require => [
        Package['nginx'],
        File['/etc/nginx/sites-available/proxmox-gui'],
      ],
  }
}
