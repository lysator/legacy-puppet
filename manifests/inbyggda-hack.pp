class inbyggda_hack
{

  package {
    [
      'genromfs',
      'isc-dhcp-server',
      'libcomedi-dev',
      'nfs-kernel-server',
      'u-boot-tools',
    ]:
      ensure  => installed,
      require => Class['network'];
  }

  file {
    '/embedded':
      ensure => directory,
      owner  => 'root',
      group  => 'inbyggda-hack',
      mode   => '2664';
    '/export':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0644';
    '/export/embedded':
      ensure  => directory,
      owner   => 'root',
      group   => 'inbyggda-hack',
      mode    => '4664',
      require => File['/export'];
    '/embedded/dhcpd.hosts':
      ensure => file,
      owner  => 'dhcpd',
      group  => 'inbyggda-hack',
      mode   => '0664';
    '/etc/apparmor.d/local/usr.sbin.dhcpd':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/inbyggda-hack/usr.sbin.dhcpd",
      require => Package['isc-dhcp-server'],
      notify  => [ Service['isc-dhcp-server'],
      Service['apparmor'] ];
    '/etc/default/isc-dhcp-server':
      ensure  => 'file',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/inbyggda-hack/isc-dhcp-server",
      require => Package['isc-dhcp-server'],
      notify  => Service['isc-dhcp-server'];
    '/etc/exports':
      ensure  => 'file',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template("${::pupfiles}/inbyggda-hack/exports.erb"),
      require => [ Package['nfs-kernel-server'],
      File['/export/embedded'] ],
      notify  => Service['nfs-kernel-server'];
    '/etc/dhcp/dhcpd.conf':
      ensure  => 'file',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template("${::pupfiles}/inbyggda-hack/dhcpd.conf.erb"),
      require => Package['isc-dhcp-server'],
      notify  => Service['isc-dhcp-server'];
  }

  service {
    'isc-dhcp-server':
      ensure  => $running,
      enable  => true,
      require => [ File['/etc/default/isc-dhcp-server'],
        File['/etc/dhcp/dhcpd.conf'],
        File['/embedded/dhcpd.hosts'],
      File['/etc/apparmor.d/local/usr.sbin.dhcpd'] ];
    'nfs-kernel-server':
      ensure    => $running,
      hasstatus => true,
      enable    => true,
      require   => File['/etc/exports'];
    'apparmor':
      ensure  => $running,
      enable  => true,
      require => File['/etc/apparmor.d/local/usr.sbin.dhcpd'];
  }
}
