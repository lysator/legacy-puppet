class nrpe
{
  package {
    'nrpe':
      ensure => installed,
      name   => $operatingsystem? {
        'Debian'   => 'nagios-nrpe-server',
        'Ubuntu'   => 'nagios-nrpe-server',
        'centos'   => 'nrpe',
        'OpenSuSE' => 'nrpe',
      };
  }

  file {
    '/etc/nagios':
      ensure => directory,
      owner  => root,
      group  => root,
      mode   => '0755';
    'nrpe.cfg':
      ensure  => file,
      name    => '/etc/nagios/nrpe.cfg',
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      require => [ Package['nrpe'],
      File['/etc/nagios'] ],
      notify  => Service['nrpe'],
      content => template("${::pupfiles}/nrpe.cfg.erb");
    'nrpe_local.cfg':
      ensure  => file,
      name    => '/etc/nagios/nrpe_local.cfg',
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      require => [ Package['nrpe'],
      File['/etc/nagios'] ],
      notify  => Service['nrpe'],
      source  => [ "${::pupfiles}/nrpe_local.cfg-${::hostname}",
        "${::pupfiles}/nrpe_local.cfg-${operatingsystem}",
      "${::pupfiles}/nrpe_local.cfg" ];
    '/etc/default/nagios-nrpe-server':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      require => Package['nrpe'],
      notify  => Service['nrpe'],
      content => template("${::pupfiles}/nagios-nrpe-server.daemon.conf.erb");
  }

  service {
    'nrpe':
      ensure  => $running,
      enable  => true,
      name    => $operatingsystem ? {
        'Debian'   => 'nagios-nrpe-server',
        'Ubuntu'   => 'nagios-nrpe-server',
        'centos'   => 'nrpe',
        'OpenSuSE' => 'nrpe',
      },
      pattern => 'nrpe',
      require => Package['nrpe'];
  }
}
