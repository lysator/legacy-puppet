# Backup users for use by remote borg-backup
define proxdisk::backup_user (
  String $ssh_key,
  String $user = $name,
  Optional[String] $homedir = "/backup/vm/${user}",
  Optional[Integer] $uid = undef,
) {
  user {
    "${user}-backup" :
      home => $homedir,
      uid  => $uid,
  }
  -> file { [ $homedir, "/backup/vm/${user}/.ssh",]:
    ensure => directory,
    owner  => "${user}-backup",
  }
  ~> file { "${homedir}/.ssh/authorized_keys":
    ensure  => present,
    owner   => "${user}-backup",
    mode    => '0400',
    content => $ssh_key,
  }

  exec { "borg init for ${user}":
    command => "borg init --encryption none ${homedir}/backup",
    cwd     => $homedir,
    creates => "${homedir}/backup",
    user    => "${user}-backup",
    path    => ['/usr/bin'],
  }
}
