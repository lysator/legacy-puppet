class nis_generic
{
  include ::nis_settings

  case $operatingsystem {
    'opensuse' : {
      package { ['yast2-nis-client',]:
        ensure => installed,
      }
    }
  }

  package {
    'nis':
      ensure => installed,
      name   => $operatingsystem ? {
        'Debian'   => 'nis',
        'Ubuntu'   => 'nis',
        'CentOS'   => 'ypbind',
        'opensuse' => 'ypbind',
      };
  }

  service {
    'nis':
      ensure    => $running,
      name      => $operatingsystem ? {
        'CentOS'   => 'ypbind',
        'Ubuntu'   => 'nis',
        'opensuse' => 'ypbind',
        'Debian'   => $facts['os']['release']['major'] ? {
          '10'    => 'nis',
          default => 'ypserv',
        },
        default    => 'nis',
      },
      enable    => true,
      pattern   => 'ypbind',
      hasstatus => $operatingsystem ? {
        'Debian' => $facts['os']['release']['major'] ? {
          '10'    => false,
          default => true,
        },
        default  => true
      },
      # Workaround rpcbind not starting properly on Ubuntu 16.04
      # https://bugs.launchpad.net/ubuntu/+source/rpcbind/+bug/1558196
      # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=805167
      require   => $operatingsystem ? {
        'Ubuntu' => [ Class['network'], Package['nis'], Class['nis_settings'], File['yp.conf'], File['/lib/systemd/system/rpcbind.socket'] ],
        default  => [ Class['network'], Package['nis'], Class['nis_settings'], File['yp.conf'] ],
      },
      subscribe => [ File['yp.conf'] ];
  }

  if $operatingsystem == 'Ubuntu' {
    file { '/lib/systemd/system/rpcbind.socket':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/rpcbind.socket",
    }
  }
}

class nis_settings
{
  include $operatingsystem? {
    'CentOS' => 'nis_settings_centos',
    default => 'nis_settings_default'
  }
}

class nis_settings_default {
  file {
    '/etc/defaultdomain':
      ensure  => file,
      name    => '/etc/defaultdomain',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => 'lysator';
  }
}

class nis_settings_centos {
  file {
    '/etc/sysconfig/network':
      ensure  => file,
      name    => '/etc/sysconfig/network',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template("${::pupfiles}/sysconfig-network-CentOS.erb");
  }
}

class nis_client_generic inherits nis_generic
{
  file {
    'yp.conf':
      ensure  => file,
      name    => $operatingsystem? {
        'Debian'   => '/etc/yp.conf',
        'Ubuntu'   => '/etc/yp.conf',
        'centos'   => '/etc/yp.conf',
        'opensuse' => '/etc/yp.conf',
      },
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['nis'],
      source  => [ "${::pupfiles}/yp.conf-${operatingsystem}",
      "${::pupfiles}/yp.conf" ];
  }
}

class nis_limited
{
  file {
    '/etc/nsswitch.conf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      source  => [ "${::pupfiles}/nsswitch.conf-limited-${operatingsystem}",
      "${::pupfiles}/nsswitch.conf-limited" ],
      require => Service['nis'];

    '/roots':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0770';
  }

  ensure_line {
    'passwd_root':
      file    => '/etc/passwd',
      line    => $has_homedirs ? {
        true    => '+@root:x:::::',
        default => '+@root:x::::/roots:',
      },
      before  => Ensure_line['passwd_nouser'],
      require => File['/etc/nsswitch.conf'];

    'passwd_nouser':
      file    => '/etc/passwd',
      line    => '+:x:::::/bin/nologin',
      require => File['/etc/nsswitch.conf'];
  }
}

class nis_client_limited inherits nis_client_generic
{
  include ::nis_limited
}

class nis_client_user inherits nis_client_generic
{
  file {
    '/etc/nsswitch.conf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => ["${::pupfiles}/nsswitch.conf-user-${operatingsystem}",
      "${::pupfiles}/nsswitch.conf-user" ],
      require => Service['nis'];
  }
}

class nis_slave inherits nis_generic {
  #Currently neccessary to run ypinit -s nis manually to initialize

  include ::nis_limited

  file {
    'yp.conf':
      ensure  => file,
      name    => '/etc/yp.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      require => Package['nis'],
      content => template("${::pupfiles}/nis/slave/yp.conf.erb");
    '/etc/ypserv.securenets':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      require => Package['nis'],
      notify  => Service['nis'],
      source  => "${::pupfiles}/nis/slave/ypserv.securenets";
    '/etc/default/nis':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      source  => "${::pupfiles}/nis/slave/service-config",
      require => Package['nis'],
      notify  => Service['nis'];
  }
}
