class claptrap {
  include ::freebsd::nvidia

  file { '/usr/local/etc/X11/xorg.conf':
    ensure => file,
    source => "${::pupfiles}/freebsd/workstation/claptrap-xorg.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # These two rules group the internal speaker with the headphones.
  # This allows FreeBSD to mute the speaker when headphones are
  # plugged in.
  # Make sure that hw.snd.default_unit is set to the internal speaker,
  # the different audio devices can be seen by executing "cat
  # /dev/sndstat".
  ensure_line { 'Grouping of speaker':
    file => '/boot/device.hints',
    line => 'hint.hdac.1.cad2.nid21.config="as=2 seq=0"',
  }

  ensure_line { 'Grouping of headphones':
    file => '/boot/device.hints',
    line => 'hint.hdac.1.cad2.nid27.config="as=2 seq=15"',
  }
}

