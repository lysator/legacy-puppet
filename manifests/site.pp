# $puphome passed in by environment variable FACTER_PUPHOME
$pupfiles = "${::puphome}/Files"
$pupsecrets = '/root/puppet_secrets'

# The normal location where we check out Puppet, and especially where
# SKARP is located:
$puptree = '/opt/puppet/conf.git'

$osdist = "${::operatingsystem}-${::lsbdistrelease}"

# We set $running to stopped when Puppet is run during kickstart, and
# use $running in every service definition when we would normally have
# specified 'ensure => running', so services aren't started while the
# machine is installing.
$running = $pup_rundaemons ? { false => 'stopped', default => running, }

if ($operatingsystem == 'Ubuntu') and ($operatingsystemrelease == '16.04') {
  # Workaround for https://bugs.launchpad.net/ubuntu/+source/puppet/+bug/1457957
  # Can be removed when a new enough version of puppet gets released in Ubuntu
  Service {
    provider => 'debian',
  }
}

$own_dns = false

class lysatorbase (
  Boolean $mailserver = false
) {
  include ::network
  include ::syslog
  class { '::kerberos': ldap => false }
  class { '::mail': server => $mailserver }
  include ::nrpe
  include ::ntp::client
  include ::sysadm_environment
  include ::osfixes

  include ::puppetify
  include ::log_puppetrun
}

class lysatorbase_ldap (
  Boolean $mailserver = false
) {
  include ::network
  include ::syslog
  class { '::kerberos': ldap => true }
  class { '::mail': server => $mailserver }
  include ::nrpe
  include ::ntp::client
  include ::sysadm_environment
  include ::osfixes

  include ::puppetify
  include ::log_puppetrun
}

class services_arm
{
  include ::network
  include ::syslog
  class { '::kerberos': ldap => false }
  class { '::mail': server => false }
  include ::nrpe
  include ntp::client
  include ::sysadm_environment

  include ::puppetify
  include ::log_puppetrun

  include ::nis_client_limited
}

class services
{
  include ::lysatorbase
  include ::nis_client_limited
}

# Work in progress
class services_ldap (
  Boolean $cache_ldap = false
) {
  include ::lysatorbase_ldap
  if $cache_ldap {
    include ::ldap_client_limited_cached
  } else {
    include ::ldap_client_limited
  }
}

class proxmox
{
  $proxmox = true
  case $::lsbdistrelease {
    '7.7': {
      exec { '7.7_remove_no_subscription_notice':
        command => "/bin/sed -i \"s/data.status !== 'Active'/false/\" /usr/share/pve-manager/ext4/pvemanagerlib.js",
        onlyif  => "/bin/grep \"data.status !== 'Active'\" /usr/share/pve-manager/ext4/pvemanagerlib.js",
      }
      file {
        'socat_ipv6_forwarder_for_spice_config':
          path    => '/etc/default/socat',
          content => "OPTIONS=\"TCP6-LISTEN:3128,bind=[${::ipaddress6}],fork,reuseaddr TCP4:localhost:3128\"",
      }
      file {
        'debian_socat_init':
          path   => '/etc/init.d/socat',
          source => "${::pupfiles}/socat.init",
      }
      package {
        'socat':
          ensure => installed,
          name   => 'socat',
      }
      service {
        'socat_ipv6_forwarder_for_spice':
          ensure    => $::running,
          name      => 'socat',
          enable    => true,
          require   => [Package['socat'], File['debian_socat_init']],
          subscribe => File['socat_ipv6_forwarder_for_spice_config'],
      }
    }
    default: {
      exec { 'remove_no_subscription_notice':
        command => "/bin/sed -i \"s/data.status !== 'Active'/false/\" /usr/share/pve-manager/ext6/pvemanagerlib.js",
        onlyif  => "/bin/grep \"data.status !== 'Active'\" /usr/share/pve-manager/ext6/pvemanagerlib.js",
      }
    }
  }
  include ::services
}

class inhysningar
{
  include ::network
  include ::syslog
  include ntp::client
  # include inhysning_iptable
  # or whatever replaces iptables
  include ::puppetify
  include ::log_puppetrun

  #user { 'lysroot':
  #  ensure  => present,
  #  home  => '/opt/lysroot',
  #  shell  => '/bin/bash',
  #  uid  => '0',
  #  gid  => '0',
  #  password => '$6$K3xOgbwYdO3bQml9$vRbq9kVY8XHwrgbj2cmas/tFLEhyuXEJIQbkgCjxzSTGEf2jUhIAmt44tO0QW7.jjXODuHcyjuKzWPbkDFW5Y0',
  #}
}

class user_accessible
{
  include ::lysatorbase
  include ::user_environment
  include ::development_environment
  include ::autofs
  include ::printclient
  include ::nis_client_user
}

class user_accessible_ldap
{
  include ::lysatorbase
  include ::user_environment
  include ::development_environment
  include ::autofs_ldap
  include ::printclient
  include ::ldap_client
}

class cpu_servers
{
  include ::user_accessible
}

class cpu_servers_ldap
{
  include ::user_accessible_ldap
}

class workstations
{
  include ::user_accessible
  include ::workstation
  include ::osfixes_workstations
}

node 'slartibartfast.lysator.liu.se',
{
  include ::workstations
}

node 'gibraltar.lysator.liu.se'
{
  include ::services
  include ::services::gitlab
#  include ::services::temperature_foo
}

node 'radius.lysator.liu.se'
{
  include ::services
  include ::services::radius
}

node 'admin.lysator.liu.se'
{
  include ::services
  include ::services::admin
}

node 'hermod.lysator.liu.se'
{
  $has_homedirs = true
  $own_dns = true
  class { '::lysatorbase_ldap': mailserver => true, }
  class { '::services_ldap': cache_ldap => true, }
}

node 'tanenbaum.lysator.liu.se'
{
  include ::services_ldap
  include ::services::jabber
}

node 'medreg.lysator.liu.se'
{
  include ::services
  include ::services::medreg
}

node 'jskom.lysator.liu.se'
{
  include ::services
  include ::services::jskom
}
