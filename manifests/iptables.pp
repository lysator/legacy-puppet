class ubuntu_debian_iptables
{
  package {
    [
      'iptables', 'iptables-persistent',
    ]:
      ensure  => installed,
      require => Class['network'];
  }

  file {
    'ubuntu-debian-iptables-peristent-v4':
      ensure => 'file',
      source => "${::pupfiles}/iptables/${::hostname}.v4",
      path   => '/etc/iptables/rules.v4',

      group  => 'root',
      owner  => 'root',
      mode   => '0644',

      notify => Service['iptables-persistent'];
  }

  file {
    'ubuntu-debian-iptables-peristent-v6':
      ensure => 'file',
      source => "${::pupfiles}/iptables/${::hostname}.v6",
      path   => '/etc/iptables/rules.v6',

      group  => 'root',
      owner  => 'root',
      mode   => '0644',

      notify => Service['iptables-persistent'];
  }

  service {
    'iptables-persistent':
      ensure     => running,
      enable     => true,
      hasstatus  => false,
      hasrestart => true;
  }
}

# Intended for inhysning
class inhysning_iptable
{
  if ($operatingsystem == 'Ubuntu') or ($operatingsystem == 'Debian') {
    package {
      [
        'iptables', 'iptables-persistent',
      ]:
        ensure  => installed,
        require => Class['network'];
    }

    file {
      'inhysning-iptables-peristent-v4':
        ensure => 'file',
        source => "${::pupfiles}/iptables/inhysning.v4",
        path   => '/etc/iptables/rules.v4',

        group  => 'root',
        owner  => 'root',
        mode   => '0644',

        notify => Service['iptables-persistent'];
    }

    file {
      'inhysning-iptables-peristent-v6':
        ensure => 'file',
        source => "${::pupfiles}/iptables/inhysning.v6",
        path   => '/etc/iptables/rules.v6',

        group  => 'root',
        owner  => 'root',
        mode   => '0644',

        notify => Service['iptables-persistent'];
    }

    service {
      'iptables-persistent':
        ensure     => running,
        enable     => true,
        hasstatus  => false,
        hasrestart => true;
    }
  }
}
