class debian_package_repositories
{
  exec {
    'apt-update':
      command     => '/usr/bin/apt-get update',
      refreshonly => true,

      # Some repositories (e.g. gitlab) are HTTPS-only
      require     => Package['apt-transport-https'],
  }

  exec {
    'apt-key-update':
      command     => '/usr/bin/apt-key update',
      refreshonly => true,
      notify      => Exec['apt-update'];
  }

  package {
    'apt-transport-https':
      ensure => installed,
  }

  file {
    '/etc/apt/sources.list':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      content => template("${::pupfiles}/sources.list-Debian.erb"),
      notify  => Exec['apt-update'],
      tag     => pkgrepo;
  }

  file {
    'local-apt-keys':
      path    => '/etc/apt/trusted.gpg.d/',
      source  => "${::pupfiles}/apt-keys-debian/",
      recurse => true,
      purge   => true,
      ignore  => [
        'debian-archive-*.gpg',
        'debian-archive-*.asc',
      ],
      owner   => 'root',
      group   => 'root',
      mode    => '0644';
  }

  recursive_directory {
    'local-apt-sources':
      source_dir => "${::pupfiles}/apt-sources-debian/",
      dest_dir   => '/etc/apt/sources.list.d/',
      owner      => 'root',
      group      => 'root',
      dir_mode   => '0755',
      file_mode  => '0644',
      notify     => Exec['apt-key-update'],
      require    => File['local-apt-keys'];
  }

  file {
    '/etc/apt/sources.list.d/':
      ensure  => 'directory',
      recurse => true,
      purge   => true,
  }
}

class debian_keep_os_updated
{
  cron {
    'update-os':
      command  => '/opt/puppet/scripts/randomsleep; apt-get -qq update',
      user     => 'root',
      month    => '1-12',
      monthday => '1-31',
      weekday  => '0-7',
      hour     => '3',
      minute   => '33';
  }
}

class debian_no_networkmanager
{
  if !$proxmox {
    package {
      [
        'network-manager', 'gnome-network-manager',
      ]:
        #    require => Class["network"],
        ensure => absent,
        notify => Service['interfaces'];
    }
  }
}

class debian_preseed
{
  file {
    '/var/cache/local':
      ensure => 'directory',
      group  => 'root',
      owner  => 'root',
      mode   => '0644',
      before => Class['network'];
  }
  file {
    '/var/cache/local/preseeding':
      ensure  => 'directory',
      group   => 'root',
      owner   => 'root',
      mode    => '0644',
      require => File['/var/cache/local'],
  }
}

class debian_pathsh
{
  file {
    '/etc/path.sh':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
  }
}

class debian_no_debsecan {
  package { 'debsecan':
    ensure => absent,
  }
}

class debian_remove_default_motd {
  exec { 'printf "" > /etc/motd':
    path   => ['/bin/', '/usr/bin/'],
    onlyif => 'grep "The programs included with the Debian GNU/Linux system are free software" /etc/motd',
  }
}

class debian_fixes
{
  include ::debian_no_networkmanager
  include ::debian_package_repositories
  include ::debian_preseed
  include ::debian_pathsh
  include ::debian_no_debsecan
  include ::debian_remove_default_motd
}
