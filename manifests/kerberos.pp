class pam::linux
{
  $pam_krb5 = $operatingsystem ? {
    'Debian' => 'libpam-krb5',
    'Ubuntu' => 'libpam-krb5',
    'centos' => 'pam_krb5',
    'OpenSuSE' => 'pam_krb5',
  }

  package {
    [ $pam_krb5 ]:
      ensure => installed,
  }

  case $operatingsystem {
    'Debian', 'Ubuntu', 'OpenSuSE' : {
      include ::pam::linux::debian
    }
    'CentOS' : {
      include ::pam::linux::centos
    }
  }
}

class pam::linux::debian
{
  file {
    '/etc/pam.d/common-auth':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/pam/common-auth-${operatingsystem}",
      require =>[  File['krb5.conf'], Package[$pam::linux::pam_krb5] ];
    '/etc/pam.d/common-account':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/pam/common-account-${operatingsystem}",
      require =>[  File['krb5.conf'], Package[$pam::linux::pam_krb5] ];
    '/etc/pam.d/common-session':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/pam/common-session-${operatingsystem}",
      require =>[  File['krb5.conf'], Package[$pam::linux::pam_krb5] ];
    '/etc/pam.d/common-password':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/pam/common-password-${operatingsystem}",
      require =>[  File['krb5.conf'], Package[$pam::linux::pam_krb5] ];
  }
}

class pam::linux::centos
{
  file {
    '/etc/pam.d/system-auth':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/pam/system-auth-${operatingsystem}",
      require =>[  File['krb5.conf'], Package[$pam::linux::pam_krb5] ];
    '/etc/pam.d/password-auth':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/pam/password-auth-${operatingsystem}",
      require =>[  File['krb5.conf'], Package[$pam::linux::pam_krb5] ];
    '/etc/pam.d/fingerprint-auth':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => "${::pupfiles}/pam/fingerprint-auth-${operatingsystem}",
      require =>[  File['krb5.conf'], Package[$pam::linux::pam_krb5] ];
  }

  rh_sysconfig {
    'authconfig_kerberos':
      subsystem => 'authconfig',
      setting   => 'USEKERBEROS',
      value     => 'yes';
    'authconfig_nis':
      subsystem => 'authconfig',
      setting   => 'USENIS',
      value     => 'yes';
  }
}

class kerberos (
  Boolean $ldap
) {
    class { '::kerberos::linux': ldap => $ldap }
}

class kerberos::linux (
  Boolean $ldap
) {
  $krb5 = $operatingsystem ? {
    'Debian' => 'krb5-user',
    'Ubuntu' => 'krb5-user',
    'centos' => 'krb5-libs',
    'OpenSuSE' => 'krb5-client',
  }

  $krb5_conf = $operatingsystem ? {
    'Debian' => '/etc/krb5.conf',
    'Ubuntu' => '/etc/krb5.conf',
    'centos' => '/etc/krb5.conf',
    'OpenSuSE' => '/etc/krb5.conf',
    default => '/etc/krb5/krb5.conf',
  }

  $krb5_conf_src = $ldap ? {
    true => "${::pupfiles}/krb5.conf-ldap",
    default => "${::pupfiles}/krb5.conf",
  }

  package {
    [ $krb5 ]:
      ensure => installed,
  }

  file {
    'krb5.conf':
      ensure  => file,
      name    => $krb5_conf,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => [ Class['network'],
      Package[$krb5] ],
      source  => $krb5_conf_src,
  }

  include ::pam::linux
}
