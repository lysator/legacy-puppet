class identd
{
  file {
    '/etc/oidentd.conf':
      ensure => file,
      notify => Service['oidentd'],
      source => "${::pupfiles}/oidentd.conf",
      owner  => root,
      group  => root,
      mode   => '0444';
  }

  service {
    'oidentd':
      ensure  => running,
      enable  => true,
      require => Package['oidentd'];
  }

  package {
    'oidentd':
      ensure => latest;
  }
}
