class zfsonlinux_debian
{
  exec {
    'modprobe-zfs':
      command     => '/sbin/modprobe zfs',
      refreshonly => true;
  }
  package {
    'debian-zfs':
      ensure => installed,
      name   => 'debian-zfs',
      notify => Exec['modprobe-zfs'],
  }
  file {
    'zed.rc':
      ensure => file,
      name   => '/etc/zfs/zed.d/zed.rc',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/zfsonlinux/zed.rc",
  }
  service {
    'zfs-event-deamon':
      ensure    => running,
      name      => 'zfs-zed',
      enable    => true,
      subscribe => File['zed.rc'],
  }
}
