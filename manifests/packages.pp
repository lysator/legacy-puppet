class user_environment::ubuntu
{
  include '::user_environment::debian_ubuntu_common'

  package {
    [
      'enigmail',
      'ffmpeg',
      'finger',
      'firefox-locale-sv',
      'flashplugin-installer',
      'language-pack-en',
      'language-pack-gnome-en',
      'language-pack-gnome-sv',
      'language-pack-kde-en',
      'language-pack-kde-sv',
      'language-pack-sv',
      'ncmpcpp',
      'pv',
      'thunderbird',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class user_environment::debian
{
  include '::user_environment::debian_ubuntu_common'
  package {
    [
      'bbdb',
      'empathy',
      'ffmpeg',
      'flashplugin-nonfree',
      'gnugo',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class user_environment::opensuse
{
  package {
    [
      'emacs',
      'fish',
      'htop',
      'sshfs',
      'texlive-scheme-full',
      'vim',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class user_environment::debian_ubuntu_common
{
  package {
    [
      'alpine',
      'aspell-en',
      'aspell-sv',
      'csh',
      'dot2tex',
      'dvtm',
      'emacs',
      'erlang-mode',
      'graphviz',
      'konqueror',
      'lyskom-elisp-client',
      'ncmpc',
      'picocom',
      'pmount',
      'python-pip',
      'python3-pip',
      'python3-venv',
      'recode',
      'sshfs',
      'telnet',
      'texlive-full',
      'tree',
      'vim',
      'xchm',
      'zstd',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class user_environment::centos
{
  package {
    [
      'alpine',
      'aspell-en',
      'dvtm',
      'emacs',
      'emacs-erlang',
      'fuse-sshfs',
      'graphviz',
      'picocom',
      'recode',
      'telnet',
      'texlive',
      'tree',
      'vim-enhanced',
      'xchm',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class user_environment
{
  include "user_environment::${::operatingsystem}"
}

class development_environment::ubuntu
{
  include "development_environment::${::operatingsystem}::${::architecture}"
  include '::development_environment::debian_ubuntu_common'
  package {
    [
      'clang',
      'clang-format',
      'g++',
      'gcc',
      'ghc',
      'ghc-doc',
      'ghc-prof',
      'libclang-dev',
      'libclang1',
      'libfuse-dev',
      'libghc-curl-dev',
      'libghc-curl-doc',
      'libghc-curl-prof',
      'libghc-diff-dev',
      'libghc-diff-doc',
      'libghc-diff-prof',
      'libghc-haxml-dev',
      'libghc-haxml-prof',
      'libghc-hscolour-dev',
      'libghc-hscolour-doc',
      'libghc-hscolour-prof',
      'libghc-http-dev',
      'libghc-http-prof',
      'libghc-irc-dev',
      'libghc-irc-doc',
      'libghc-mtl-prof',
      'libghc-network-doc',
      'libghc-network-prof',
      'libghc-parsec3-prof',
      'libghc-regex-base-prof',
      'libghc-regex-posix-dev',
      'libghc-regex-posix-prof',
      'libghc-safe-dev',
      'libghc-safe-doc',
      'libghc-safe-prof',
      'libgnutls28-dev',
      'lldb',
      'llvm',
      'llvm-dev',
      'llvm-runtime',
      'openocd',
      'ruby-dev',
      'sloccount',
    ]:
      ensure  => latest,
      require => Class['network'],
  }
}

class development_environment::ubuntu::amd64
{
  package {
    [
      'lib32ncurses5-dev',
      'lib32z1-dev',
      'libasound2-dev:i386',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class development_environment::ubuntu::i386
{
}

class development_environment::debian
{
  include '::development_environment::debian_ubuntu_common'

  package {
    [
      'ghc',
      'ghc-doc',
      'ghc-prof',
      'libghc-curl-dev',
      'libghc-curl-doc',
      'libghc-curl-prof',
      'libghc-diff-dev',
      'libghc-diff-doc',
      'libghc-diff-prof',
      'libghc-haxml-dev',
      'libghc-haxml-prof',
      'libghc-hscolour-dev',
      'libghc-hscolour-doc',
      'libghc-hscolour-prof',
      'libghc-http-dev',
      'libghc-http-prof',
      'libghc-irc-dev',
      'libghc-irc-doc',
      'libghc-mtl-prof',
      'libghc-network-doc',
      'libghc-network-prof',
      'libghc-regex-base-prof',
      'libghc-regex-posix-dev',
      'libghc-regex-posix-prof',
      'libghc-safe-dev',
      'libghc-safe-doc',
      'libghc-safe-prof',
      'libimlib2-dev',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class development_environment::debian_ubuntu_common
{
  include '::development_environment::linux_common'

  package {
    [
      'autoconf-doc',
      'autotools-dev',
      'bison-doc',
      'build-essential',
      'docbook',
      'docbook-dsssl',
      'docbook-to-man',
      'docbook-utils',
      'docbook-xsl',
      'docbook-xsl-doc-html',
      'freeglut3-dev',
      'g++-multilib',
      'gcc-avr',
      'gdb-avr',
      'git-doc',
      'git-extras',
      'glade',
      'haskell-doc',
      'libcapi20-3',
      'libcapi20-dev',
      'libcups2-dev',
      'libexif-dev',
      'libfribidi-dev',
      'libgcrypt11-dev',
      'libgif-dev',
      'libgpg-error-dev',
      'libgphoto2-dev',
      'libgtk2.0-dev',
      'libieee1284-3-dev',
      'libjack-jackd2-dev',
      'libldap2-dev',
      'libmysqlclient-dev',
      'libostyle1c2',
      'libperl-dev',
      'libpq-dev',
      'libqt4-dev',
      'libsane-dev',
      'libsgmls-perl',
      'libspiro0',
      'libuninameslist-dev',
      'libusb-dev',
      'libxml2-dev',
      'libxslt1-dev',
      'libxxf86vm-dev',
      'libkrb5-dev',
      'libzstd-dev',
      'manpages-dev',
      'manpages-posix-dev',
      'pkg-config',
      'python-dev',
      'python3-dev',
      'python-django',
      'python-tk',
      'python-virtualenv',
      'rcs',
      'sgmlspl',
      'valgrind',
      'x11proto-xf86vidmode-dev',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class development_environment::linux_common
{
  package {
    [
      'autoconf',
      'automake',
      'avr-libc',
      'avrdude',
      'bison',
      'bzr',
      'cabal-install',
      'cmake',
      'cmake-curses-gui',
      'cvs',
      'ddd',
      'dia',
      'flex',
      'fontforge',
      'git-cvs',
      'git-gui',
      'git-svn',
      'gitk',
      'libtool',
      'mercurial',
      'openjade',
      'perl',
      'prelink',
      'python',
      'python-markdown',
      'python-twisted',
      'ruby',
      'sbcl',
      'subversion',
      'yasm',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class development_environment::centos
{
  include '::development_environment::linux_common'

  package {
    [
      'Django',
      'avr-gcc',
      'avr-gcc-c++',
      'avr-gdb',
      'byacc',
      'cscope',
      'ctags',
      'cups-devel',
      'docbook-dtds',
      'docbook-style-dsssl',
      'docbook-style-xsl',
      'docbook-utils',
      'docbook2X',
      'doxygen',
      'elfutils',
      'elfutils-libelf',
      'freeglut-devel',
      'fribidi-devel',
      'gcc',
      'gcc-c++',
      'gettext',
      'ghc',
      'ghc-HTTP-devel',
      'ghc-hscolour-devel',
      'ghc-regex-posix-devel',
      'giflib-devel',
      'glade3',
      'glade3-libgladeui-devel',
      'gnutls-devel',
      'gtk2-devel',
      'guile',
      'guile-devel',
      'indent',
      'intltool',
      'jack-audio-connection-kit-devel',
      'libXxf86vm-devel',
      'libexif-devel',
      'libgcrypt-devel',
      'libglade2-devel',
      'libgpg-error-devel',
      'libgphoto2-devel',
      'libieee1284-devel',
      'libspiro-devel',
      'libtasn1-devel',
      'libuninameslist-devel',
      'libusb-devel',
      'libxml2-devel',
      'libxslt-devel',
      'patch',
      'patchutils',
      'perl-SGMLSpm',
      'perl-devel',
      'pkgconfig',
      'qt-devel',
      'rcs',
      'sane-backends-devel',
      'swig',
      'systemtap',
      'tkinter',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class development_environment
{
  include "development_environment::${::operatingsystem}"
}

class development_environment::opensuse
{
}

class sysadm_environment::ubuntu
{
  include '::sysadm_environment::debian'
}

class sysadm_environment::debian
{
  include '::sysadm_environment::linux_common'

  package {
    [
      'lsb-release',
      'lldpd',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class sysadm_environment::opensuse
{
  include '::sysadm_environment::linux_common'

  package {
    [
      'lsb-release',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}
class sysadm_environment::centos
{
  include '::sysadm_environment::linux_common'

  package {
    [
      'redhat-lsb',
      'bind-utils',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class sysadm_environment::linux_common
{
  package {
    [
      'iotop',
      'lshw',
      'lsof',
      'pciutils',
      'pdsh',
      'screen',
      'strace',
      'sysstat',
      'tcsh',
      'tmux',
      'traceroute',
      'usbutils',
      'zsh',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class sysadm_environment
{
  include "sysadm_environment::${::operatingsystem}"
}

class x11_client::ubuntu
{
  package {
    [
      'awesome',
      'blackbox',
      'fluxbox',
      'fvwm',
      'gajim',
      'gimp',
      'glurp',
      'gv',
      'inkscape',
      'kde-baseapps',
      'konsole',
      'lxde',
      'mplayer',
      'opera-beta',
      'opera-developer',
      'opera-stable',
      'pidgin',
      'plasma-desktop',
      'psi',
      'sonata',
      'twm',
      'ubuntu-desktop',
      'ubuntu-gnome-desktop',
      'vlc',
      'wmaker',
      'wmii',
      'xautolock',
      'xfce4',
      'xpdf',
      'xscreensaver',
      'xterm',
      'xtightvncviewer',
    ]:
      ensure  => installed,
      require => Class['network'],
  }

  # Baloo indexes "$HOME" automatically, which kills NFS performance.
  package {
    'baloo':
      ensure => purged,
  }
}

class x11_client::debian
{
  package {
    [
      'awesome',
      'blackbox',
      'fluxbox',
      'fvwm',
      'gajim',
      'gimp',
      'glurp',
      'gnome',
      'gv',
      'inkscape',
      'kde',
      'konsole',
      'mplayer',
      'opera',
      'pidgin',
      'psi',
      'qtnx',
      'scrotwm',
      'sonata',
      'twm',
      'vlc',
      'wmaker',
      'wmii',
      'xautolock',
      'xfce4',
      'xlockmore',
      'xpdf',
      'xscreensaver',
      'xterm',
      'xtightvncviewer',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

# X11 programs that are of "generic" use, and can be of use remotely
# as well.  Very graphically intensive applications that will almost
# certainly be used on machines with a local display do not belong
# here.
class x11_client
{
  include "x11_client::${::operatingsystem}"
}

class x11_client::opensuse
{
}

class lysator_branding
{
  package {
    'lysator-wallpapers':
      ensure => latest,
  }
}

class workstation::ubuntu
{
  include ::lysator_branding
  include '::workstation::debian'
}

class workstation::debian
{
  package { [ 'devscripts', ]:
    ensure => installed,
  }
}

class workstation::opensuse
{
}

class workstation
{
  include ::x11_client
  include "workstation::${::operatingsystem}"
  include ::user_packages
  include ::login_manager

  file { '/etc/emacs/site-start.d/10lyskom.el':
    source => "${::pupfiles}/lyskom-serverlist.el",
    mode   => '0444',
  }

}

# Packages installed on request from users. Yes, /pkg exists, but it's not the
# end-all of software installation methods especially when it comes to things like
# SDL which has thousands of dependencies.
class user_packages::ubuntu
{
  package {
    [
      'a2ps',
      'agda-bin',
      'anki',
      'apt-file',
      'arandr',
      'aria2',
      'asciidoc',
      'auctex',
      'audacity' ,
      'autocutsel',
      'bash-doc',
      'biber',
      'blender',
      'bless',
      'borgbackup',
      'calibre',
      'cantata',
      'cargo',
      'cargo-doc',
      'cdecl',
      'cgoban',
      'chrpath',
      'cinnamon',
      'cloc',
      'codeblocks',
      'conky-all',
      'cowsay',
      'cscope',
      'curl',
      'darkice',
      'dos2unix',
      'dosbox',
      'doxygen',
      'dzen2',
      'eclipse',
      'eclipse-cdt',
      'eclipse-jdt',
      'elinks',
      'emacs-goodies-el',
      'enlightenment',
      'enscript',
      'erlang-base',
      'erlang-dev',
      'expect',
      'exuberant-ctags',
      'fcitx',
      'fcitx-config-gtk',
      'fcitx-hangul',
      'fcitx-mozc',
      'feh',
      'figlet',
      'filezilla',
      'fish',
      'flatpak',
      'fonts-inconsolata',
      'fortune-mod',
      'freecad',
      'fsharp',
      'gconf2',
      'gdb',
      'geany',
      'git-buildpackage',
      'global',
      'gnat',
      'gnucash',
      'google-chrome-stable',
      'gperf',
      'gstreamer1.0-libav',
      'gstreamer1.0-plugins-bad',
      'gtk-recordmydesktop',
      'guile-2.2',
      'guile-2.2-dev',
      'guile-2.2-doc',
      'gwenview',
      'help2man',
      'hexchat',
      'htop',
      'i3',
      'icecast2',
      'inotify-tools',
      'ipython',
      'ipython3',
      'isync',
      'josm',
      'jq',
      'k3b',
      'kdelibs5-dev',
      'kdiff3',
      'keepass2',
      'khal',
      'khal-doc',
      'kmail',
      'kontact',
      'konversation',
      'krita',
      'ksh',
      'lame',
      'libasound2-dev',
      'libbluetooth-dev',
      'libboost-dev',
      'libboost-doc',
      'libbz2-dev',
      'libcairo2-dev',
      'libdbus-1-dev',
      'libeigen3-dev',
      'libgconf2-dev',
      'libglew-dev',
      'libgumbo-dev',
      'libhdf5-dev',
      'libimlib2-dev',
      'liblua5.2-dev',
      'libmodplug-dev',
      'libmono-system-data-datasetextensions4.0-cil',
      'libmono-system-management4.0-cil',
      'libmono-system-web-extensions4.0-cil',
      'libmono-system-xml-linq4.0-cil',
      'libmp3lame-dev',
      'libmpg123-dev',
      'libnspr4',
      'libnspr4-dev',
      'libnss3-dev',
      'libopenal-dev',
      'libopencv-dev',
      'libopenmpi-dev',
      'libphysfs-dev',
      'libportaudio2',
      'librecad',
      'libreoffice',
      'libreoffice-l10n-da',
      'libreoffice-l10n-de',
      'libreoffice-l10n-en-gb',
      'libreoffice-l10n-es',
      'libreoffice-l10n-fi',
      'libreoffice-l10n-fr',
      'libreoffice-l10n-nb',
      'libreoffice-l10n-sv',
      'libsdl-gfx1.2-dev',
      'libsdl-image1.2-dev',
      'libsdl-mixer1.2-dev',
      'libsdl-ttf2.0-dev',
      'libsdl2-dev',
      'libsdl2-gfx-dev',
      'libsdl2-gfx-doc',
      'libsdl2-image-dev',
      'libsdl2-mixer-dev',
      'libsdl2-net-dev',
      'libsdl2-ttf-dev',
      'libsndfile1-dev',
      'libsqlite3-dev',
      'libstdc++-5-doc',
      'libtheora-dev',
      'libx11-xcb-dev',
      'libxcb-ewmh-dev',
      'libxcb-ewmh2',
      'libxcb-icccm4',
      'libxcb-icccm4-dev',
      'libxcb-image0',
      'libxcb-image0-dev',
      'libxcb-keysyms1',
      'libxcb-keysyms1-dev',
      'libxcb-randr0-dev',
      'libxcb-render-util0',
      'libxcb-render-util0-dev',
      'libxcb-util-dev',
      'libxcb-util0-dev',
      'libxcb-xtest0-dev',
      'libxext-dev',
      'libxtst-dev',
      'libxv1:i386',
      'links2',
      'love','love-doc',
      'lua5.2',
      'luajit',
      'lynx',
      'lyx',
      'make-doc',
      'maven',
      'mc',
      'milkytracker',
      'minicom',
      'mono-devel',
      'mono-dmcs',
      'mono-xbuild',
      'monodevelop',
      'moreutils',
      'mpc',
      'mpv',
      'mumble',
      'mutt',
      'mysql-workbench',
      'ncdu',
      'neovim',
      'netbeans',
      'netris',
      'newsboat',
      'nitrogen',
      'nodejs',
      'numlockx',
      'octave',
      'octave-doc',
      'okular',
      'openttd',
      'oregano',
      'pacmixer', # installs from repomaster
      'pandoc',
      'parallel',
      'pass',
      'pass-otp',
      'pavucontrol',
      'php-cli',
      'pike7.8',
      'pms',
      'protobuf-compiler',
      'puppet-lint',
      'pwgen',
      'python-ldap',
      'python-opencv',
      'python-opengl',
      'python-pandas',
      'python-pygame',
      'python-scikits-learn',
      'python-scipy',
      'python-sklearn',
      'python-sklearn-pandas',
      'python3',
      'python3-doc',
      'python3-examples',
      'python3-i3pystatus',
      'python3-tk',
      'qemu',
      'qt5-default',
      'qtcreator',
      'qttools5-dev',
      'quassel-client',
      'r-base',
      'racket',
      'ranger',
      'rapidsvn',
      'rdesktop',
      'redshift',
      'redshift-gtk',
      'remmina',
      'rustc',
      'rxvt-unicode',
      'scdaemon',
      'scons',
      'scribus',
      'scribus-doc',
      'scrot',
      'shellcheck',
      'source-highlight',
      'sqlite',
      'sqlitebrowser',
      'ssvnc',
      'stellarium',
      'stow',
      'swi-prolog',
      'taskwarrior',
      'terminator',
      'terminology',
      'texworks',
      'tf',
      'thinlinc-client',
      'tig',
      'ubuntu-mate-core',
      'ubuntu-mate-desktop',
      'uim-tcode',
      'unclutter',
      'unrar',
      'urlview',
      'valac',
      'vdirsyncer',
      'vim-gnome',
      'vim-latexsuite',
      'virt-viewer',
      'virtualbox',
      'virtualbox-ext-pack',
      'vit',
      'vivaldi-snapshot',
      'vlock',
      'weechat',
      'wine-stable',
      'wireshark',
      'xbindkeys',
      'xcape',
      'xdotool',
      'xfce4-terminal',
      'xfonts-terminus',
      'xkcdpass',
      'xmlto',
      'xmobar',
      'xmonad',
      'xnest',
      'xpenguins',
      'xscreensaver-data-extra',
      'xscreensaver-gl-extra',
      'xserver-xephyr',
      'zanshin',
      'zathura',
    ]:
      ensure  => latest,
      require => Class['network'],
  }

  include ::steam
}

class user_packages::debian
{
  package {
    [
      'auctex',
      'dtach',
      'erlang-base',
      'erlang-dev',
      'exiv2',
      'finch',
      'fsharp',
      'gperf',
      'irssi',
      'irssi-dev',
      'irssi-plugin-otr',
      'irssi-scripts',
      'libasound2-dev',
      'libboost-dev',
      'libboost-doc',
      'libcairo2-dev',
      'libdbus-1-dev',
      'libgconf2-dev',
      'libnspr4',
      'libnspr4-dev',
      'libnss3-dev',
      'libsdl-image1.2-dev',
      'libsdl-mixer1.2-dev',
      'libsdl-ttf2.0-dev',
      'links2',
      'lynx',
      'moreutils',
      'mpc',
      'msttcorefonts',
      'mutt',
      'newsbeuter',
      'octave',
      'octave-doc',
      'oregano',
      'parallel',
      'pv',
      'rlwrap',
      'scons',
    ]:
      ensure  => installed,
      require => Class['network'],
  }

  package {
    'thinlinc-client':
      ensure   => latest,
      provider => 'dpkg',
      require  => Class['network'],
      source   => "${::pupfiles}/packages/thinlinc-client_4.8.0-5456_amd64.deb",
  }
}

class user_packages::opensuse
{
  package {
    [
      'auctex',
      'cedet-common',
      'cedet-contrib',
      'dwm-tools',
      'eclipse',
      'eclipse-cdt',
      'eieio',
      'erlang-base',
      'erlang-dev',
      'feh',
      'finch',
      'fsharp',
      'gperf',
      'gstreamer0.10-ffmpeg',
      'gstreamer0.10-plugins-bad',
      'irssi',
      'irssi-dev',
      'irssi-plugin-otr',
      'irssi-scripts',
      'java-1_8_0-openjdk',
      'libasound2-dev',
      'libboost-dev',
      'libboost-doc',
      'libcairo2-dev',
      'libdbus-1-dev',
      'libgconf2-dev',
      'libmono-winforms1.0-cil',
      'libmono-winforms2.0-cil',
      'libnspr4-0d',
      'libnspr4-dev',
      'libnss3-dev',
      'libsdl-image1.2-dev',
      'libsdl-mixer1.2-dev',
      'libsdl-ttf2.0-dev',
      'links2',
      'lynx',
      'mpc',
      'msttcorefonts',
      'mutt',
      'netbeans',
      'octave3.0',
      'octave3.0-doc',
      'octave3.0-emacsen',
      'oregano',
      'pdfjam',
      'qemu',
      'rxvt-unicode',
      'scons',
      'scrot',
      'vim-full',
      'vim-plugin-latex',
      'wine',
      'xfonts-terminus',
    ]:
      ensure  => installed,
      require => Class['network'],
  }
}

class user_packages
{
  include "user_packages::${::operatingsystem}"
}

class steam
{
  exec {
    'debconf-preseed':
      command => "/usr/bin/debconf-set-selections < ${::pupfiles}/debconf-steam.seeds",
      unless  => '/usr/bin/debconf-show steam | /bin/grep -q "^\* steam/question:"',
  }

  -> package {
    'steam':
      ensure => latest,
  }

  ~> file {
    '/usr/bin/steam':
      source => "${::pupfiles}/steam_startup_script",
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
  }

  package { 'acl':
    ensure => present,
  }

  file {
    '/opt/steam-games':
      ensure => directory,
      owner  => 'root',
      group  => 'lysator',
      mode   => 'u=rwx,g=srwx,o=',
  }

  exec {
    'set_steam_common_acl':
      command => '/usr/bin/setfacl -d -m u::rwx,g::rwx,o::r-x /opt/steam-games',
      unless  => '/usr/bin/test "$(/usr/bin/getfacl -pdc /opt/steam-games | /usr/bin/xargs)" = "user::rwx group::rwx other::r-x"',
      require => Package['acl'],
  }

  file {
    '/var/cache/steam-downloads':
      ensure => directory,
      owner  => 'root',
      group  => 'lysator',
      mode   => 'u=rwx,g=srwx,o=',
  }

  exec {
    'set_steam_cache_acl':
      command => '/usr/bin/setfacl -d -m u::rwx,g::rwx,o::r-x /var/cache/steam-downloads',
      unless  => '/usr/bin/test "$(/usr/bin/getfacl -pdc /var/cache/steam-downloads | /usr/bin/xargs)" = "user::rwx group::rwx other::r-x"',
      require => Package['acl'],
  }

  file {
    '/var/lib/update-notifier/user.d/steam-install-notify':
      ensure => absent,
  }
}
