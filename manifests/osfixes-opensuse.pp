class opensuse_keep_os_updated
{
  cron {
    'update-os':
      command  => '/opt/puppet/scripts/randomsleep; zypper --quiet --ignore-unkown ref && zypper --quiet --ignore-unkown --skip-interactive update',
      user     => 'root',
      month    => '1-12',
      monthday => '1-31',
      weekday  => '0-7',
      hour     => '3',
      minute   => '33';
  }
}

class opensuse_fixes
{

}
