class cups_printclient
{
  include ::cups

  # Delete the old configuration.
  # TODO: This can be removed after puppet has been run once on all computers.
  file {
    '/etc/cups/client.conf':
      ensure  => absent,
      require => Package['cups'];
  }

  file {
    '/etc/cups/printers.conf':
      ensure  => file,
      notify  => Service['cups'],
      source  => "${::pupfiles}/cups/printers.conf",
      owner   => root,
      group   => lp,
      mode    => '0600',
      require => Package['cups'];
  }

  file {
    '/etc/cups/lpoptions':
      ensure  => file,
      notify  => Service['cups'],
      source  => "${::pupfiles}/cups/lpoptions",
      owner   => root,
      group   => lp,
      mode    => '0444',
      require => Package['cups'];
  }

  file {
    '/etc/cups/ppd/DA-Converter.ppd':
      ensure  => file,
      notify  => Service['cups'],
      source  => "${::pupfiles}/cups/ppd/DA-Converter.ppd",
      owner   => root,
      group   => root,
      mode    => '0644',
      require => Package['cups'];
  }

  file {
    '/etc/cups/ppd/urd.ppd':
      ensure  => file,
      notify  => Service['cups'],
      source  => "${::pupfiles}/cups/ppd/urd.ppd",
      owner   => root,
      group   => root,
      mode    => '0644',
      require => Package['cups'];
  }

  file {
    '/usr/lib/cups/filter/kyofilter_C':
      ensure  => file,
      notify  => Service['cups'],
      source  => "${::pupfiles}/cups/filter/urd",
      owner   => root,
      group   => root,
      mode    => '0555',
      require => Package['cups'];
  }

  service {
    'cups':
      ensure  => $running,
      enable  => true,
      require => Package['cups'];
  }
}

class cups
{
  package {
    'cups':
      ensure  => installed,
      require => Service['interfaces'];
  }
}


class printclient
{
  case $operatingsystem {
    'Debian': {
      include ::cups_printclient
    }
    'Ubuntu': {
      include ::cups_printclient
    }
  }
}
