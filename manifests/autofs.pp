class autofs
{
  package {
    'autofs':
      ensure  => installed,
      name    => $operatingsystem ? {
        'Debian'   => 'autofs5',
        'Ubuntu'   => 'autofs',
        'centos'   => 'autofs',
        'OpenSuSE' => 'autofs',
      },
      require => [ Package['nfs'],
        Class['nis_client_generic'],
      Service['nis'] ];
  }

  package {
    'nfs':
      ensure  => installed,
      name    => $operatingsystem ? {
        'Debian'   => 'nfs-common',
        'Ubuntu'   => 'nfs-common',
        'centos'   => 'nfs-utils',
        'OpenSuSE' => 'nfs-client',
      },
      require => Class['network'];
  }

  file {
    'auto.master':
      ensure  => file,
      name    => '/etc/auto.master',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['autofs'],
      source  => [ "${::pupfiles}/autofs/auto.master-${::hostname}",
        "${::pupfiles}/autofs/auto.master-${operatingsystem}",
      "${::pupfiles}/autofs/auto.master" ],
  }

  file {
    '/lysator':
      ensure => symlink,
      target => '/mp/lysator',

  }

  case $operatingsystem {
    'debian': {
      file {
        '/etc/idmapd.conf':
          ensure => file,
          owner  => 'root',
          group  => 'root',
          mode   => '0444',
          source => "${::pupfiles}/idmapd.conf-${lsbdistcodename}";
        '/etc/default/nfs-common':
          ensure => file,
          owner  => 'root',
          group  => 'root',
          mode   => '0444',
          source => "${::pupfiles}/nfs-common";
        '/etc/default/autofs':
          ensure  => file,
          owner   => 'root',
          group   => 'root',
          mode    => '0444',
          notify  => Service['autofs'],
          content => template("${::pupfiles}/autofs/autofs.init-${operatingsystem}.erb"),
      }
    }
    'ubuntu': {
      file {
        '/etc/default/nfs-common':
          ensure => file,
          owner  => 'root',
          group  => 'root',
          mode   => '0444',
          source => "${::pupfiles}/nfs-common";
        '/etc/default/autofs':
          ensure  => file,
          owner   => 'root',
          group   => 'root',
          mode    => '0444',
          notify  => Service['autofs'],
          content => template("${::pupfiles}/autofs/autofs.init-${operatingsystem}.erb"),
      }
    }
    'OpenSuSE' : {
      file {
        '/etc/idmapd.conf':
          ensure => file,
          owner  => 'root',
          group  => 'root',
          mode   => '0444',
          source => "${::pupfiles}/idmapd.conf-${operatingsystem}";
        '/etc/sysconfig/nfs':
          ensure => file,
          owner  => 'root',
          group  => 'root',
          mode   => '0444',
          source => "${::pupfiles}/nfs-${operatingsystem}";
        '/etc/sysconfig/autofs':
          ensure  => file,
          owner   => 'root',
          group   => 'root',
          mode    => '0444',
          notify  => Service['autofs'],
          content => template("${::pupfiles}/autofs/autofs.init-${operatingsystem}.erb"),
      }
    }

  }

  service {
    'autofs':
      ensure    => $running,
      enable    => true,
      require   => Package['autofs'],
      hasstatus => $operatingsystem ? {
        'Ubuntu' => true,
        default  => false,
      },
      pattern   => $operatingsystem ? {
        'debian' => 'automount',
        default  => 'autofs',
      },
      subscribe => File['auto.master'];
  }
}
