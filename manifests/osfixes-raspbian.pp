class raspbian_package_repositories
{
  exec {
    'apt-update':
      command     => '/usr/bin/apt-get update',
      refreshonly => true;
  }

  exec {
    'apt-key-update':
      command     => '/usr/bin/apt-key update',
      refreshonly => true,
      notify      => Exec['apt-update'];
  }

  file {
    '/etc/apt/sources.list':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      content => template("${::pupfiles}/sources.list-Raspbian.erb"),
      notify  => Exec['apt-update'],
      tag     => pkgrepo;
  }

  file {
    'local-apt-keys':
      path    => '/etc/apt/trusted.gpg.d/',
      source  => "${::pupfiles}/apt-keys-raspbian/",
      recurse => true,
      purge   => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0644';
  }

  recursive_directory {
    'local-apt-sources':
      source_dir => "${::pupfiles}/apt-sources-raspbian/",
      dest_dir   => '/etc/apt/sources.list.d/',
      owner      => 'root',
      group      => 'root',
      dir_mode   => '0755',
      file_mode  => '0644',
      notify     => Exec['apt-key-update'],
      require    => File['local-apt-keys'];
  }

  file {
    '/etc/apt/sources.list.d/':
      ensure  => 'directory',
      recurse => true,
      purge   => true,
  }
}

class raspbian_no_networkmanager
{
  package { ['network-manager', 'gnome-network-manager']:
    ensure => absent,
    notify => Service['interfaces'];
  }
}

class raspbian_no_dhcpcd
{
  package { ['dhcpcd', 'dhcpcd5']:
    ensure => purged;
  }
}

class raspbian_fixes
{
  include ::raspbian_no_networkmanager
  include ::raspbian_package_repositories
  include ::raspbian_no_dhcpcd
}
