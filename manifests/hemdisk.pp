class hemdisk
{
  cron {
    'scrub-frigg':
      command  => '/sbin/zpool scrub tank1',
      user     => 'root',
      month    => '1-12',
      monthday => '7',
      weekday  => '*',
      hour     => '3',
      minute   => '33';
  }

  package {
    [
      'fail2ban',
      'ifenslave',
      'nfs-kernel-server',
    ]:
      ensure => installed,
  }

  exec {
    'modprobe-zfs':
      command     => '/sbin/modprobe zfs',
      refreshonly => true;
  }

  package {
    'debian-zfs':
      ensure => installed,
      name   => 'zfs-dkms',
      notify => Exec['modprobe-zfs'],
  }

  file {
    'zed.rc':
      ensure => file,
      name   => '/etc/zfs/zed.d/zed.rc',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/zfsonlinux/zed.rc",
  }

  service {
    'zfs-event-deamon':
      ensure    => running,
      name      => 'zfs-zed',
      enable    => true,
      subscribe => File['zed.rc'],
  }

  file {
    'aliases-bond':
      ensure => file,
      name   => '/etc/modprobe.d/aliases-bond.conf',
      source => "${::pupfiles}/network/interfaces/aliases-bond.conf-frigg_lacp",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file {
    'modules':
      ensure => file,
      name   => '/etc/modules',
      source => "${::pupfiles}/modules/frigg",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  exec {
    'export-all-nfs':
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => 'exportfs -a',
  }
}
