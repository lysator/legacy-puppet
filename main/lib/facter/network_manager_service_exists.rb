Facter.add(:network_manager_service_exists) do
  setcode do
    File.exists?('/lib/systemd/system/NetworkManager.service') || File.exists?('/lib/systemd/system/network-manager.service')
  end
end
