# These custom facts are a workaround for a limitation in facter 1.3.8,
# which does not report MAC addresses for interfaces that do not have
# an IP address.  And for the ifconfig(8) command only reporting the
# six most significant octets of the MAC address correctly, filling the
# others with zeros.
# See also <http://reductivelabs.com/redmine/issues/show/1415>.

Dir.glob('/sys/class/net/*/address').each do |fpath|
    interface = File.basename(File.dirname(fpath))
  Array(`/sbin/ip addr show dev '#{interface}'`).each do |line|
    if line =~ /link\/[^ ]* ((\w{1,2}:){5,}\w{1,2})/
            hwaddr = $1.upcase
            Facter.add("nsc_macaddress_" + interface) do
        setcode { hwaddr }
            end
        end
    end

#   This is a prettier way of getting the MAC addresses, but for some
#   reason Puppet hangs when reading the address file. :-(  Standalone
#   facter doesn't, though.
#    hwaddr = File.read(fpath).strip()
#    Facter.add("nsc_macaddress_" + interface) do
#       setcode do
#           hwaddr
#       end
#    end

end
