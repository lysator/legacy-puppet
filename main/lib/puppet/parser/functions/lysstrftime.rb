module Puppet::Parser::Functions
    newfunction(:lysstrftime, :type => :rvalue) do |args|
	if args.length < 1
	    self.fail("lysstrftime(): Missing format string parameter")
	end
	fmt = args.shift()
	if args.length == 0
	    return Time.now().strftime(fmt)
	end
	self.fail("lysstrftime(): Too many arguments")
    end
end
