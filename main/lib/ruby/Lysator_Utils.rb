require 'ipaddr'

module Lysator_Utils

  def resolve_ipnets(netspecs, *flags)
    
    def resolve_netspec(addr, ipfamily, on_error)
      if m = /^(v[0-9]+):(.*)/.match(addr)
        origaddr = addr  # Save for diagnostics
        familyspec,addr = m[1],m[2]
        if familyspec == "v4"
          ipfamily = Socket::AF_INET
        elsif familyspec == "v6"
          ipfamily = Socket::AF_INET6
        else
          raise(Puppet::ParseError,
                "resolve_ipnets(): Bad IP family #{familyspec} in #{origaddr}")
        end
      end
      if m = /^(.*)(\/[^\/]*)$/.match(addr)
        addr,mask = m[1],m[2]
      else
        mask = ""
      end
      begin
        ip = Socket.getaddrinfo(addr, nil, family=ipfamily)[0][3]
      rescue SocketError
        if on_error == :fail
          raise
        else
          ip = addr
        end
      end
      return ip + mask
    end
    module_function :resolve_netspec

    def resolve_helper(arg, family, on_error)
      if arg.is_a?(Array)
        return arg.collect do |a|
          resolve_helper(a, family, on_error)
        end
      elsif arg.is_a?(String)
        return resolve_netspec(arg, family, on_error)
      else
        raise(Puppet::ParseError,
              "resolve_ipnets(): Bad parameter type #{arg.class}: #{arg}")
      end
    end
    module_function :resolve_helper

    ipfamily = Socket::AF_UNSPEC
    on_error = :fail
    flags.each do |f|
      case f
      when 'ipv4'
        ipfamily = Socket::AF_INET
      when 'ipv6'
        ipfamily = Socket::AF_INET6
      when 'ignoreerrors'
        on_error = :ignore
      when 'failerrors'
        on_error = :fail
      else
        raise(Puppet::ParseError, "resolve_ipnets(): Bad flag #{f}")
      end
    end
    return resolve_helper(netspecs, ipfamily, on_error)
  end
  module_function :resolve_ipnets

end
