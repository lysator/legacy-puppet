RM	= rm -vf

export	FACTER_PUPHOME	= $(shell pwd)
export	RUBYLIB	= ${FACTER_PUPHOME}/main/lib/ruby
# We can't rely on $PATH when Puppet is run from cron.  Specifically,
# /sbin and /usr/sbin are missing.
export	PATH	= /bin:/usr/bin:/sbin:/usr/sbin:/usr/sfw/bin:/usr/local/bin:/opt/lysator/bin

LOGDIR	  := /var/log/puppet
FACTER_LOGDIR := ${LOGDIR}
export FACTER_LOGDIR			# Needed by the log-puppetrun class
NOW	  := $(shell /bin/date +'%Y %m %d--%H.%M.%S')
MONTHDIR  := $(word 1,${NOW})-$(word 2,${NOW})
TIMESTAMP := $(word 1,${NOW})-$(word 2,${NOW})-$(word 3,${NOW})
SHORTHOST := $(shell /usr/bin/env uname -n | /usr/bin/env sed 's/[.].*//')
LOGSUBDIR := ${LOGDIR}/${SHORTHOST}/${MONTHDIR}
LOGFILE	  := ${LOGSUBDIR}/${TIMESTAMP}

# Extra flags to give to Puppet
PUPFLAGS=
ifeq ($(shell uname),SunOS)
	PUPFLAGS:=${PUPFLAGS} --confdir=/etc --vardir=/opt/lysator/var
endif

# Extra environment variables to give to Puppet
PUPENV	=

# Command to strip "uninteresting" messages from cron runs
STRIPMSG= egrep -v "^Notice:|^notice:|^info:|^debug:|^warning: Dynamic lookup of|^Warning: The use of 'import' is deprecated at |^   \(at /usr/lib/ruby/vendor_ruby/puppet/parser/parser_support.rb:110:in |^Warning: Unknown variable: |^Warning: Facter: Could not process routing table entry: Expected a destination followed by key/value pairs, got|^Warning: Unacceptable location. The name"
#STRIPMSG= cat
STRIPJUNK= egrep -v '^ifconfig: status: SIOCGLIFFLAGS:'
STRIPGIT= egrep -v '^Already up-to-date.'
PUPPET	= ${PUPENV} puppet apply --color=false ${PUPFLAGS} --libdir="${RUBYLIB}" --modulepath="${FACTER_PUPHOME}:/usr/share/puppet/modules"


default:
	cd Files && ${MAKE}
	@#cd sendmail && ${MAKE}

# Note that this target does not install, or change, the cron job that
# runs Puppet.
run:
	mkdir -p "${LOGSUBDIR}"
	@echo "Source:  ${FACTER_PUPHOME}"	>"${LOGFILE}"
	@echo "Command: ${PUPPET}"	>>"${LOGFILE}"
	@echo ""			>>"${LOGFILE}"
	${PUPPET} --tags puppet_config manifests 2>&1 | ${STRIPJUNK} | tee -a "${LOGFILE}"
	${PUPPET} --tags pkgrepo manifests 2>&1 | ${STRIPJUNK} | tee -a "${LOGFILE}"
	${PUPPET} manifests 2>&1 | ${STRIPJUNK} | tee -a "${LOGFILE}"
	@if [ `wc -l <"${LOGFILE}"` -le 3 ]; then \
	    /bin/rm -f "${LOGFILE}" ; true ; \
	else \
	    fin=`date +'%Y-%m-%d  %H:%M:%S'`; \
	    echo ""			>>"${LOGFILE}" ; \
	    echo "Finished:  $$fin"	>>"${LOGFILE}" ; \
	fi

# This target, however, does install the cron job.
install:
	@${MAKE} run PUPENV="FACTER_PUP_DOINSTALL=true ${PUPENV}"
	@${MAKE} run PUPENV="FACTER_PUP_DOINSTALL=true ${PUPENV}"
	@${MAKE} run PUPENV="FACTER_PUP_DOINSTALL=true ${PUPENV}"

# Run from cron
# This target also installs the cron job, but it only logs problems
# to the "terminal".
# Note: egrep will return 1 if nothing matches, which is perfectly normal
# in this case, thus the "; true".  Unfortunately we lose the exit status
# of the make command, but it shouldn't matter much here.
cron:
	@git pull -q > /dev/null
	@${MAKE} run PUPENV="FACTER_PUP_DOINSTALL=true ${PUPENV}" | \
	    (${STRIPMSG}; true)

# The 'kickstart' target is used when we run Puppet from the %post kickstart
# script, since we don't want to actually start the services then.
kickstart:
	@${MAKE} run PUPENV="FACTER_PUP_DOINSTALL=true PUP_RUNDAEMONS=false ${PUPENV}"

# Perform a "dryrun", i.e run without actually making any changes.
dry:
	@${MAKE} run PUPFLAGS="--noop ${PUPFLAGS}" LOGSUBDIR="/tmp" LOGFILE="/tmp/pupdry"

test:
	${PUPPET} test.pp


clean:
	${RM} .puppethome
	cd Files && ${MAKE} clean
	@#cd sendmail && ${MAKE} clean
	${RM}  *..tmp *.BAK *~ \#.*



.PHONY: default run install cron kickstart test clean
