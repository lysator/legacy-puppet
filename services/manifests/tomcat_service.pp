class services::tomcat_service
{

  file {
    '/etc/init.d/tomcat':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0755',
      source => "${::pupfiles}/tomcat-service/tomcat-initd-service",
      notify => Service['tomcat'];
  }

  exec { 'enable-tomcat':
    command => '/usr/sbin/update-rc.d tomcat defaults',
  }

  service {
    'tomcat':
      ensure  => $running,
      enable  => true,
      require => Exec['enable-tomcat'];
  }

}
