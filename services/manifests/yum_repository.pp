# Sets up machine to be deb AND RPM repo master.
class services::yum_repository
{
  $arches = [
    'x86_64',
  ]

  package { 'createrepo-c': }

  # RHEL, CentOS, Rocky, etc.
  services::yum_repository::init_repos { 'enterprise linux':
    os_slug  => 'el',
    versions => [
      '7',
      '8',
      '9',
    ],
    arches   => $arches,
  }

  # Fedora
  services::yum_repository::init_repos { 'fedora':
    os_slug  => 'fedora',
    versions => [
      '35',
      '36',
      '37',
      '38',
      '39',
    ],
    arches   => $arches,
  }

  # contains chronic, required by lysator_update_yum_repos_cron
  package { 'moreutils': }

  file { '/usr/local/bin/lysator_update_yum_repos_cron':
    ensure => file,
    source => "${::pupfiles}/repository/lysator_update_yum_repos_cron",
    owner  => root,
    group  => root,
    mode   => '0555',
  }

  cron { 'lysator_update_yum_repos_cron':
    command => '/usr/local/bin/lysator_update_yum_repos_cron',
    user    => root,
    minute  => 24,
  }

  file { '/usr/local/bin/add_latest_thinlinc_client_to_repos':
    ensure => file,
    source => "${::pupfiles}/repository/add_latest_thinlinc_client_to_repos",
    owner  => root,
    group  => root,
    mode   => '0555',
  }

  cron { 'add_latest_thinlinc_client_to_repos':
    command => '/usr/local/bin/add_latest_thinlinc_client_to_repos',
    user    => root,
    weekday => 6,
    hour    => 6,
    minute  => 15,
  }
}

define services::yum_repository::init_repos (
  String $os_slug,
  Array[String] $versions,
  Array[String] $arches,
) {
  file { "/srv/repos/${os_slug}/":
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0775',
  }

  $versions.each |$version| {
    file { "/srv/repos/${os_slug}/${version}":
      ensure  => directory,
      owner   => root,
      group   => root,
      mode    => '0775',
      require => File["/srv/repos/${os_slug}/"],
    }
    $arches.each |$arch| {
      $repo_path = "/srv/repos/${os_slug}/${version}/${arch}"
      file { $repo_path:
        ensure  => directory,
        owner   => root,
        group   => root,
        mode    => '0775',
        require => File["/srv/repos/${os_slug}/${version}"],
      }
      ~> exec { "/usr/bin/createrepo_c ${repo_path}":
        refreshonly => true,
      }
    }
  }
}
