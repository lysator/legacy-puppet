class services::login
{

  package {
    'freeradius':
      ensure => installed;
    'freeradius-krb5':
      ensure => installed;
    'freeradius-utils':
      ensure => installed;
  }

  service {
    'freeradius':
      ensure => $running,
      enable => true;
  }

  file {
    '/etc/freeradius/users':
      ensure => file,
      owner  => root,
      group  => freerad,
      mode   => '0640',
      source => "${::pupfiles}/login/radius-users.erb",
      notify => Service['freeradius'];
    '/etc/freeradius/sites-available/default':
      ensure => file,
      owner  => root,
      group  => freerad,
      mode   => '0640',
      source => "${::pupfiles}/login/radius-sites-default.erb",
      notify => Service['freeradius'];
    '/etc/freeradius/clients.conf':
      ensure  => file,
      owner   => root,
      group   => freerad,
      mode    => '0640',
      content => template("${::pupfiles}/login/radius-clients-conf.erb"),
      notify  => Service['freeradius'];
    '/etc/init.d/freeradius':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0755',
      source => "${::pupfiles}/login/radius-initd-service",
      notify => Service['freeradius'];

  }
# TBD: /etc/freeradius/clients.conf should ideally be here, but I don't want to put the secret in it under version control.
# Anyway, it should look a lot like this:
## 
## client localhost {
##         ipaddr = 127.0.0.1
##         secret          = <some random assortment of digits and letters>
##         require_message_authenticator = no
## }
## 
## 


}
