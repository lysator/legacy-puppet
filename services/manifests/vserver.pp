class services::vserver
{
  package
  {
    ['linux-image-2.6.26-2-vserver-686', 'util-vserver', 'xinetd', 'vserver-debiantools']:
      ensure => installed;
  }

  file
  {
    '/etc/xinetd.conf':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0644',
      source => "${::pupfiles}/vserver/xinetd.conf-backus",
      notify => Service['xinetd'];
  }

  service
  {
    'xinetd':
      ensure  => $running,
      require => File['/etc/xinetd.conf'],
      enable  => true;
  }

}
