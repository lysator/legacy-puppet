class services::gitlab
{
  file {
    '/etc/gitlab/gitlab.rb':
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0600',
      content => template("${::pupfiles}/gitlab/gitlab.rb.erb"),
      notify  => Exec['gitlab-reconfigure'];
  }

  exec {
    'gitlab-reconfigure':
      command     => '/opt/gitlab/bin/gitlab-ctl reconfigure',
      refreshonly => true;
  }

  cron {
    'daily-backups':
      command => '/opt/gitlab/bin/gitlab-rake gitlab:backup:create > /dev/null',
      user    => 'root',
      minute  => 0,
      hour    => 18;
  }

}
