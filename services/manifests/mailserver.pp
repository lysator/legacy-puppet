class services::mailserver {
  include ::services::mailserver::postfix
  include ::services::mailserver::mailman
  include ::services::mailserver::dovecot
  include ::services::mailserver::webmail
  include ::services::mailserver::nfs
  include ::services::mailserver::statistics

  file {
    '/opt/scripts':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0775';
    '/opt/scripts/allow-mail-login':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0544',
      source  => "${::pupfiles}/mailserver/allow-mail-login",
      require => File['/opt/scripts'];
    '/opt/scripts/deactivate_maildirs.hermod':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0544',
      source  => "${::pupfiles}/mailserver/deactivate_maildirs.hermod",
      require => File['/opt/scripts'];
    '/opt/scripts/deactivate_mail_logins.hermod':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0544',
      source  => "${::pupfiles}/mailserver/deactivate_mail_logins.hermod",
      require => File['/opt/scripts'];
    '/opt/scripts/disallow-mail-login':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0544',
      source  => "${::pupfiles}/mailserver/disallow-mail-login",
      require => File['/opt/scripts'];
    '/opt/scripts/gen_maildir.hermod':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0544',
      source  => "${::pupfiles}/mailserver/gen_maildir.hermod",
      require => File['/opt/scripts'];
    '/etc/apache2/ports.conf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      content => '',
      require => Package['apache2'],
      notify  => Service['apache2'];
    '/etc/mysql/my.cnf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      source  => "${::pupfiles}/mailserver/mariadb/my.cnf",
      require => Package['mariadb-server'],
      notify  => Service['mysql'];
    '/etc/mysql/mariadb.conf.d/50-server.cnf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      source  => "${::pupfiles}/mailserver/mariadb/50-server.cnf",
      require => Package['mariadb-server'],
      notify  => Service['mysql'];
    '/etc/bind/named.conf.options':
      ensure  => file,
      owner   => 'root',
      group   => 'bind',
      mode    => '0444',
      source  => "${::pupfiles}/mailserver/dns/named.conf.options",
      require => Package['bind9'],
      notify  => Service['bind9'];
    '/etc/cron.hourly/warn-too-many-smtp-login-hosts':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0555',
      source => "${::pupfiles}/mailserver/cron/warn-too-many-smtp-login-hosts";
    '/etc/cron.weekly/clear-warn-too-many-smtp-login-hosts':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0555',
      source => "${::pupfiles}/mailserver/cron/clear-warn-too-many-smtp-login-hosts";
    '/etc/cron.hourly/warn-invalid-from-lysator':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0555',
      source => "${::pupfiles}/mailserver/cron/warn-invalid-from-lysator";
    '/etc/cron.weekly/clear-warn-invalid-from-lysator':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0555',
      source => "${::pupfiles}/mailserver/cron/clear-warn-invalid-from-lysator";
    '/var/mail':
      ensure  => symlink,
      target  => '/home',
      replace => true,
      force   => true;
  }

  package {
    [ 'procmail', 'apache2', 'mariadb-server', 'bind9', 'python2', 'iptables-persistent', 'python3-certbot-apache' ]:
      ensure => installed;
  }

  file {
    '/etc/iptables/':
      ensure  => directory,
      source  => "${::pupfiles}/mailserver/iptables/",
      recurse => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  service {
    'apache2':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['apache2'];
    'mysql':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['mariadb-server'];
    'bind9':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['bind9'];
  }


}

class services::mailserver::postfix {
  include ::services::mailserver::clamav
  include ::services::mailserver::gld
  include ::services::mailserver::spamassassin
  include ::services::mailserver::komimportmail
  include ::services::mailserver::postsrsd

  package {
    'postfix':
      ensure => installed;
    'postfix-mysql':
      ensure => installed;

  }
  service {
    'postfix':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => [ Package['postfix'],
      Package['postfix-mysql'] ];
    'check_runeberg':
      ensure    => $running,
      enable    => true,
      hasstatus => false,
      require   => File['/etc/init.d/check_runeberg'];
  }

  file {
    '/etc/postfix/main.cf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/postfix/main.cf",
      notify => Service['postfix'];
    '/etc/postfix/master.cf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/postfix/master.cf",
      notify => Service['postfix'];
    '/etc/postfix/access_maps':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0754';
    '/etc/postfix/access_maps/mysql_recipient.cf':
      content => template("${::pupfiles}/mailserver/postfix/access_maps/mysql_recipient.cf.erb"),
      require => File['/etc/postfix/access_maps'],
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      notify  => Service['postfix'];
    '/etc/postfix/relay':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0754';
    '/etc/postfix/aliasfiles':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0754';
    '/etc/postfix/header_checks.regexp':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/postfix/header_checks.regexp",
      notify => Service['postfix'];

    '/opt/scripts/check_runeberg.py':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0544',
      source  => "${::pupfiles}/mailserver/postfix/scripts/check_runeberg.py",
      require => File['/opt/scripts'];
    '/etc/init.d/check_runeberg':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0555',
      source  => "${::pupfiles}/mailserver/postfix/scripts/check_runeberg",
      require => File['/opt/scripts/check_runeberg.py'];
  }

  services::mailserver::relay_script {
    'litheblas':
      path    => '/opt/scripts/fetch_litheblaslist.py',
      base    => "${::pupfiles}/mailserver/postfix/scripts",
      reqfile => '/opt/scripts';
  }

  postmap_file {
    '/etc/postfix/canonical.recipient':
      source  => "${::pupfiles}/mailserver/postfix/canonical.recipient",
      service => 'postfix';

    '/etc/postfix/transport':
      source  => "${::pupfiles}/mailserver/postfix/transport",
      service => 'postfix';

    '/etc/postfix/virtual.regexp':
      source  => "${::pupfiles}/mailserver/postfix/virtual.regexp",
      service => 'postfix';

    '/etc/postfix/access_maps/client_access':
      source  => "${::pupfiles}/mailserver/postfix/access_maps/client_access",
      require => File['/etc/postfix/access_maps'],
      service => 'postfix';
    '/etc/postfix/access_maps/greylisting-whitelisted-clients':
      source  => "${::pupfiles}/mailserver/postfix/access_maps/greylisting-whitelisted-clients",
      require => File['/etc/postfix/access_maps'],
      service => 'postfix';
    '/etc/postfix/access_maps/heloblock':
      source  => "${::pupfiles}/mailserver/postfix/access_maps/heloblock",
      require => File['/etc/postfix/access_maps'],
      service => 'postfix';
    '/etc/postfix/access_maps/kom':
      source  => "${::pupfiles}/mailserver/postfix/access_maps/kom",
      require => File['/etc/postfix/access_maps'],
      service => 'postfix';
    '/etc/postfix/access_maps/recipient_table':
      source  => "${::pupfiles}/mailserver/postfix/access_maps/recipient_table",
      require => File['/etc/postfix/access_maps'],
      service => 'postfix';
    '/etc/postfix/access_maps/sender_domain':
      source  => "${::pupfiles}/mailserver/postfix/access_maps/sender_domain",
      require => File['/etc/postfix/access_maps'],
      service => 'postfix';
    '/etc/postfix/access_maps/sunet_filter':
      source  => "${::pupfiles}/mailserver/postfix/access_maps/sunet_filter",
      require => File['/etc/postfix/access_maps'],
      service => 'postfix';

    '/etc/postfix/relay/relay_recipients':
      source  => "${::pupfiles}/mailserver/postfix/relay/relay_recipients",
      require => File['/etc/postfix/relay'],
      service => 'postfix';
  }

  postalias_file {
    '/etc/postfix/aliasfiles/aronsson-aliases':
      source  => "${::pupfiles}/mailserver/postfix/aliasfiles/aronsson-aliases",
      require => File['/etc/postfix/aliasfiles'],
      service => 'postfix';
    '/etc/postfix/aliasfiles/lists-moved':
      source  => "${::pupfiles}/mailserver/postfix/aliasfiles/lists-moved",
      require => File['/etc/postfix/aliasfiles'],
      service => 'postfix';
    '/etc/postfix/aliasfiles/lists-the-wrong-way':
      source  => "${::pupfiles}/mailserver/postfix/aliasfiles/lists-the-wrong-way",
      require => File['/etc/postfix/aliasfiles'],
      service => 'postfix';

  }
}

class services::mailserver::komimportmail {
  file {
    '/var/lib/komimportmail':
      ensure  => directory,
      owner   => 'komimportmail',
      group   => 'root',
      mode    => '0755',
      require => User['komimportmail'];
    '/var/log/komimportmail':
      ensure  => directory,
      owner   => 'komimportmail',
      group   => 'root',
      mode    => '0755',
      require => User['komimportmail'];
  }

  user {
    'komimportmail':
      ensure     => present,
      gid        => 'nogroup',
      home       => '/var/lib/komimportmail',
      managehome => false,
      shell      => '/bin/false',
      uid        => 500;
  }
}

class services::mailserver::mailman {
  package {
    'mailman3-full':
      ensure => installed;
    'python3-mysqldb':
      ensure => installed;
    'python3-pymysql':
      ensure => installed;
    'python3-xapian-haystack':
      ensure => installed;
  }

  file {
    '/etc/mailman3/mailman.cfg':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      content => template("${::pupfiles}/mailserver/mailman/mailman.cfg.erb"),
      notify => Service['mailman3'];
    '/etc/mailman3/mailman-hyperkitty.cfg':
      ensure => file,
      owner  => 'root',
      group  => 'list',
      mode   => '0440',
      content => template("${::pupfiles}/mailserver/mailman/mailman-hyperkitty.cfg.erb"),
      notify => Service['mailman3'];
    '/etc/mailman3/mailman-web.py':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      content => template("${::pupfiles}/mailserver/mailman/mailman-web.py.erb"),
      notify => Service['mailman3'];
    '/opt/scripts/create_mailman_lists.py':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0544',
      source  => "${::pupfiles}/mailserver/mailman/create_mailman_lists.py",
      require => File['/opt/scripts'];
    '/opt/scripts/update-lystring-lysmembers':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0544',
      source  => "${::pupfiles}/mailserver/mailman/update-lystring-lysmembers",
      require => File['/opt/scripts'];
  }

  service {
    'mailman3':
      ensure    => $running,
      enable    => true,
      hasstatus => false,
      require   => Package['mailman3-full'];
  }

  cron {
    'update-lystring-lysmembers':
      command => '/opt/scripts/update-lystring-lysmembers',
      user    => 'root',
      hour    => 5,
      minute  => 13,
      weekday => 7,
      require => File['/opt/scripts/update-lystring-lysmembers'];
    'create_mailman_lists':
      command     => '/usr/bin/env python3 /opt/scripts/create_mailman_lists.py',
      user        => 'root',
      month       => '1-12',
      monthday    => '1-31',
      weekday     => '0-7',
      hour        => '0-23',
      minute      => '0-59',
      require     => [File['/opt/scripts/create_mailman_lists.py'], Package['python3-mysqldb']],
  }

  vhost {
    'lists.lysator.liu.se':
      linkname => 'lists.lysator.liu.se.conf',
      source => "${::pupfiles}/mailserver/mailman/apache2.conf";
  }
}

class services::mailserver::clamav {
  package {
    [ 'clamav-daemon', 'clamav-freshclam', 'clamsmtp' ]:
      ensure => installed;
  }

  service {
    'clamav-daemon':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['clamav-daemon'];
    'clamav-freshclam':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => [ Package['clamav-freshclam'],
      Service['clamav-daemon'] ];
    'clamsmtp':
      ensure    => $running,
      enable    => true,
      require   => [ Package['clamsmtp'],
      Service['clamav-daemon'] ],
      subscribe => Service['clamav-daemon'];
  }

  file {
    '/etc/clamav/clamd.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/clamav/clamd.conf",
      notify => Service['clamav-daemon'];
    '/etc/clamav/freshclam.conf':
      ensure => file,
      owner  => 'clamav',
      group  => 'adm',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/clamav/freshclam.conf",
      notify => Service['clamav-freshclam'];
    '/etc/clamsmtpd.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/clamav/clamsmtpd.conf",
      notify => Service['clamsmtp'];
  }
}

class services::mailserver::postsrsd {
  package {
    'postsrsd':
      ensure => installed;
  }

  service {
    'postsrsd':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['postsrsd'];
  }

  file {
    '/etc/default/postsrsd':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/postsrsd/service-config",
      notify => Service['postsrsd'];
  }
}

class services::mailserver::gld {
  package {
    'postfix-gld':
      ensure => installed;
  }

  service {
    'gld':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['postfix-gld'];
  }

  file {
    '/etc/default/gld':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/gld/service-config",
      notify => Service['gld'];
    '/etc/gld.conf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0440',
      content => template("${::pupfiles}/mailserver/gld/gld.conf.erb"),
      notify  => Service['gld'];
  }

  cron {
    'gld-clean':
      command => "(/usr/sbin/gld -c 90 && /usr/sbin/gld -k 14) | egrep -v 'Cleaned [[:digit:]]+ entries older than [[:digit:]]+ days'",
      user    => 'root',
      hour    => 4,
      minute  => 39,
      require => Package['postfix-gld'];
  }
}

# Expose e-mail statistics to https://graf.lysator.liu.se/
class services::mailserver::statistics {

  package {
    'prometheus-postfix-exporter':
      ensure => installed;
  }

  service {
    'prometheus-postfix-exporter':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['prometheus-postfix-exporter'];
  }

  file {
    '/etc/default/prometheus-postfix-exporter':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/mailserver/prometheus-postfix-exporter/service-config",
      notify => Service['prometheus-postfix-exporter'];
  }
}

class services::mailserver::spamassassin {
  package {
    ['spamassassin', 'spamd']:
      ensure => installed;
  }

  service {
    'spamassassin':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => [ Package['spamassassin'], Package['spamd'] ],
      name      => 'spamd',
  }

  file {
    '/etc/default/spamassassin':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/spamassassin/service-config",
      notify => Service['spamassassin'];
    '/etc/spamassassin/init.pre':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/spamassassin/init.pre",
      notify => Service['spamassassin'];
    '/etc/spamassassin/v310.pre':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/spamassassin/v310.pre",
      notify => Service['spamassassin'];
    '/etc/spamassassin/v312.pre':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/spamassassin/v312.pre",
      notify => Service['spamassassin'];
    '/etc/spamassassin/v320.pre':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/spamassassin/v320.pre",
      notify => Service['spamassassin'];
    '/etc/spamassassin/v330.pre':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/spamassassin/v330.pre",
      notify => Service['spamassassin'];
    '/etc/spamassassin/local.cf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/spamassassin/local.cf",
      notify => Service['spamassassin'];
    '/var/lib/spamassassin':
      ensure => directory,
      owner  => 'debian-spamd',
      group  => 'debian-spamd',
      mode   => '0755',
  }
}

class services::mailserver::dovecot {
  package {
    [ 'dovecot-pop3d', 'dovecot-imapd', 'libpam-mysql' ]:
      ensure => installed;
  }

  service {
    'dovecot':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => [ Package['dovecot-pop3d'],
      Package['dovecot-imapd'] ];
  }

  ensure_line {
    'dovecot_limits':
      file   => '/etc/security/limits.conf',
      line   => 'dovecot         -    nofile          8192',
      notify => Service['dovecot'];
    'dovecot_limits_root':
      file   => '/etc/security/limits.conf',
      line   => 'root            -    nofile          8192',
      notify => Service['dovecot'];
  }


  file {
    '/etc/dovecot/dovecot.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/dovecot.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/10-auth.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/10-auth.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/10-director.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/10-director.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/10-logging.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/10-logging.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/10-mail.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/10-mail.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/10-master.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/10-master.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/10-ssl.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/10-ssl.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/10-tcpwrapper.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/10-tcpwrapper.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/15-lda.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/15-lda.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/15-mailboxes.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/15-mailboxes.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/20-imap.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/20-imap.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/20-pop3.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/20-pop3.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/90-acl.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/90-acl.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/90-plugin.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/90-plugin.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/90-quota.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/90-quota.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/90-sieve.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/90-sieve.conf",
      notify => Service['dovecot'];
    '/etc/dovecot/conf.d/auth-system.conf.ext':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/conf.d/auth-system.conf.ext",
      notify => Service['dovecot'];
    '/etc/pam.d/lysatormail':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      content => template("${::pupfiles}/mailserver/dovecot/pam_mysql.erb"),
      require => Package['libpam-mysql'],
      notify  => Service['dovecot'];
    '/etc/pam.d/dovecot':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/dovecot/pam",
      notify => Service['dovecot'];
  }
}

class services::mailserver::webmail {
#  include services::mailserver::webmail::horde
  include services::mailserver::webmail::roundcube
}

class services::mailserver::webmail::horde {
  package {
    [ 'php-horde-webmail' ]:
      ensure => installed;
  }

  file {
    '/etc/horde/nag/conf.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/nag/conf.php";
    '/etc/horde/mnemo/conf.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/mnemo/conf.php";
    '/etc/horde/turba/conf.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/turba/conf.php";
    '/etc/horde/trean/conf.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/trean/conf.php";
    '/etc/horde/ingo/conf.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/ingo/conf.php";
    '/etc/horde/imp/conf.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/imp/conf.php";
    '/etc/horde/horde/conf.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/horde/conf.php";
    '/etc/horde/gollem/conf.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/gollem/conf.php";
    '/etc/horde/kronolith/conf.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/kronolith/conf.php";
    '/etc/horde/imp/backends.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/backends.php";
    '/etc/horde/imp/backends.local.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "${::pupfiles}/mailserver/webmail/horde/backends.local.php";
  }

  vhost {
    'webmail.lysator.liu.se':
      linkname => 'webmail.lysator.liu.se.conf',
      source => "${::pupfiles}/mailserver/webmail/horde/apache2.conf";
  }
}

class services::mailserver::webmail::roundcube {
  package {
    [ 'roundcube' ]:
      ensure => installed;
  }

  file {
    '/etc/roundcube/apache.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      require   => Package['roundcube'],
      source => "${::pupfiles}/mailserver/webmail/roundcube/apache.conf";
    '/etc/roundcube/config.inc.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      require => Package['roundcube'],
      content => template("${::pupfiles}/mailserver/webmail/roundcube/config.inc.php.erb");
    '/etc/roundcube/debian-db.php':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      require => Package['roundcube'],
      content => template("${::pupfiles}/mailserver/webmail/roundcube/debian-db.php.erb");
  }
}

class services::mailserver::nfs {
  package {
    'nfs-kernel-server':
      ensure => installed;
  }

  service {
    'nfs-kernel-server':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['nfs-kernel-server'];
  }

  file {
    '/etc/exports':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      source  => "${::pupfiles}/mailserver/nfs/exports",
      require => Package['nfs-kernel-server'],
      notify  => Service['nfs-kernel-server'];
  }
}

define services::mailserver::relay_script($path, $base, $reqfile='') {
  file {
    "relay_file_${name}":
      ensure  => file,
      path    => $path,
      owner   => 'root',
      group   => 'root',
      mode    => '0544',
      source  => "${base}/fetch_${name}list.script",
      require => $reqfile ? { '' => [], default => File[$reqfile] };
    "relay_cron_${name}":
      ensure  => file,
      path    => "/etc/cron.d/fetch_${name}list",
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      source  => "${base}/${name}_recipientrelays.cron",
      require => File["relay_file_${name}"];
  }
}
