class services::jskom {
  # packages
  package {
    ['nginx']:
      ensure => installed,
  }

  file {
    '/etc/nginx/sites-enabled/default':
      ensure => absent,
  }

  file {
    '/etc/nginx/sites-available/jskom':
      ensure => file,
      source => "${::pupfiles}/jskom/jskom.vhost",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      notify => Service['nginx'];
    '/etc/nginx/sites-available/httpkom':
      ensure => file,
      source => "${::pupfiles}/jskom/httpkom.vhost",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      notify => Service['nginx'];
    '/etc/nginx/sites-enabled/jskom':
      ensure  => symlink,
      target  => '/etc/nginx/sites-available/jskom',
      require => File['/etc/nginx/sites-available/jskom'],
      notify  => Service['nginx'];
    '/etc/nginx/sites-enabled/httpkom':
      ensure  => symlink,
      target  => '/etc/nginx/sites-available/httpkom',
      require => File['/etc/nginx/sites-available/httpkom'],
      notify  => Service['nginx'];
  }

  service {
    'nginx':
      ensure  => $running,
      enable  => true,
      require => Package['nginx'];
  }
}
