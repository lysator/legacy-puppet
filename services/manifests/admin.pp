class services::admin
{
  package
  {
    'nginx':
      ensure => installed;
    'python-dev':
      ensure => installed;
    'libmysqlclient-dev':
      ensure => installed;
  }

  file
  {
    '/etc/nginx/conf.d/admin.conf':
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template("${::pupfiles}/admin/nginx-admin.erb"),
      notify  => Service['nginx'];
    '/etc/init.d/supervisord':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0755',
      source => "${::pupfiles}/admin/supervisord-initd-service",
      notify => Service['supervisord'];
    '/etc/supervisord.conf':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0644',
      source => "${::pupfiles}/admin/supervisord-conf",
      notify => Service['supervisord'];
    '/var/log/uwsgi':
      ensure => directory,
      owner  => 'www-data',
      group  => 'www-data',
      mode   => '0750';
    '/web/lysadm-flask/conf/uwsgi.ini':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0644',
      source => "${::pupfiles}/admin/lysadm-uwsgi.ini",
      notify => Service['supervisord'];
  }

  exec { 'enable-supervisord':
    command => '/usr/sbin/update-rc.d supervisord defaults',
  }

  service
  {
    'nginx':
      ensure => $running,
      enable => true;
    'supervisord':
      ensure  => $running,
      enable  => true,
      require => Exec['enable-supervisord'];

  }

## Manual steps for now:
## check out lysadm-flask from git.lysator.liu.se to /web/lysadm-flask
## (easy_install pip)
## pip install uwsgi
## pip install supervisord
## pip install -r /web/lysadm-flask/requirements.txt
}
