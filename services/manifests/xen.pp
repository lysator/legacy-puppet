class services::xen
{
  package
  {
    ['xen-linux-system-2.6.26-2-xen-686', 'libc6-xen']:
      ensure => installed;
  }

  file
  {
    '/etc/xen/xend-config.sxp':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0644',
      source => "${::pupfiles}/xen/xend-config.sxp-backus.erb",
      notify => Service['xend'];
  }

  service
  {
    'xend':
      ensure => $running,
      enable => true;
  }

}
