class services::jukeserver
{
  file {
    '/usr/local/etc/musicpd.conf':
      ensure => file,
      owner  => root,
      group  => wheel,
      mode   => '0644',
      source => "${::pupfiles}/kvaser/musicpd.conf",
      notify => Service['musicpd'];
  }

  service {
    'musicpd':
      ensure => $running,
      enable => true;
  }

}
