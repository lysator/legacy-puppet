class services::asterisk
{
  package
  {
    ['asterisk', 'gcc', 'make', 'libncurses5-dev']:
      ensure => installed;
  }
}
