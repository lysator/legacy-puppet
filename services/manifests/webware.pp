class services::webware
{
  package
  {
    'python-pam':
      ensure => installed;
    'subversion':
      ensure => installed;
    'apache2':
      ensure => installed;
    'apache2-threaded-dev':
      ensure => installed;
    'python-cheetah':
      ensure => installed;
#    "python-dev":
#      ensure => installed;
    'alien':
      ensure => installed;
    'python-mysqldb':
      ensure => installed;
  }

  file
  {
    '/etc/apache2/mods-available/webkit.load':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0444',
      source => "${::pupfiles}/webware/webkit.load",
      notify => Service['apache2'];
    '/etc/apache2/mods-enabled/webkit.load':
      ensure => symlink,
      target => '/etc/apache2/mods-available/webkit.load',
      owner  => root,
      group  => root,
      notify => Service['apache2'];
    '/etc/apache2/mods-enabled/rewrite.load':
      ensure => symlink,
      target => '/etc/apache2/mods-available/rewrite.load',
      owner  => root,
      group  => root,
      notify => Service['apache2'];
    '/etc/apache2/mods-enabled/ssl.load':
      ensure => symlink,
      target => '/etc/apache2/mods-available/ssl.load',
      owner  => root,
      group  => root,
      notify => Service['apache2'];
    '/etc/apache2/sites-available/webware.lysator.liu.se':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0444',
      source => "${::pupfiles}/webware/vhost-webware",
      notify => Service['apache2'];
    '/etc/apache2/sites-enabled/webware.lysator.liu.se':
      ensure => symlink,
      target => '/etc/apache2/sites-available/webware.lysator.liu.se',
      owner  => root,
      group  => root,
      notify => Service['apache2'];
    '/etc/apache2/ports.conf':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0444',
      source => "${::pupfiles}/webware/ports.conf",
      notify => Service['apache2'];
  }

  service
  {
    'apache2':
      ensure => $running,
      enable => true;
  }

  cron {
    'lysinv-appserver':
      command  => '/home/lysinv/keepalive_appserver',
      user     => 'lysinv',
      month    => '1-12',
      monthday => '1-31',
      weekday  => '0-7',
      hour     => '0-23',
      minute   => [4,14,24,34,44,54];
  }
}
