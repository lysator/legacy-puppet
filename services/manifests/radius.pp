class services::radius
{

  package {
    'freeradius':
      ensure => installed;
    'freeradius-mysql':
      ensure => installed;
    'freeradius-utils':
      ensure => installed;
  }

  service {
    'freeradius':
      ensure => $running,
      enable => true;
  }


}
