class services::ftp {
  package {
    [
      'curl',
      'jq',
      'moreutils',
      'nginx',
      'nginx-prometheus-exporter',
      'node_exporter',
      'py37-certbot-nginx',
      'rsync',
      'tor',
      'vsftpd-ssl',
    ]:
    ensure => installed,
  }

  services::ftp::vsftpd { 'vsftpd_ipv4':
    ftp_listen => 'ipv4',
  }

  services::ftp::vsftpd { 'vsftpd_ipv6':
    ftp_listen => 'ipv6',
  }

  file { '/usr/local/etc/nginx/nginx.conf':
    ensure => file,
    source => "${::pupfiles}/ftp/nginx.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/etc/tor/torrc':
    ensure => file,
    source => "${::pupfiles}/ftp/torrc",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/bin/acme_sync.sh':
    ensure => file,
    source => "${::pupfiles}/ftp/acme_sync.sh",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0555',
  }

  file { '/usr/local/bin/git_mirror.sh':
    ensure => file,
    source => "${::pupfiles}/ftp/git_mirror.sh",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0555',
  }

  file { '/etc/cron.d/ftp-sync':
    ensure => file,
    source => "${::pupfiles}/ftp/ftp-sync",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/etc/rsync/rsyncd.conf':
    ensure => file,
    source => "${::pupfiles}/ftp/rsyncd.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0444',
  }

  file { '/usr/local/etc/rsyncd.conf':
    ensure => link,
    target => "/usr/local/etc/rsync/rsyncd.conf",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0444',
  }

  file { '/usr/local/etc/rsync_motd':
    ensure => file,
    source => "${::pupfiles}/ftp/rsync_motd",
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { 'newsyslog.conf.d':
    path => "/usr/local/etc/newsyslog.conf.d",
    source => "${::pupfiles}/ftp/newsyslog.conf.d/",
    recurse => remote,
    purge => false,
    ensure => file,
  }

  file { 'ftprsync':
    path => "/usr/local/bin",
    source => "${::pupfiles}/ftp/ftprsync/",
    recurse => remote,
    purge => false,
    ensure => file,
  }

  file { 'ftprsync-config':
    path => "/usr/local/etc/ftprsync",
    source => "${::pupfiles}/ftp/ftprsync-config/",
    recurse => remote,
    purge => false,
    ensure => file,
  }
}

define services::ftp::vsftpd($ftp_listen) {
  file { "/usr/local/etc/vsftpd_${ftp_listen}.conf":
    ensure  => file,
    content => template("${::pupfiles}/ftp/vsftpd.erb"),
    owner   => 'root',
    group   => 'wheel',
    mode    => '0644',
  }

  file { "/usr/local/etc/rc.d/vsftpd_${ftp_listen}":
    ensure  => file,
    content => template("${::pupfiles}/ftp/vsftpd.service.erb"),
    owner   => 'root',
    group   => 'wheel',
    mode    => '0555',
  }

  file { "/usr/local/libexec/vsftpd_${ftp_listen}":
    ensure => link,
    target => 'vsftpd',
  }

  ensure_line { "Enable vsftpd_${ftp_listen}":
    file => '/etc/rc.conf',
    line => "vsftpd_${ftp_listen}_enable=\"YES\"",
  }
}
