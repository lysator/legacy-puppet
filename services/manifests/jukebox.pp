class services::jukebox
{
  package {
    'mpd':
      ensure => installed;
    'fail2ban':
      ensure => installed;
  }

  file {
    '/etc/mpd.conf':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0644',
      source => "${::pupfiles}/jukebox/mpd.conf",
      notify => Service['mpd'];
  }

  service {
    'mpd':
      ensure => $running,
      enable => true;
  }

  file {
    '/etc/nftables.conf':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0644',
      source => "${::pupfiles}/jukebox/nftables.conf",
      notify => Service['nftables'];
  }

  service {
    'nftables':
      ensure => $running,
      enable => true;
  }

#  cron {
#    "music-group":
#      command => "chgrp -R lysator /mnt/music/",
#      user => "root",
#      minute => [5,15,25,35,45,55];
#    "music-dir-permissions":
#      command => "find /mnt/music/ -type d -exec chmod g+rwx {} \\;",
#      user => "root",
#      minute => [5,15,25,35,45,55];
#    "music-file-permissions":
#      command => "find /mnt/music/ -type f -exec chmod g+rw {} \\;",
#      user => "root",
#      minute => [5,15,25,35,45,55];
#  }
}
