class services::bugzilla {
  # This class is based on instructions at
  # <http://www.bugzilla.org/docs/4.4/en/html/configuration.html>
  #
  # If you are installing a newer version of Bugzilla, you should
  # probably look at the release notes and/or the updated installation
  # instructions.
  #
  # There are quite a few things you need to do manually::
  #
  #   mysql_secure_installation
  #   cd /var/www && git clone https://git.lysator.liu.se/lysator/bugzilla.git
  #   cd bugzilla
  #   # Install Perl modules as explained below.
  #   ./checksetup.pl
  #   # Edit localconfig.
  #   ./grant-bugs
  #
  # Migrate the database and "data" directory from the old
  # installation, as explained at
  # <https://wiki.mozilla.org/Bugzilla:Move_Installation>.
  #
  # Set up SSL certificates (Files/bugzilla/apache-site.erb lists the
  # files that need to exist.)
  #
  # Once the database is set up you also need to allow large attachments:
  #
  #   mysql -u bugs -p bugs
  #   > ALTER TABLE attachments AVG_ROW_LENGTH=1000000, MAX_ROWS=20000;

  $host = 'bugzilla.lysator.liu.se'
  $bugzilladirectory = '/var/www/bugzilla'

  package { 'mysql-server':
    ensure => installed,
  }

  file { '/etc/mysql/my.cnf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    source  => "${::pupfiles}/bugzilla/my.cnf",
    require => Package['mysql-server'],
    notify  => Service['mysql'];
  }

  service { 'mysql':
    ensure    => $running,
    enable    => true,
    hasstatus => true,
  }

  package { 'apache2':
    ensure => installed,
  }

  exec { '/usr/sbin/a2enmod headers':
    require => Package['apache2'],
    creates => '/etc/apache2/mods-enabled/headers.load',
    notify  => Service['apache2'],
  }

  exec { '/usr/sbin/a2enmod expires':
    require => Package['apache2'],
    creates => '/etc/apache2/mods-enabled/expires.load',
    notify  => Service['apache2'],
  }

  exec { '/usr/sbin/a2enmod ssl':
    require => Package['apache2'],
    creates => '/etc/apache2/mods-enabled/ssl.load',
    notify  => Service['apache2'],
  }

  exec { '/usr/sbin/a2enmod cgid':
    require => Package['apache2'],
    creates => '/etc/apache2/mods-enabled/cgid.load',
    notify  => Service['apache2'],
  }

  file { '/etc/apache2/sites-available/bugzilla.lysator.liu.se':
    require => Package['apache2'],
    content => template("${::pupfiles}/bugzilla/apache-site.erb"),
    owner   => 'root',
    group   => 'root',
    notify  => Service['apache2'],
  }

  exec { '/usr/sbin/a2ensite bugzilla.lysator.liu.se':
    require => File['/etc/apache2/sites-available/bugzilla.lysator.liu.se'],
    creates => '/etc/apache2/sites-enabled/bugzilla.lysator.liu.se',
    notify  => Service['apache2'],
  }

  exec { '/usr/sbin/a2dissite default':
    require => Package['apache2'],
    onlyif  => '/usr/bin/test -f /etc/apache2/sites-enabled/000-default',
    notify  => Service['apache2'],
  }

  service { 'apache2':
    ensure    => $running,
    hasstatus => true,
    enable    => true,
  }

  package { 'patchutils':
    ensure => installed,
  }

  package { 'graphviz':
    ensure => installed,
  }

  # This list of packages was current for Bugzilla 4.4.4.
  #
  # I also had to install a few modules from CPAN, as intructed by
  #
  #     ./checksetup.pl --check-modules
  #
  # in the unpacked Bugzilla directory:
  #
  #     perl install-module.pl PatchReader
  #     perl install-module.pl Email::Reply
  #     perl install-module.pl Daemon::Generic
  #     perl install-module.pl Apache2::SizeLimit
  package { ['libemail-send-perl',
      'libdatetime-perl',
      'libdatetime-timezone-perl',
      'libtemplate-perl',
      'libemail-mime-perl',
      'libmath-random-isaac-perl',
      'libtemplate-plugin-gd-perl',
      'libchart-perl',
      'libxml-twig-perl',
      'libauthen-sasl-perl',
      'libnet-smtp-ssl-perl',
      'libsoap-lite-perl',
      'libjson-rpc-perl',
      'libjson-xs-perl',
      'libtest-taint-perl',
      'libhtml-scrubber-perl',
      'libencode-detect-perl',
      'libtheschwartz-perl',
      'libapache2-mod-perl2',
      'libfile-mimeinfo-perl',
      'libhtml-formattext-withlinks-perl',
    ]:
      ensure => installed,
  }

  cron { 'bugzilla collectstats':
    command => "cd ${bugzilladirectory} && ./collectstats.pl",
    hour    => 5,
    minute  => 0,
  }

  cron { 'bugzilla whineatnews':
    command => "cd ${bugzilladirectory} && ./whineatnews.pl > /dev/null",
    hour    => 0,
    minute  => 55,
  }

  cron { 'bugzilla whine':
    command => "cd ${bugzilladirectory} && ./whine.pl",
    minute  => '*/15',
  }
}
