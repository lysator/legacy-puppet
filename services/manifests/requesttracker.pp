class services::requesttracker {

  # NGINX for Request Tracker (RT)
  package {
    [
      'nginx',
    ]:
      ensure => 'installed',
  }

  -> file {
    '/etc/nginx/nginx.conf':
      ensure  => file,
      source  => "${::pupfiles}/nginx/rt-nginx.conf",
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => [
        File['/etc/nginx/sites-enabled'],
        File['/etc/nginx/sites-available'],
        File['/etc/nginx/ssl'],
      ],
  }

  -> file {
    '/etc/nginx/sites-available/rt-server':
      ensure  => file,
      content => template("${::pupfiles}/nginx/sites-available/rt-server.erb"),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  -> exec {
    'generate-dhparam-file':
      command => 'openssl dhparam -out /etc/nginx/ssl/dhparam.pem 4096',
      path    => '/usr/bin',
      unless  => 'test -f /etc/nginx/ssl/dhparam.pem',
  }

  -> exec {
    'enable-site':
      command => 'ln -sf /etc/nginx/sites-available/rt-server /etc/nginx/sites-enabled/',
      path    => '/bin',
  }

  file {
    '/etc/nginx/sites-enabled/default':
      ensure => purged,
  }

  file {
    '/etc/nginx/ssl':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0751',
  }

  file {
    '/etc/nginx/sites-available':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
  }

  file {
    '/etc/nginx/sites-enabled':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
  }

  service {
    'nginx':
      ensure  => running,
      require => [
        Package['nginx'],
      ],
  }

  # Fail2ban for Request Tracker (RT)
  package {
    [
      'fail2ban',
    ]:
      ensure => 'installed',
  }

  -> file {
    '/etc/fail2ban/filter.d/requesttracker.conf':
      ensure  => file,
      content => template("${::pupfiles}/fail2ban/filter.d/requesttracker.conf.erb"),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  -> file {
    '/etc/fail2ban/jail.local':
      ensure => file,
      source => "${::pupfiles}/fail2ban/rt-jail.local",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  service {
    'fail2ban':
      ensure  => running,
      require => [
        Package['fail2ban'],
      ],
  }

  file {
    '/etc/systemd/system/rt4-fcgi@.service':
      ensure => file,
      source => "${::pupfiles}/spawn-fcgi/rt4-fcgi@.service",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }

  file {
    '/etc/systemd/system/rt4-fcgi@.socket':
      ensure => file,
      source => "${::pupfiles}/spawn-fcgi/rt4-fcgi@.service",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
  }
}
