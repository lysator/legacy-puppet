class services::temperature_foo
{
  package {
    'owfs':
      ensure => installed;
    'nginx':
      ensure => installed;
    'curl':
      ensure => installed;
  }

  file {
    '/etc/init.d/owfs':
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
      source => "${::pupfiles}/temperature-foo/init-owfs",
      notify => Service['owfs'];
    '/opt/temperature':
      ensure => 'directory',
      owner  => 'root',
      group  => 'root',
      mode   => '0755';
    '/opt/temperature/temp-csv.sh':
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
      source => "${::pupfiles}/temperature-foo/temp-csv.sh";
    '/opt/temperature/humi-csv.sh':
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
      source => "${::pupfiles}/temperature-foo/humi-csv.sh";
    '/opt/temperature/temp-populate.sh':
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
      source => "${::pupfiles}/temperature-foo/temp-populate.sh";
    '/media/1wire':
      ensure => 'directory',
      owner  => 'root',
      group  => 'root',
      mode   => '0755';
    '/srv/temperature':
      ensure => 'directory',
      owner  => 'root',
      group  => 'root',
      mode   => '0755';
    '/srv/temperature/index.html':
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/temperature-foo/index.html";
    '/srv/temperature/dygraph-combined.js':
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/temperature-foo/dygraph-combined.js";
    '/etc/nginx/conf.d/default.conf':
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => "${::pupfiles}/temperature-foo/nginx-conf";

  }

  # */5 * * * * /opt/temperature/temp-populate.sh > /dev/null
  cron {
    'Read temperature every 5 minutes':
      command => '/opt/temperature/temp-populate.sh > /dev/null',
      user    => 'root',
      minute  => '*/5';
  }

  service {
    'owfs':
      #   ensure => running,
      enable => true;
    'owserver':
      enable => true;
  }


}
