class services::jabber
{
  package
  {
    "ejabberd":
      ensure => installed;
  }

  service
  {
    'ejabberd':
      ensure    => $running,
      enable    => true;
  }
}
