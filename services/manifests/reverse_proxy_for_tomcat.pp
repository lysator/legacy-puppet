class services::reverse_proxy_for_tomcat
{
  package {
    'nginx':
      ensure => installed;
  }

  file {
    '/etc/nginx/sites-available/default':
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template("${::pupfiles}/nginx-reverseproxy-for-tomcat/nginx-default.erb"),
      notify  => Service['nginx'];
  }

  service {
    'nginx':
      ensure => $running,
      enable => true;
  }

}
