class services::puppet::puppet_git_server
{
  package
  {
    'git-daemon-sysvinit':
      ensure => installed;
  }

  file
  {
    '/etc/default/git-daemon':
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0444',
      source  => "${::pupfiles}/puppet/git-daemon",
      notify  => Service['git-daemon'],
      require => Package['git-daemon-sysvinit'],;
  }

  service
  {
    'git-daemon':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['git-daemon-sysvinit'];
  }
}
