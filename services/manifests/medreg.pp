class services::medreg
{
  package
  {
    'apache2':
      ensure => installed;
    'libapache2-mod-fcgid':
      ensure => installed;
    'mariadb-server':
      ensure => installed;
    'python-mysqldb':
      ensure => installed;
    # Squid is needed for pam_auth binary
    'squid':
      ensure => installed;
  }

  file
  {
    '/etc/apache2/mods-enabled/rewrite.load':
      ensure  => symlink,
      target  => '/etc/apache2/mods-available/rewrite.load',
      owner   => root,
      group   => root,
      notify  => Service['apache2'],
      require => Package['apache2'];
    '/etc/apache2/mods-enabled/ssl.load':
      ensure  => symlink,
      target  => '/etc/apache2/mods-available/ssl.load',
      owner   => root,
      group   => root,
      notify  => Service['apache2'],
      require => Package['apache2'];
    '/etc/apache2/sites-available/medreg.lysator.liu.se.conf':
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0444',
      source  => "${::pupfiles}/medreg/apache-vhost",
      notify  => Service['apache2'],
      require => Package['apache2'];
    '/etc/apache2/sites-enabled/medreg.lysator.liu.se':
      ensure  => symlink,
      target  => '/etc/apache2/sites-available/medreg.lysator.liu.se.conf',
      owner   => root,
      group   => root,
      notify  => Service['apache2'],
      require => File['/etc/apache2/sites-available/medreg.lysator.liu.se.conf'];
    '/etc/apache2/sites-enabled/000-default':
      ensure => absent,
      notify => Service['apache2'];
    '/usr/lib/squid/basic_pam_auth':
      ensure  => file,
      mode    => '2755',
      require => Package['squid'];
  }

  service
  {
    'apache2':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['apache2'];
    'mariadb':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['mariadb-server'];
    'squid':
      ensure  => 'stopped',
      enable  => false,
      require => Package['squid'];
  }

  cron {
    'medreg-show-yesterday':
      command => '/opt/medreg/prg/show_yesterday.py',
      user    => 'medreg',
      hour    => 0,
      minute  => 5;
    'medreg-mika':
      command => '/opt/medreg/prg/mika.py',
      user    => 'medreg',
      hour    => 1,
      minute  => 10;
    'medreg-remind':
      command => '/opt/medreg/prg/remind.py',
      user    => 'medreg',
      hour    => 1,
      minute  => 20;
    'medreg-show-payment':
      command => '/opt/medreg/prg/show_payment.py',
      user    => 'medreg',
      hour    => 1,
      minute  => 30;
    'medreg-show-cardstatus':
      command => '/opt/medreg/prg/show_cardstatus.py',
      user    => 'medreg',
      hour    => 1,
      minute  => 40;
    'backup-mysql':
      command => 'TMPFILE=`/bin/mktemp /var/tmp/fulldump.sql.XXX` && /usr/bin/mysqldump --all-databases --events > "$TMPFILE" && mv "$TMPFILE" /var/lib/mysql-dump/fulldump.sql',
      user    => 'root',
      hour    => 3,
      minute  => 11;
  }
}
