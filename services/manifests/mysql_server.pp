class services::mysql_server {

  file {
    '/etc/mysql/my.cnf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0444',
      source  => "${::pupfiles}/mysql/my.cnf",
      require => Package['mariadb-server'],
      notify  => Service['mariadb'];
  }

  package {
    'mariadb-server':
      ensure => installed;
  }

  service {
    'mariadb':
      ensure    => $running,
      enable    => true,
      hasstatus => true,
      require   => Package['mariadb-server'];
  }
}

