class services::web
{
  package
  {
    ['build-essential', 'gcc', 'make']:
      ensure => installed;
  }
}
