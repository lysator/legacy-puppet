class services::backup
{
  file
  {
    '/opt/LYSrdiff':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0775';
    '/opt/LYSrdiff/bin':
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0775',
      require => File['/opt/LYSrdiff'];
    '/opt/LYSrdiff/var':
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0775',
      require => File['/opt/LYSrdiff'];
    '/usr/local/nagios':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0775';
    '/usr/local/nagios/libexec':
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0775',
      require => File['/usr/local/nagios'];
    '/usr/local/nagios/libexec/check_lysrdiff':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0775',
      source  => "${::pupfiles}/backup/check_lysrdiff",
      require => File['/usr/local/nagios/libexec'];
  }
}
