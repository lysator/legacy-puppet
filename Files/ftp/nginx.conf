
#user  nobody;
worker_processes  4;

# This default error log path is compiled-in to make sure configuration parsing
# errors are logged somewhere, especially during unattended boot when stderr
# isn't normally logged anywhere. This path will be touched on every nginx
# start regardless of error log location configured here. See
# https://trac.nginx.org/nginx/ticket/147 for more info. 
#
#error_log  /var/log/nginx/error.log;
#

#pid        logs/nginx.pid;


load_module /usr/local/libexec/nginx/ngx_http_headers_more_filter_module.so;
load_module /usr/local/libexec/nginx/ngx_http_fancyindex_module.so;


events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;
    tcp_nopush      on;

    keepalive_timeout  65;

    server_tokens off;
    more_clear_headers 'Server';

    vhost_traffic_status_zone;
    vhost_traffic_status_dump /var/tmp/nginx/vts.db;
    vhost_traffic_status_limit off;
    vhost_traffic_status_histogram_buckets 0.005 0.01 0.05 0.1 0.5 1 2 3 4 5 7.5 10;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" - "$http_x_forwarded_for"';

    server {
        listen       80;
        listen       [::]:80;
        server_name  hina;

        access_log  /var/log/nginx/access.log  main;

        location / {
            root   /var/ftp;
            fancyindex on;
            fancyindex_footer /.footer.html;
            error_page 404 /.404.html;
        }

        # nginx prometheus performance export
        location = /basic_status {
            stub_status;
        }

        location /metrics {
            vhost_traffic_status_display;
            # don't include requests to this location in stats
            vhost_traffic_status_bypass_stats on;
        }
    }


    # HTTPS server
    #
    server {
        listen       443 ssl;
        listen       [::]:443 ssl;
        server_name  hina;

        access_log  /var/log/nginx/access.log main;

        ssl_dhparam /usr/local/etc/nginx/dhparam.pem;

        ssl_certificate /usr/local/etc/letsencrypt/live/hina.lysator.liu.se/fullchain.pem;
        ssl_certificate_key  /usr/local/etc/letsencrypt/live/hina.lysator.liu.se/privkey.pem;

        ssl_session_cache    shared:SSL:1m;
        ssl_session_timeout  5m;

        ssl_ciphers  HIGH:!aNULL:!MD5;
        ssl_prefer_server_ciphers  on;

        location / {
            root   /var/ftp;
            fancyindex on;
            fancyindex_footer /.footer.html;
            error_page 404 /.404.html;
        }
    }

}
