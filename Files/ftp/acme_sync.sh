#!/bin/sh

for host in laban labolina anna; do
  cat /usr/local/etc/letsencrypt/live/hina.lysator.liu.se/fullchain.pem /usr/local/etc/letsencrypt/live/hina.lysator.liu.se/privkey.pem | ssh -i /root/.ssh/id_acme_sync -b 2001:6b0:17:f0a0::c3 "$host" /opt/acme-sync/acme-sync.sh
done
