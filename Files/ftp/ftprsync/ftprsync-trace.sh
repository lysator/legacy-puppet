#!/bin/sh

PATH=/bin:/sbin:/usr/bin:/usr/sbin
export PATH

LOCKFILEDIR=/var/run/ftprsync/
LOCALREPODIR=/export/ftp/pub/
LOGFILEDIR=/var/log/ftprsync/
LOGFILEPREFIX=
LOCKFILE=
RSYNCPROG=/usr/local/bin/rsync
RSYNCCMD="-vrlt --hard-links --delete --delete-after --delay-updates --stats --progress --chmod ug+w,o-w,+rX"
RSYNCSERV=
RSYNCREPO=
LOCALREPO=
LOGFILE=
RSYNCEXIT=
EXTRAARGS=

if [ $# -lt 5 ]
then
    echo "usage: $0 rsync-server rsync-repo local-repo lockfile logfile [extra rsync args]" >&2
    exit 1
fi

RSYNCSERV=$1
RSYNCREPO=$2
LOCALREPO=$LOCALREPODIR$3
LOCKFILE=$LOCKFILEDIR$4
LOGFILEPREFIX=$5
LOGFILE=$LOGFILEDIR$LOGFILEPREFIX.`date +%Y-%m-%d_%H:%M:%S`.out

shift 5
for arg in $@
do
	EXTRAARGS="$EXTRAARGS $arg"
done

if [ -e $LOCKFILE ]
then
	echo "lockfile exists, rsync already running (syncing $LOCALREPO)" >&2
	exit 2
fi

cp /dev/null $LOGFILE
if [ $? != 0 ]
then
	echo "cannot create logfile, check permissions (syncing $LOCALREPO)" >&2
	exit 3
fi

cp /dev/null $LOCKFILE
if [ $? != 0 ]
then
	echo "cannot create lockfile, check permissions (syncing $LOCALREPO)" >&2
	exit 4
fi

for file in `find $LOGFILEDIR -name "$LOGFILEPREFIX.*" | sort -n -r | tail +9`
do
	rm $file
done

echo "$RSYNCPROG --log-file=$LOGFILE $RSYNCCMD$EXTRAARGS $RSYNCSERV::$RSYNCREPO $LOCALREPO/" > $LOGFILE

for i in 1 2 3
do
	$RSYNCPROG --log-file=$LOGFILE $RSYNCCMD $EXTRAARGS $RSYNCSERV::$RSYNCREPO $LOCALREPO/ > /dev/null 2>&1
	RSYNCEXIT=$?
	if [ $RSYNCEXIT -eq 0 ]
	then
		break
	else
		echo "rsync of $LOCALREPO failed, exit code was $RSYNCEXIT (attempt $i)" >&2
		echo "last lines of $LOGFILE is" >&2
		echo "======================================="
		tail -n 16 $LOGFILE >&2
		echo "======================================="
		echo ""
		echo "rsync failed, exit code was $RSYNCEXIT (attempt $i)" >> $LOGFILE
	fi
done

/usr/local/bin/cache-flush.py $LOGFILE $LOCALREPO

rm $LOCKFILE
if [ $? != 0 ]
then
	echo "cannot remove lockfile, remove it by hand (syncing $LOCALREPO)" >&2
fi

if [ $RSYNCEXIT != 0 ]
then
	echo rsync of $LOCALREPO failed after three attempts >&2
	exit 5
else
	date -u > $LOCALREPO/project/trace/ftp.lysator.liu.se
	exit 0
fi
