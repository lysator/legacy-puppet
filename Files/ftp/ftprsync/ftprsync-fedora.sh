#!/bin/sh

PATH=/bin:/sbin:/usr/bin:/usr/sbin
export PATH

LOCKFILEDIR=/var/run/ftprsync/
LOCALREPODIR=/var/ftp/pub/
LOGFILEDIR=/var/log/ftprsync/
LOGFILEPREFIX=
LOCKFILE=
RSYNCPROG=/usr/local/bin/rsync
RSYNCCMD="-vrlt --hard-links --numeric-ids --delete --delete-delay --delay-updates --stats --progress "
RSYNCSERV=
RSYNCREPO=
LOCALREPO=
LOGFILE=
RSYNCEXIT=
EXTRAARGS=
REPORTAPP=/usr/local/bin/report_mirror
REPORTCONF=/usr/local/etc/report_mirror.conf
LOGFILESUFFIX=`date +%Y-%m-%d_%H:%M:%S`.out


if [ $# != 4 ]
then
    echo "usage: $0 epel-server epel-repo fedora-server fedora-repo" >&2
    exit 1
fi

EPELSERV=$1
EPELREPO=$2
FEDORASERV=$3
FEDORAREPO=$4

create_lock () {
	if [ -e $LOCKLFILEDIR/$1 ]
	then
		echo "lockfile exists, rsync already running (syncing $1)" >&2
		return 1
	fi
	cp /dev/null $LOCKFILEDIR/$1
	ret=$?
	if [ $ret != 0 ]
	then
		echo "cannot create lockfile, check permissions (syncing $1)" >&2
		return 1
	fi
	return 0
}

remove_lock () {
	if [ -e $LOCKFILEDIR/$1 ]
	then
		rm $LOCKFILEDIR/$1
		ret=$?
		if [ $ret != 0 ]
		then
			echo "cannot remove lockfile, remove it by hand (synching $1)" >&2
		fi
	fi
}

create_log () {
	cp /dev/null $LOGFILEDIR/$1.$LOGFILESUFFIX
	ret=$?
	if [ $ret != 0 ]
	then
		echo "cannot create logfile, check permissions (syncing $1)" >&2
		return 1
	fi
	for file in `find $LOGFILEDIR -name "$1.*" | sort -n -r | tail +9`
	do
		rm $file
	done
	return 0
}

do_rsync () {
	serv=$1
	remote=$2
	local=$3
	logfile=$4
	echo "$RSYNCPROG --log-file=$logfile $RSYNCCMD $serv::$remote $local/" > $logfile
	for i in 1 2 3
	do
		$RSYNCPROG --log-file=$logfile $RSYNCCMD $serv::$remote $local/ > /dev/null 2>&1
		ret=$?

		/usr/local/bin/cache-flush.py $logfile $local

		if [ $ret -eq 0 ]
		then
			return 0
		else
			echo "rsync of $local failed, exit code was $ret (attempt $i)" >&2
			echo "last lines of $logfile is" >&2
			echo "======================================="
			tail -n 16 $logfile >&2
			echo "======================================="
			echo ""
			echo "rsync failed, exit code was $ret (attempt $i)" >> $logfile
		fi
	done
	echo "rsync of $local failed after three attempts" >&2
	return 1
}


create_lock "epel.rsync.lock"
ret=$?
if [ $ret -eq 0 ]
then
	create_log "epel"
	ret=$?
	if [ $ret -eq 0 ]
	then
		do_rsync $EPELSERV $EPELREPO $LOCALREPODIR/epel $LOGFILEDIR/epel.$LOGFILESUFFIX
	fi
	remove_lock "epel.rsync.lock"
fi

create_lock "fedora.rsync.lock"
ret=$?
if [ $ret -eq 0 ]
then
	create_log "fedora"
	ret=$?
	if [ $ret -eq 0 ]
	then
		do_rsync $FEDORASERV $FEDORAREPO $LOCALREPODIR/fedora $LOGFILEDIR/fedora.$LOGFILESUFFIX
	fi
	remove_lock "fedora.rsync.lock"
fi

$REPORTAPP -c $REPORTCONF > /dev/null
