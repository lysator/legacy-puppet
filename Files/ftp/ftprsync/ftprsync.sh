#!/bin/sh

PATH=/bin:/sbin:/usr/bin:/usr/sbin
export PATH

CONFIG_FILE="/usr/local/etc/ftprsync/$3.conf"
if [ -r "$CONFIG_FILE" ]; then
	. "$CONFIG_FILE"
elif [ -t 1 ]; then # Check if the script is running interactively.
	echo "WARNING: Configuration file $CONFIG_FILE not found!"
fi

LOCKFILEDIR=/var/run/ftprsync/
LOCALREPODIR=/var/ftp/pub/
LOGFILEDIR=/var/log/ftprsync/
LOGFILEPREFIX=
LOCKFILE=
RSYNCPROG=/usr/local/bin/rsync
RSYNCCMD="${RSYNC_FLAGS:-"-vrlt --hard-links --delete --delete-delay --delay-updates --stats --progress --chmod ug+w,o-w,+rX"}"
RSYNCSERV=
RSYNCREPO=
LOCALREPO=
LOGFILE=
NUM_LOGFILES="${NUM_LOGFILES:-9}"
RSYNCEXIT=
EXTRAARGS=

if [ $# -lt 5 ]
then
    echo "usage: $0 rsync-server rsync-repo local-repo lockfile logfile [extra rsync args]" >&2
    exit 1
fi

RSYNCSERV=$1
RSYNCREPO=$2
LOCALREPO=$LOCALREPODIR$3
LOCKFILE=$LOCKFILEDIR$4
LOGFILEPREFIX=$5
LOGFILE=$LOGFILEDIR$LOGFILEPREFIX.`date +%Y-%m-%d_%H:%M:%S`.out

# Random sleep for 0 s to 5 m.
/usr/local/bin/perl -e 'sleep(int(rand(5*60)))'

shift 5
for arg in $@
do
	EXTRAARGS="$EXTRAARGS $arg"
done

if [ ! -d "$LOGFILEDIR" ]
then
	mkdir -p "$LOGFILEDIR"
fi

if [ ! -d "$LOCKFILEDIR" ]
then
	mkdir -p "$LOCKFILEDIR"
fi

if [ -e $LOCKFILE ]
then
	echo "lockfile exists, rsync already running (syncing $LOCALREPO)" >&2
	exit 2
fi

cp /dev/null $LOGFILE
if [ $? != 0 ]
then
	echo "cannot create logfile, check permissions (syncing $LOCALREPO)" >&2
	exit 3
fi

cp /dev/null $LOCKFILE
if [ $? != 0 ]
then
	echo "cannot create lockfile, check permissions (syncing $LOCALREPO)" >&2
	exit 4
fi

for file in `find $LOGFILEDIR -name "$LOGFILEPREFIX.*" | sort -n -r | tail +$NUM_LOGFILES`
do
	rm $file
done

echo "$RSYNCPROG --log-file=$LOGFILE $RSYNCCMD$EXTRAARGS $RSYNCSERV::$RSYNCREPO $LOCALREPO/" > $LOGFILE

$RSYNCPROG --log-file=$LOGFILE $RSYNCCMD $EXTRAARGS $RSYNCSERV::$RSYNCREPO $LOCALREPO/ > /dev/null 2>&1
RSYNCEXIT=$?
if [ $RSYNCEXIT != 0 ] && [ $RSYNCEXIT != 23 ] && [ $RSYNCEXIT != 24 ]; then
	echo "rsync of $LOCALREPO failed, exit code was $RSYNCEXIT" >&2
	echo "last lines of $LOGFILE is" >&2
	echo "======================================="
	tail -n 16 $LOGFILE >&2
	echo "======================================="
	echo ""
	echo "rsync failed, exit code was $RSYNCEXIT" >> $LOGFILE
fi

/usr/local/bin/cache-flush.py $LOGFILE $LOCALREPO

rm $LOCKFILE
if [ $? != 0 ]
then
	echo "cannot remove lockfile, remove it by hand (syncing $LOCALREPO)" >&2
fi

if [ $RSYNCEXIT != 0 ] && [ $RSYNCEXIT != 23 ] && [ $RSYNCEXIT != 24 ]
then
	echo rsync of $LOCALREPO failed after >&2
	exit 5
else
	exit 0
fi
