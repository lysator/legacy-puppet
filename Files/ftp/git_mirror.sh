#!/bin/sh -e

if [ $# -ne 3 ]
then
	echo "github-mirror.sh \$user \$repo \$mirror_dir"
	exit 1
fi

name="$1"
proj="$2"
wd="$3"

cd "$wd"

TMP_DIR=$(mktemp -d)

curl https://api.github.com/repos/$name/$proj/releases -o ${TMP_DIR}/releases.json
jq '[.[] | {tag_name, draft, assets: [.assets[].browser_download_url]}]' < ${TMP_DIR}/releases.json > ${TMP_DIR}/tmp.json
list=$(jq -r '.[] | @base64' < ${TMP_DIR}/tmp.json)
for i in $list
do
	draft=$(echo "$i" | base64 -d | jq -r '.draft')
	tagname=$(echo "$i" | base64 -d | jq -r '.tag_name')
	for j in $(echo "$i" | base64 -d | jq -r '.assets[]')
	do
		url=$(echo "$j")
		filename=$(basename "$url")
		mkdir -p "$tagname"
		cd "$tagname"
		test -f "$filename" || curl -L "$url" -o "$filename"
		cd ..
	done
done
rm -r "${TMP_DIR}"
