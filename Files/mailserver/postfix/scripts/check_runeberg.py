#!/usr/bin/python2

import asyncore
import smtplib
import smtpd
import logging

# This server
LOCALADDRESS='127.0.0.1'
LOCALPORT=10040

# Relay-server
MAILHOST='127.0.0.1'
MAILPORT=10025

LOGLEVEL=logging.WARNING

accepted_adresses=['editor@runeberg.org','editors@runeberg.org','info@runeberg.org','postmaster@runeberg.org','red@runeberg.org','redaksjon@runeberg.org','redaksjonen@runeberg.org','redaktion@runeberg.org','redaktionen@runeberg.org','webmaster@runeberg.org','runeberg@lysator.liu.se','runeberg@runeberg.org','root@runeberg.org']

class FilterServer(smtpd.SMTPServer):
    def process_message(self, peer, mailfrom, rcpttos, data):
        logging.debug('Recieved message [%s] to %s' % (mailfrom,rcpttos))
        for adress in accepted_adresses:
            if data.find(adress)!=-1:
                logging.debug('Matched [%s], accepting message.' % (adress,))
                return self._deliver(mailfrom, rcpttos, data)
        logging.debug('Message rejected')
        return "550 Message content rejected"

    def _deliver(self, mailfrom, rcpttos, data):
        try:
            s = smtplib.SMTP()
            s.connect(MAILHOST,MAILPORT)
            try:
                refused = s.sendmail(mailfrom, rcpttos,data)
                logging.debug('Sent message to relay-server')
            finally:
                s.quit()
        except Exception,e:
            logging.error(e)
            return "450 Server configuration error (check_runeberg.py)"

# Do note, the following function is slightly modified from:
# http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/278731
# Author: Chad J. Schroeder
# Copyright (C) 2005 Chad J. Schroeder

# Standard Python modules.
import os               # Miscellaneous OS interfaces.
import sys              # System-specific parameters and functions.

# Default maximum for the number of available file descriptors.
MAXFD = 1024

# The standard I/O file descriptors are redirected to /dev/null by default.
if (hasattr(os, "devnull")):
   REDIRECT_TO = os.devnull
else:
   REDIRECT_TO = "/dev/null"

def createDaemon():
   """Detach a process from the controlling terminal and run it in the
   background as a daemon.
   """

   try:
      pid = os.fork()
   except OSError, e:
      raise Exception, "%s [%d]" % (e.strerror, e.errno)

   if (pid == 0):	# The first child.
      os.setsid()
      try:
         pid = os.fork()	# Fork a second child.
      except OSError, e:
         raise Exception, "%s [%d]" % (e.strerror, e.errno)

      if (pid == 0):	# The second child.
          os.chdir('/') # To prevent keeping filesystems busy
      else:
         os._exit(0)	# Exit parent (the first child) of the second child.
   else:
       os._exit(0)	# Exit parent of the first child.
   import resource		# Resource usage information.
   maxfd = resource.getrlimit(resource.RLIMIT_NOFILE)[1]
   if (maxfd == resource.RLIM_INFINITY):
      maxfd = MAXFD
  
   # Iterate through and close all file descriptors.
   for fd in range(0, maxfd):
      try:
         os.close(fd)
      except OSError:	# ERROR, fd wasn't open to begin with (ignored)
         pass
   os.open(REDIRECT_TO, os.O_RDWR)	# standard input (0)

   # Duplicate standard input to standard output and standard error.
   os.dup2(0, 1)			# standard output (1)
   os.dup2(0, 2)			# standard error (2)

   return(0)

if __name__ == '__main__':
    l = logging.FileHandler('/var/log/check_runeberg.log')
    fmt = logging.Formatter(logging.BASIC_FORMAT)
    l.setFormatter(fmt)
    logging.root.addHandler(l)
    logging.debug('check_runeberg.py started')
    createDaemon()
    logging.debug('daemonized')
    mailer = FilterServer((LOCALADDRESS,LOCALPORT),(MAILHOST,MAILPORT))
    asyncore.loop()
    logging.debug('Server terminated')
