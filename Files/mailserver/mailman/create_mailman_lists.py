import MySQLdb
import subprocess

MAIL_DOMAIN = 'lysator.liu.se'
MAILMAN_DOMAIN = 'lists.lysator.liu.se'
MAILMAN_USER = 'list'

db=MySQLdb.connect(db="mailconf")
db.query("""SELECT listinfo.listaddr, members.login FROM listinfo JOIN members ON listinfo.uid=members.id WHERE listinfo.action='CREATE'""")
result = db.store_result()

c = db.cursor()

while True:
    row = result.fetch_row()
    if not row:
        break
    listaddr = row[0][0]
    owner = row[0][1]

    output = subprocess.run(['sudo', '-u', MAILMAN_USER, '/usr/bin/mailman', 'create', '--no-domain', '--notify', '--owner', owner + '@' + MAIL_DOMAIN, listaddr + '@' + MAILMAN_DOMAIN], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    c.execute("""DELETE FROM listinfo WHERE listaddr=%s""", (listaddr,))
    

db.close()
