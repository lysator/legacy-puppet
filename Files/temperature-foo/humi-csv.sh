#! /bin/sh

# Read the humidity sensor and output it on one line comma
# separated (and a timestamp too).
# Example output: 2014-13-19, 37.0839
# The value is %RH (i.e. relative humidity)

THE_TIME=`date +"%Y-%m-%d %T"`
S1=`head /media/1wire/26.FDFD91010000/humidity`

echo $THE_TIME, $S1
