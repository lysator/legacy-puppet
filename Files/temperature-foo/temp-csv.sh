#! /bin/sh

# Read the temperature sensors and output it on one line comma
# separated (and a timestamp too).
# Example output: 2014-13-19 11:13:02, 22.125, 24.1875, 20.0625, 17.75

THE_TIME=`date +"%Y-%m-%d %T"`
S1=`head /media/1wire/28.7BB2AD060000/temperature`
S2=`head /media/1wire/28.EDD47C060000/temperature`
S3=`head /media/1wire/28.375F84050000/temperature`
S4=`head /media/1wire/28.E39F84050000/temperature`

curl –-connect-timeout 3 https://www.temperatur.nu/termo/valla/temp.txt 2>/dev/null > /srv/temperature/valla.txt
VALLATEMP=`cat /srv/temperature/valla.txt`

echo $THE_TIME, $S1, $S2, $S3, $S4, $VALLATEMP

