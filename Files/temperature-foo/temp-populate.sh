#! /bin/sh

# Write temperature (from temp-csv.sh) to files in a directory from
# where the web server can serve it.

# A file for the last read value
/opt/temperature/temp-csv.sh > /srv/temperature/temp.txt

# Append it to a file with all readings so far
cat /srv/temperature/temp.txt >> /srv/temperature/tempdata.csv

# Likewise for humidity
/opt/temperature/humi-csv.sh > /srv/temperature/humi.txt
cat /srv/temperature/humi.txt >> /srv/temperature/humidata.csv

# Create a file with headers and append the last 30 days of readings
#echo "Timestamp, Varmsida, Insug varmsida, Kallsida, Utluft kallsida, Utomhus (Valla)" > /srv/temperature/30days.csv
echo "Timestamp, Kallsida rack, Kallsida dorr, Varmsida, Labbyta, Utomhus (Valla)" > /srv/temperature/30days.csv
tail -n 8640 /srv/temperature/tempdata.csv >> /srv/temperature/30days.csv

