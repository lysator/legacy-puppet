# TCP/UDP network access for NFS
# File managed by puppet
network inet stream,
network inet6 stream,
network inet dgram,
network inet6 dgram,
